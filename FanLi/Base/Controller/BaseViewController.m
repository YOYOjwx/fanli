//
//  BaseViewController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/12.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "BaseViewController.h"
#import <UMMobClick/MobClick.h>

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置背景颜色 */
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.navigationController.tabBarItem.title.length) {
        [MobClick beginLogPageView:self.navigationController.tabBarItem.title];
    }
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (self.navigationController.tabBarItem.title.length) {
        [MobClick endLogPageView:self.navigationController.tabBarItem.title];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [super touchesBegan:touches withEvent:event];
    /** 设置键盘回弹 */
    [self.view endEditing:YES];
    [self.navigationController.navigationBar endEditing:YES];
}

@end
