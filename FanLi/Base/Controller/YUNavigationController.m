//
//  YUNavigationController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/12.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YUNavigationController.h"
#import "UIImage+Common.h"

@interface YUNavigationController ()

@end

@implementation YUNavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 保持左滑动手势 */
    self.interactivePopGestureRecognizer.delegate = nil;
    /** 取消半透明 */
    self.navigationBar.translucent = NO;
    /** 设置标题文字字体 */
    [self.navigationBar setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]}];
}

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated{
    [super pushViewController:viewController animated:animated];
    /** 左边返回父视图按钮 */
    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(back)];
}

/** 返回父视图 */
- (void)back{
    [self popViewControllerAnimated:YES];
}

@end
