//
//  YUTabBarController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/12.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YUTabBarController.h"
#import "YUNavigationController.h"
#import "BaseViewController.h"
#import "UIImage+Common.h"

@interface YUTabBarController ()<UITabBarControllerDelegate>


@property (nonatomic, strong) UIView *tabBarSelectView;

@end

@implementation YUTabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 取消半透明 */
    self.tabBar.translucent = NO;
    /** 被选中后文字的颜色 */
    self.tabBar.tintColor = [UIColor whiteColor];
    /** tabBar添加阴影 */
    self.tabBar.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabBar.layer.shadowOpacity = 0.05;
    /** 去除边框线 */
    [self.tabBar setBackgroundImage:[UIImage new]];
    [self.tabBar setShadowImage:[UIImage new]];
    /** 创建控制器 */
    [self creatChildViewController];
    /** 接收代理 */
    self.delegate = self;
    /** 创建tabBar选择的视图 */
    self.tabBarSelectView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth / 5, 49)];
    self.tabBarSelectView.backgroundColor = UIColorFromHex(0xFF5E56);
    [self.tabBar insertSubview:self.tabBarSelectView atIndex:0];
}

/** 创建控制器 */
- (void)creatChildViewController{
    /** 选中后的颜色 */
    UIColor *selectColor = [UIColor whiteColor];
    /** 首页 */
    UIViewController *homeVC = [NSClassFromString(@"HomeController") new];
    YUNavigationController *home = [[YUNavigationController alloc] initWithRootViewController:homeVC];
    homeVC.navigationItem.leftBarButtonItem = nil;
    home.tabBarItem.title = @"首页";
    home.tabBarItem.image = [[UIImage imageNamed:@"首页"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    home.tabBarItem.selectedImage = [[[UIImage imageNamed:@"首页"] imageWithColor:selectColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    [self addChildViewController:home];
    /** 搜返利 */
    UIViewController *searchVC = [NSClassFromString(@"SearchCommodityController") new];
    YUNavigationController *search = [[YUNavigationController alloc] initWithRootViewController:searchVC];
    searchVC.navigationItem.leftBarButtonItem = nil;
    search.tabBarItem.image = [[UIImage imageNamed:@"搜返利"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    search.tabBarItem.selectedImage = [[[UIImage imageNamed:@"搜返利"] imageWithColor:selectColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    search.tabBarItem.title = @"搜返利";
    [self addChildViewController:search];
    /** 商城返利 */
    UIViewController *rebateVC = [NSClassFromString(@"RebateController") new];
    YUNavigationController *rebate = [[YUNavigationController alloc] initWithRootViewController:rebateVC];
    rebateVC.navigationItem.leftBarButtonItem = nil;
    rebate.tabBarItem.image = [[UIImage imageNamed:@"商城返利"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    rebate.tabBarItem.selectedImage = [[[UIImage imageNamed:@"商城返利"] imageWithColor:selectColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    rebate.tabBarItem.title = @"商城返利";
    [self addChildViewController:rebate];
    /** 趣逛逛 */
    UIViewController *strollVC = [NSClassFromString(@"StrollController") new];
    YUNavigationController *stroll = [[YUNavigationController alloc] initWithRootViewController:strollVC];
    strollVC.navigationItem.leftBarButtonItem = nil;
    stroll.tabBarItem.image = [[UIImage imageNamed:@"趣逛逛"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    stroll.tabBarItem.selectedImage = [[[UIImage imageNamed:@"趣逛逛"] imageWithColor:selectColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    stroll.tabBarItem.title = @"趣逛逛";
    [self addChildViewController:stroll];
    /** 个人中心 */
    UIViewController *myVC = [NSClassFromString(@"MyController") new];
    YUNavigationController *my = [[YUNavigationController alloc] initWithRootViewController:myVC];
    myVC.navigationItem.leftBarButtonItem = nil;
    my.tabBarItem.image = [[UIImage imageNamed:@"个人中心"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    my.tabBarItem.selectedImage = [[[UIImage imageNamed:@"个人中心"] imageWithColor:selectColor] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    my.tabBarItem.title = @"个人中心";
    [self addChildViewController:my];
}

/** 选中一个Item后的代理 */
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController{
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionTransitionNone animations:^{
        CGRect frame = self.tabBarSelectView.frame;
        frame.origin.x = tabBarController.selectedIndex * (kMainWidth / 5);
        self.tabBarSelectView.frame = frame;
    } completion:nil];
}

@end
