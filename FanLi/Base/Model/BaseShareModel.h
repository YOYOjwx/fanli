//
//  BaseShareModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"
#import <UIKit/UIKit.h>

@interface BaseShareModel : BaseModel

/** 推广介绍 */
@property (nonatomic, strong) NSString *urlContent;
/** 详情介绍 */
@property (nonatomic, strong) NSString *urlDesc;
/** 应用图标 */
@property (nonatomic, strong) NSString *urlIcon;
/** 分享Key，用于区分推广的类型 */
@property (nonatomic, strong) NSString *urlKey;
/** 推广标题 */
@property (nonatomic, strong) NSString *urlTitle;
/** 推广的链接 */
@property (nonatomic, strong) NSString *urlValue;

@end
