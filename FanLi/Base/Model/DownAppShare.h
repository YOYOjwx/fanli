//
//  DownAppShare.h
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseShareModel.h"

@interface DownAppShare : BaseShareModel

+ (instancetype)share;

@end
