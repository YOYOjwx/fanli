//
//  ShareModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseShareModel.h"

@interface ShareModel : BaseShareModel

+ (instancetype)share;

@end
