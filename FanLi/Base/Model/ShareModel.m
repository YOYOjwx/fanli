//
//  ShareModel.m
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ShareModel.h"

@implementation ShareModel

+ (instancetype)share{
    static ShareModel *share = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share = [[super alloc] init];
    });
    return share;
}

@end
