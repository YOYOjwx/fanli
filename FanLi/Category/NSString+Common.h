//
//  NSString+Common.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/27.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Common)

/** 判断url来源 */
- (BOOL)judgeURLSource:(NSString *)urlStr;
/** 判断手机号码 */
- (BOOL)checkPhoneNumber;
/** 判断邮箱 */
- (BOOL)checkEmailAddress;
/** 判断邮政编码 */
- (BOOL)checkZipCode;
/** 加密字段 */
- (NSString*)stringWithMD5;

@end
