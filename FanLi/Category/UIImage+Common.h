//
//  UIImage+Common.h
//  YOYO
//
//  Created by 雨天记忆 on 2017/4/19.
//  Copyright © 2017年 Dorado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Common)

/** 改变图片颜色 */
- (UIImage *)imageWithColor:(UIColor *)color;
/** 根据颜色创建图片 */
+ (UIImage *)imageWithColor:(UIColor *)color;

@end
