//
//  CameraPhoto.h
//  xiangji
//
//  Created by 雨天记忆 on 16/7/21.
//  Copyright © 2016年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface CameraPhoto : UIImagePickerController

//根据sourceType类型选择是创建相机还是相册
- (instancetype)initWithCameraForSourceType:(UIImagePickerControllerSourceType)sourceType success:(void(^)(UIImage *image))success;

@end
