//
//  CameraPhoto.m
//  xiangji
//
//  Created by 雨天记忆 on 16/7/21.
//  Copyright © 2016年 雨天记忆. All rights reserved.
//

#import "CameraPhoto.h"
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

@interface CameraPhoto ()<UIAlertViewDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic, assign) BOOL isRestricted;

@property (nonatomic, copy) void (^success)(UIImage *image);

@end

@implementation CameraPhoto

#pragma mark 创建方法
- (instancetype)initWithCameraForSourceType:(UIImagePickerControllerSourceType)sourceType success:(void (^)(UIImage *))success{
    BOOL isAvailable = NO;
    switch (sourceType) {
        case UIImagePickerControllerSourceTypePhotoLibrary:
        case UIImagePickerControllerSourceTypeSavedPhotosAlbum:{//判断相册是否可用
            isAvailable = [self useOfPhotos];
            break;
        }
        case UIImagePickerControllerSourceTypeCamera:{//判断相机是否可用
            isAvailable = [self useOfCamera];
            break;
        }
    }
    if (!isAvailable) return nil;//如果相机不可用，是限制状态下，会崩溃
    self = [super init];
    if (self) {
        //设备可编辑，选择的照片可以进行剪裁
        self.allowsEditing = YES;
        self.sourceType = sourceType;
        self.delegate = self;
        self.success = success;
    }
    return self;
}

#pragma mark 代理方法
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSURL *url = nil;
    if (self.isRestricted) {
        //表示是访问受限
        url = [NSURL URLWithString:@"prefs:root=General"];
    }else{
        NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
        url = [NSURL URLWithString:[NSString stringWithFormat:@"prefs:root=%@",bundleIdentifier]];
    }
    [[UIApplication sharedApplication] openURL:url];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark 获取照片使用权限
- (BOOL)useOfPhotos{
    //拿到照片的使用权限
    ALAuthorizationStatus author = [ALAssetsLibrary authorizationStatus];
    switch (author) {
        case ALAuthorizationStatusDenied:{//拒绝访问
            self.isRestricted = NO;
            [self alertViewShow:@"照片使用权限未打开"];
            break;
        }
        case ALAuthorizationStatusRestricted:{//限制，可能是家长控制
            self.isRestricted = YES;
            [self alertViewShow:@"照片的访问限制被关闭"];
            return NO;
            break;
        }
        default:break;
    }
    return YES;
}

#pragma mark 获取相机使用权限
- (BOOL)useOfCamera{
    AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    switch (status) {
        case AVAuthorizationStatusDenied:{//否认
            self.isRestricted = NO;
            [self alertViewShow:@"相机使用权限未打开"];
            break;
        }
        case AVAuthorizationStatusRestricted:{//限制，可能是家长控制
            self.isRestricted = YES;
            [self alertViewShow:@"相机的访问限制被关闭"];
            return NO;
            break;
        }
        default:break;
    }
    return YES;
}

#pragma mark 照片处理完成后走的代理方法
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info{
    //取出编辑后的照片
    UIImage *editeImage = info[@"UIImagePickerControllerEditedImage"];
    //取出拍照后原始的照片
    UIImage *originalImage = info[@"UIImagePickerControllerOriginalImage"];
    //如果选择模式是相机，保存拍照后的图片
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera) {
        //保存照片到相册
        UIImageWriteToSavedPhotosAlbum(originalImage, nil, nil, nil);
    }
    UIImage *useImage = nil;
    if (picker.allowsEditing) {//判断图片是否编辑剪裁
        //可以剪裁 使用编辑后的照片
        useImage = editeImage;
    }else{
        //不可以编辑 使用原始的照片
        useImage = originalImage;
    }
    if (self.success) {
        self.success(useImage);
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark 弹出提示框
- (void)alertViewShow:(NSString *)title{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"设置",nil];
    [alert show];
}

@end
