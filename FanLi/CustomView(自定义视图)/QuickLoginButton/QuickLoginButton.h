//
//  QuickLoginButton.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/26.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuickLoginButton : UIButton

@property (nonatomic, strong) UIColor *titleColor;

@end
