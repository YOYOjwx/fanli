//
//  QuickLoginButton.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/26.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "QuickLoginButton.h"

@implementation QuickLoginButton

+ (instancetype)buttonWithType:(UIButtonType)buttonType{
    QuickLoginButton *btn = [super buttonWithType:buttonType];
    btn.layer.borderWidth = 0.5;
    btn.layer.cornerRadius = 4;
    btn.layer.masksToBounds = YES;
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    return btn;
}

- (void)setTitleColor:(UIColor *)titleColor{
    [self setTitleColor:titleColor forState:UIControlStateNormal];
}

- (UIColor *)titleColor{
    return self.titleLabel.textColor;
}

- (void)setTitleColor:(UIColor *)color forState:(UIControlState)state{
    [super setTitleColor:color forState:state];
    self.layer.borderColor = color.CGColor;
}

@end
