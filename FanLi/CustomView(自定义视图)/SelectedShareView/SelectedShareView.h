//
//  SelectedShareView.h
//  OfficeComfortable
//
//  Created by 雨天记忆 on 16/5/5.
//  Copyright © 2016年 sswl-3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TencentOpenAPI/QQApiInterfaceObject.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "QQQuickLogin.h"
#import "WeiboQuickLogin.h"
#import "WXApi.h"

@interface SelectedShareView : UIView<QQApiInterfaceDelegate,WXApiDelegate>

+ (instancetype)share;

+ (void)shareWithURL:(NSString *)url imageUrl:(NSString *)imageUrl title:(NSString *)title description:(NSString *)description array:(NSArray *)array success:(void(^)())success;

@end
