//
//  SelectedShareView.m
//  OfficeComfortable
//
//  Created by 雨天记忆 on 16/5/5.
//  Copyright © 2016年 sswl-3. All rights reserved.
//

#import "SelectedShareView.h"
#import "ShareModel.h"
#import "SDWebImageManager.h"

@interface SelectedShareView ()
{
    float platformWidth;
    float floorViewHeight;
}
//存放支持分享平台的view
@property (nonatomic, strong) UIView *floorView;
/** 数组 */
@property (nonatomic, strong) NSArray *platformTypes;
/** 蒙版 */
@property (nonatomic, strong) UIView *maskView;
/** 分享用的URL */
@property (nonatomic, strong) NSURL *url;
/** 分享用的图片URL */
@property (nonatomic, strong) NSURL *imageUrl;
/** 分享用的标题 */
@property (nonatomic, strong) NSString *title;
/** 分享用的内容 */
@property (nonatomic, strong) NSString *descriptionStr;
/** 分享图片 */
@property (nonatomic, strong) UIImage *image;
/** 分享完成的Block */
@property (nonatomic, copy) void (^success)();

@end

@implementation SelectedShareView

+ (instancetype)share{
    static SelectedShareView *share = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        share = [[super alloc] init];
    });
    return share;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        //添加手势，隐藏当前页面
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (UIView *)floorView{
    if (!_floorView) {
        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGFloat height = [UIScreen mainScreen].bounds.size.height;
        _floorView = [[UIView alloc] initWithFrame:CGRectMake(0, height, width, 0)];
    }
    return _floorView;
}

- (UIView *)maskView{
    if (!_maskView) {
        _maskView = [[UIView alloc] initWithFrame:self.bounds];
        _maskView.backgroundColor = [UIColor blackColor];
        _maskView.alpha = 0.3;
    }
    return _maskView;
}

+ (void)shareWithURL:(NSString *)url imageUrl:(NSString *)imageUrl title:(NSString *)title description:(NSString *)description array:(NSArray *)array success:(void (^)())success{
    SelectedShareView *share = [self share];
    [share initWithPlatformTypes:array];
    share.url = [NSURL URLWithString:url];
    share.imageUrl = [NSURL URLWithString:imageUrl];
    share.title = title;
    share.descriptionStr = description;
    share.success = success;
    [[[UIApplication sharedApplication].delegate window] addSubview:share];
}

- (void)initWithPlatformTypes:(NSArray *)platformTypes{
    self.frame = [UIScreen mainScreen].bounds;
    CGFloat width = [UIScreen mainScreen].bounds.size.width;
    CGFloat height = [UIScreen mainScreen].bounds.size.height;
    //数组赋值
    self.platformTypes = platformTypes;
    //蒙板透明的view
    [self addSubview:self.maskView];
    
    
    //每一个分享平台的宽度和高度是整个屏幕的4/1
    platformWidth = width / 4;
    //底板视图高度默认值
    floorViewHeight = platformWidth;
    
    //根据数组的个数，决定floorView的高度
    NSUInteger count = platformTypes.count - 1 == 0?0:platformTypes.count - 1;
    if (count != 0) {//除以0会崩溃
        floorViewHeight = platformWidth + platformWidth * (count / 4);
    }
    self.floorView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.floorView];
    
    UILabel *title_lb = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, width - 10, 25)];
    title_lb.text = @"分享到";
    title_lb.font = CurrencyFont(12);
    title_lb.textColor = UIColorFromHex(0x999999);
    [self.floorView addSubview:title_lb];
    
    //添加需要分享平台的控件
    for (int i = 0; i < platformTypes.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        //每一个button的宽度
        float buttonWidth = platformWidth / 2;
        //button的控件间隙
        float deviation = (platformWidth  - buttonWidth) / 2;
        //button的Y轴距离
        float buttonY = platformWidth * (i / 4);
        button.frame = CGRectMake(i % 4 * platformWidth + deviation, buttonY + 45, buttonWidth, buttonWidth);
        button.tag = i;
        [button setImage:[UIImage imageNamed:platformTypes[i]] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(platformShare:) forControlEvents:UIControlEventTouchUpInside];
        float lb_y = button.frame.size.height + button.frame.origin.y;
        UILabel *name_lb = [[UILabel alloc] initWithFrame:CGRectMake(i % 4 * platformWidth, lb_y, platformWidth, 25)];
        name_lb.text = platformTypes[i];
        name_lb.textColor = UIColorFromHex(0x666666);
        name_lb.font = CurrencyFont(12);
        name_lb.textAlignment = NSTextAlignmentCenter;
        [self.floorView addSubview:name_lb];
        [self.floorView addSubview:button];
    }
    NSInteger floor_count = platformTypes.count / 4;
    if (platformTypes.count % 4 != 0) floor_count++;
    CGFloat floor_height = floor_count * platformWidth + 40;
    [UIView animateWithDuration:0.5 animations:^{//动画效果
        _floorView.frame = CGRectMake(0, height - floor_height, width,floor_height);
    }];
}

- (void)setImageUrl:(NSURL *)imageUrl{
    _imageUrl = imageUrl;
    [[SDWebImageManager sharedManager].imageDownloader downloadImageWithURL:imageUrl options:0 progress:nil completed:^(UIImage * _Nullable image, NSData * _Nullable data, NSError * _Nullable error, BOOL finished) {
        _image = image;
    }];
}

//点击平台分享实现的方法。
- (void)platformShare:(UIButton *)btn{
    [self dismissView];//关闭动画
    NSString *title = self.platformTypes[btn.tag];
    if ([title isEqualToString:@"QQ"])[self qqAndQZoneShare:0];
    if ([title isEqualToString:@"QQ空间"])[self qqAndQZoneShare:1];
    if ([title isEqualToString:@"微博"])[self weiboShare];
    if ([title isEqualToString:@"微信"])[self weChatShare];
    if ([title isEqualToString:@"朋友圈"])[self friendsShare];
}

/** 关闭动画 */
- (void)dismissView{
    [UIView animateWithDuration:0.5 animations:^{//动画效果
        self.floorView.frame = CGRectMake(0, self.frame.size.height, self.frame.size.width, self.floorView.frame.size.height);
    } completion:^(BOOL finished) {
        //动画播放完毕后，移除当前视图
        for (UIView *view in self.floorView.subviews)[view removeFromSuperview];
        [self removeFromSuperview];
    }];
}

/** 区别是QQ分享还是空间分析 */
- (void)qqAndQZoneShare:(int)type{
    [QQQuickLogin qqRegisted];
    QQApiNewsObject *newsObj = [QQApiNewsObject objectWithURL:self.url title:self.title description:self.descriptionStr previewImageURL:self.imageUrl];
    if (type) {
        [self qZoneShare:[SendMessageToQQReq reqWithContent:newsObj]];
    }else{
        [self qqShare:[SendMessageToQQReq reqWithContent:newsObj]];
    }
}

/** QQ分享 */
- (void)qqShare:(SendMessageToQQReq *)req{
    [QQApiInterface sendReq:req];
}

/** QZone分享 */
- (void)qZoneShare:(SendMessageToQQReq *)req{
    [QQApiInterface SendReqToQZone:req];
}

/** 微博分享 */
- (void)weiboShare{
}

/** 微信分享 */
- (void)weChatShare{
    WXMediaMessage *message = [WXMediaMessage message];
    //标题
    message.title = self.title;
    //内容
    message.description = self.descriptionStr;
    //图片 大小不能超过32k
    [message setThumbImage:_image];
    //创建网页链接
    WXWebpageObject *web = [WXWebpageObject object];
    web.webpageUrl = self.url.absoluteString;
    message.mediaObject = web;
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.message = message;
    req.bText = NO;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

/** 朋友圈分享 */
- (void)friendsShare{
    WXMediaMessage *message = [WXMediaMessage message];
    //标题
    message.title = self.title;
    //内容
    message.description = self.descriptionStr;
    //图片 大小不能超过32k
    [message setThumbImage:_image];
    //创建网页链接
    WXWebpageObject *web = [WXWebpageObject object];
    web.webpageUrl = self.url.absoluteString;
    message.mediaObject = web;
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.message = message;
    req.bText = NO;
    req.scene = WXSceneTimeline;
    [WXApi sendReq:req];
}

/** QQ分享成功 */
- (void)qqShareSuccess{
    
}

/** 处理来至QQ的请求 */
- (void)onReq:(QQBaseReq *)req{
    
}

/** 处理来至QQ的响应  */
- (void)onResp:(QQBaseResp *)resp{
    /** 代表是QQ分享过来的 */
    if ([resp.class.description isEqualToString:@"SendMessageToQQResp"]) {
        /** QQ分享成功 */
        if ([resp.result isEqualToString:@"0"]) {
            if (self.success) {
                self.success();
            }
        }
    }else{
        /** 这里就是微信分享过来的 */
        BaseResp *wxResp = (BaseResp *)resp;
        /** 微信分享成功 */
        if (wxResp.errCode == 0) {
            if (self.success) {
                self.success();
            }
        }
    }
}

/** 处理QQ在线状态的回调 */
- (void)isOnlineResponse:(NSDictionary *)response{
    
}

@end
