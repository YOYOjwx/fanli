//
//  YUAlertController.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUAlertController : UIAlertController

/** alert类型才会使用 */
@property (nonatomic, strong) NSString *content;
/** 是否为数字键盘 */
@property (nonatomic, assign) BOOL isNumber;
/** 根据数组创建多个action sheet类型才会有效 */
- (void)addSubActions:(NSArray<NSString *> *)subAry;
/** 点击返回Block */
- (void)selectionResults:(void(^)(NSString *content,NSInteger index))selectionResults;

@end
