//
//  YUAlertController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YUAlertController.h"

@interface YUAlertController ()

/** 添加的textField */
@property (nonatomic, strong) UITextField *textField;
/** 确认按钮Action */
@property (nonatomic, strong) UIAlertAction *determine;
/** 点击返回的Block */
@property (nonatomic, copy) void (^selectionResults)(NSString *content,NSInteger index);

@end

@implementation YUAlertController

+ (instancetype)alertControllerWithTitle:(NSString *)title message:(NSString *)message preferredStyle:(UIAlertControllerStyle)preferredStyle{
    YUAlertController *alert = [super alertControllerWithTitle:title message:message preferredStyle:preferredStyle];
    if (preferredStyle == UIAlertControllerStyleAlert) {
        [alert addTextField];
    }
    return alert;
}

/** 根据数组添加action */
- (void)addSubActions:(NSArray<NSString *> *)subAry{
    if (self.preferredStyle == UIAlertControllerStyleAlert) return;
    /** 创建取消的按钮 */
    [self addCancelAction];
    /** 创建数组内的action */
    WS(weakSelf);
    for (NSString *title in subAry) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:title style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            [weakSelf actionBlock:action index:[subAry indexOfObject:title]];
        }];
        [self addAction:action];
    }
}

/** 添加文本输入框 */
- (void)addTextField{
    WS(weakSelf);
    [self addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        weakSelf.textField = textField;
    }];
    /** 添加actions */
    [self addCancelAction];
    [self addDetermineAction];
}

/** 添加取消Action */
- (void)addCancelAction{
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:nil];
    [self addAction:cancel];
}

/** 添加确定Action */
- (void)addDetermineAction{
    WS(weakSelf);
    self.determine = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [weakSelf actionBlock:action index:0];
    }];
    [self addAction:self.determine];
}

/** actionBlock的实现 */
- (void)actionBlock:(UIAlertAction *)action index:(NSInteger)index{
    NSString *content = action.title;
    if (self.preferredStyle == UIAlertControllerStyleAlert) {
        content = self.textField.text;
    }
    if (self.selectionResults) {
        self.selectionResults(content,index);
    }
}

/** block赋值 */
- (void)selectionResults:(void (^)(NSString *, NSInteger))selectionResults{
    _selectionResults = selectionResults;
}

- (void)setContent:(NSString *)content{
    if (_isNumber) {
        content = [NSString stringWithFormat:@"%d",content.intValue];
    }
    self.textField.text = content;
}

- (NSString *)content{
    return self.textField.text;
}

- (void)setIsNumber:(BOOL)isNumber{
    _isNumber = isNumber;
    if (isNumber) {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.content = self.content;
    }
}

@end
