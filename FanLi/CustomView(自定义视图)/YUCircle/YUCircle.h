//
//  YUCircle.h
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUCircle : UIView

@property (nonatomic, assign) float progress;

@end
