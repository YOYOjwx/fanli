//
//  YUCircle.m
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUCircle.h"

@implementation YUCircle

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(context, 0.5);
    CGContextSetLineCap(context, kCGLineCapRound);
    CGContextSetStrokeColorWithColor(context, UIColorFromHex(0xFF5E56).CGColor);
    CGFloat startAngle = -M_PI / 3;
    CGFloat step = 11 * M_PI / 6 * self.progress;
    CGContextAddArc(context, self.bounds.size.width / 2, self.bounds.size.height / 2, self.bounds.size.width / 2 - 3, startAngle, startAngle+step, 0);
    CGContextStrokePath(context);
}

@end
