//
//  YUPickerView.h
//  YOYO6.0
//
//  Created by dianyun on 2017/5/11.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCellModel.h"

@interface YUPickerView : UIView

/** 选择器的类型 */
@property (nonatomic, strong) SetCellModel *model;

@property (nonatomic, copy) void (^determine)(NSString *location);
    
@end
