//
//  YUPickerView.m
//  YOYO6.0
//
//  Created by dianyun on 2017/5/11.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YUPickerView.h"

@interface YUPickerView ()<UIPickerViewDelegate,UIPickerViewDataSource>
{
    /** 市 */
    NSInteger _city;
    /** 省 */
    NSInteger _province;
    /** 区 */
    NSInteger _area;
}
/** 背景视图 */
@property (nonatomic, strong) UIView *bg_view;
/** titleLabel */
@property (nonatomic, strong) UILabel *title_lb;
/** 日期选择器 */
@property (nonatomic, strong) UIDatePicker *date_picker;
/** 地区选择器 */
@property (nonatomic, strong) UIPickerView *city_picker;;
/** cityArray */
@property (nonatomic, strong) NSArray *array;

@end

@implementation YUPickerView

- (NSArray *)array{
    if (!_array) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"];
        _array = [NSArray arrayWithContentsOfFile:path];
    }
    return _array;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        self.backgroundColor = [UIColor clearColor];
        /** 创建背景视图 */
        self.bg_view = [UIView new];
        self.bg_view.backgroundColor = [UIColor whiteColor];
        [self addSubview:self.bg_view];
        [self.bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(@0);
            make.height.equalTo(@220);
        }];
        /** 创建titleView */
        UIView *title_view = [UIView new];
        [self addSubview:title_view];
        title_view.backgroundColor = UIColorFromHex(0xF4F4F4);
        [title_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(@0);
            make.bottom.equalTo(weakSelf.bg_view.mas_top);
            make.height.equalTo(@40);
        }];
        /** 创建titleLabel */
        self.title_lb = [UILabel new];
        [title_view addSubview:self.title_lb];
        self.title_lb.font = [UIFont boldSystemFontOfSize:14];
        self.title_lb.textColor = UIColorFromHex(0x555555);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
        /** 创建cancleBtn */
        UIButton *cancle_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [title_view addSubview:cancle_btn];
        [cancle_btn setTitle:@"取消" forState:UIControlStateNormal];
        cancle_btn.layer.masksToBounds = YES;
        cancle_btn.layer.cornerRadius = 4;
        cancle_btn.layer.borderWidth = 1;
        cancle_btn.titleLabel.font = CurrencyFont(14);
        [cancle_btn setTitleColor:UIColorFromHex(0x555555) forState:UIControlStateNormal];
        cancle_btn.layer.borderColor = UIColorFromHex(0x555555).CGColor;
        [cancle_btn addTarget:self action:@selector(disMissSelf) forControlEvents:UIControlEventTouchUpInside];
        [cancle_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@10);
            make.top.equalTo(@6);
            make.bottom.equalTo(@-6);
            make.width.equalTo(cancle_btn.mas_height).multipliedBy(1.95);
        }];
        /** 创建determineBtn */
        UIButton *determine_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [title_view addSubview:determine_btn];
        [determine_btn setTitle:@"完成" forState:UIControlStateNormal];
        determine_btn.layer.masksToBounds = YES;
        determine_btn.layer.cornerRadius = 4;
        determine_btn.layer.borderWidth = 1;
        determine_btn.titleLabel.font = CurrencyFont(14);
        [determine_btn setTitleColor:UIColorFromHex(0x555555) forState:UIControlStateNormal];
        determine_btn.layer.borderColor = UIColorFromHex(0x555555).CGColor;
        [determine_btn addTarget:self action:@selector(finish) forControlEvents:UIControlEventTouchUpInside];
        [determine_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(@-10);
            make.top.bottom.width.height.equalTo(cancle_btn);
        }];
        /** 创建蒙版视图 */
        UIView *masking_view = [UIView new];
        [self addSubview:masking_view];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(disMissSelf)];
        [masking_view addGestureRecognizer:tap];
        masking_view.backgroundColor = [UIColor blackColor];
        masking_view.alpha = 0.3;
        [masking_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.bottom.equalTo(title_view.mas_top);
        }];
    }
    return self;
}

/** 开始动画 */
- (void)beganAnimation{
    [self.bg_view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@0);
    }];
    [UIView animateWithDuration:0.5 animations:^{
        [self.bg_view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
    }];
}

/** 视图消失 */
- (void)disMissSelf{
    [self.bg_view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@220);
    }];
    [UIView animateWithDuration:0.5 animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

/** 完成 */
- (void)finish{
    NSString *province = self.array[_province][@"text"];
    NSString *city = self.array[_province][@"cities"][_city][@"text"];
    NSString *county = self.array[_province][@"cities"][_city][@"cities"][_area];
    NSString *location = nil;
    if ([province isEqualToString:city]) {
        location = [city stringByAppendingFormat:@"-%@",county];
    }else{
        location = [province stringByAppendingFormat:@"-%@-%@",city,county];
    }
    if (self.determine) {
        self.determine(location);
    }
    [self disMissSelf];
}

- (void)setModel:(SetCellModel *)model{
    _model = model;
    self.title_lb.text = model.title;
    if (model.pickerType == PickerTypeCity) {
        self.city_picker = [UIPickerView new];
        [self.bg_view addSubview:self.city_picker];
        self.city_picker.delegate = self;
        self.city_picker.dataSource = self;
        self.city_picker.showsSelectionIndicator = YES;
        [self.city_picker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        [self.date_picker removeFromSuperview];
        self.date_picker = nil;
    }else{
        self.date_picker = [UIDatePicker new];
        [self.bg_view addSubview:self.date_picker];
        self.date_picker.datePickerMode = UIDatePickerModeDate;
        [self.date_picker mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        [self.city_picker removeFromSuperview];
        self.city_picker = nil;
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    switch (component) {
        case 0:return self.array.count;
        case 1:return [self.array[_province][@"cities"] count];
        case 2:{
            if ([self.array[_province][@"cities"][_city][@"cities"] isKindOfClass:[NSArray class]]) {
                return [self.array[_province][@"cities"][_city][@"cities"] count];
            }
        }
    }
    return 0;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (component == 0) {
        return self.array[row][@"text"];
    }
    if (component == 1) {
        return self.array[_province][@"cities"][row][@"text"];
    }
    return self.array[_province][@"cities"][_city][@"cities"][row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        _province = row;
    }
    if (component == 1) {
        _city = row;
    }
    if (component == 2) {
        _area = row;
    }
    [pickerView reloadAllComponents];
}

@end
