//
//  YUProgress.h
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUProgress : UIView

/** 当前进度 */
@property (nonatomic, assign) int progress;
/** 进度条颜色 */
@property (nonatomic, strong) UIColor *progressColor;
/** 未完成的颜色 */
@property (nonatomic, strong) UIColor *normalColor;

@end
