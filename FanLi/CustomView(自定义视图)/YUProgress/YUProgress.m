//
//  YUProgress.m
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUProgress.h"

@interface YUProgress ()

/** 当前进度 */
@property (nonatomic, strong) UIView *progress_view;

@end

@implementation YUProgress

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        _progressColor = UIColorFromHex(0xFF5E56);
        _normalColor = [UIColor whiteColor];
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 2.5;
        self.layer.borderWidth = 0.5;
        self.layer.borderColor = _progressColor.CGColor;
        /** 创建当前进度 */
        self.progress_view = [UIView new];
        [self addSubview:self.progress_view];
        self.progress_view.backgroundColor = _progressColor;
        [self.progress_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.bottom.equalTo(@0);
            make.width.equalTo(@0);
        }];
    }
    return self;
}

- (void)setProgress:(int)progress{
    _progress = progress;
    [_progress_view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.bottom.equalTo(@0);
        make.width.equalTo(@0);
        make.width.equalTo(self.mas_width).multipliedBy((float)progress / 100.f);
    }];
}

- (void)setProgressColor:(UIColor *)progressColor{
    _progressColor = progressColor;
    self.progress_view.backgroundColor = _progressColor;
    self.layer.borderColor = _progressColor.CGColor;
}

- (void)setNormalColor:(UIColor *)normalColor{
    _normalColor = normalColor;
    self.backgroundColor = normalColor;
}

@end
