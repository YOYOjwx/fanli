//
//  YUSegmented.h
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUSegmented : UIView

- (instancetype)initWithArray:(NSArray<NSString *> *)array column:(int)column click:(void(^)(NSInteger index))click;
/** 选中后的颜色 */
@property (nonatomic, strong) UIColor *selectColor;
/** 普通状态下的颜色 */
@property (nonatomic, strong) UIColor *normalColor;
/** 当前选中第几个item */
@property (nonatomic, assign) NSInteger selectIndex;

@end

@interface YUSegmentedItem : UIView

@property (nonatomic, strong) UILabel *title_lb;

@end
