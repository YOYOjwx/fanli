//
//  YUSegmented.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUSegmented.h"

@interface YUSegmented ()<UIScrollViewDelegate>
/** scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;
/** 下面跟随滑动视图 */
@property (nonatomic, strong) UIView *bottom_view;
/** 存放item的数据 */
@property (nonatomic, strong) NSMutableArray *itemAry;
/** 点击后执行的block */
@property (nonatomic, copy) void (^click)(NSInteger index);
/** 总共有多少列 */
@property (nonatomic, assign) int column;

@end

@implementation YUSegmented

- (NSMutableArray *)itemAry{
    if (!_itemAry) {
        _itemAry = [NSMutableArray array];
    }
    return _itemAry;
}

- (instancetype)initWithArray:(NSArray<NSString *> *)array column:(int)column click:(void (^)(NSInteger))click{
    self = [super init];
    if (self) {
        if (array == nil) return self;
        self.click = click;
        /** 设置颜色初始值 */
        self.column = column;
        self.backgroundColor = [UIColor whiteColor];
        _selectColor = UIColorFromHex(0xFF5E56);
        _normalColor = UIColorFromHex(0x666666);
        WS(weakSelf);
        self.scrollView = [UIScrollView new];
        [self addSubview:self.scrollView];
        self.scrollView.delegate = self;
        self.scrollView.showsVerticalScrollIndicator = NO;
        self.scrollView.showsHorizontalScrollIndicator = NO;
        [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        UIView *contentView = [UIView new];
        [self.scrollView addSubview:contentView];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.height.equalTo(@0);
        }];
        for (NSString *title in array) {
            NSInteger index = [array indexOfObject:title];
            YUSegmentedItem *item = [YUSegmentedItem new];
            [self.scrollView addSubview:item];
            [self.itemAry addObject:item];
            item.title_lb.text = title;
            item.title_lb.textColor = _normalColor;
            item.tag = index + 517;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView:)];
            [item addGestureRecognizer:tap];
            [item mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(@0);
                make.height.equalTo(weakSelf);
                make.width.mas_equalTo(kMainWidth / column);
                if (index == 0) {
                    make.leading.equalTo(@0);
                    item.title_lb.textColor = _selectColor;
                }else{
                    YUSegmentedItem *topItem = weakSelf.itemAry[index - 1];
                    make.leading.equalTo(topItem.mas_trailing);
                }
                if (index == array.count - 1) {
                    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
                        make.trailing.equalTo(item.mas_trailing);
                    }];
                }
            }];
        }
        YUSegmentedItem *firstItem = self.itemAry.firstObject;
        /** 创建底部条 */
        self.bottom_view = [UIView new];
        [self.scrollView addSubview:self.bottom_view];
        self.bottom_view.backgroundColor = _selectColor;
        [self.bottom_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf).offset(-3);
            make.height.equalTo(@3);
            make.centerX.equalTo(firstItem);
            make.width.equalTo(firstItem.title_lb).multipliedBy(1.2);
        }];
    }
    return self;
}

- (void)tapView:(UITapGestureRecognizer *)tap{
    YUSegmentedItem *item = (YUSegmentedItem *)tap.view;
    /** 获取点击第几个itme */
    NSInteger index = item.tag - 517;
    if (_selectIndex == index) return;
    [self changeItmeState:index];
    if (self.click) {
        self.click(_selectIndex);
    }
}

- (void)changeItmeState:(NSInteger)selectIndex{
    YUSegmentedItem *item = self.itemAry[selectIndex];
    /** 改变点击颜色 */
    for (YUSegmentedItem *itme in self.itemAry) {
        itme.title_lb.textColor = _normalColor;
    }
    item.title_lb.textColor = _selectColor;
    /** 记录点击第几个item */
    _selectIndex = selectIndex;
    if (selectIndex == 0) {
        [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }else if (selectIndex == self.itemAry.count - 1){
        [self.scrollView setContentOffset:CGPointMake(self.scrollView.contentSize.width - self.frame.size.width, 0) animated:YES];
    }else{
        [self.scrollView setContentOffset:CGPointMake(kMainWidth / self.column * (selectIndex - 1), 0) animated:YES];
    }
    [self.bottom_view mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self).offset(-3);
        make.height.equalTo(@3);
        make.centerX.equalTo(item);
        make.width.equalTo(item.title_lb).multipliedBy(1.2);
    }];
    /** 动画 */
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionTransitionNone animations:^{
        [self layoutIfNeeded];
    } completion:nil];
}

- (void)setSelectIndex:(NSInteger)selectIndex{
    _selectIndex = selectIndex;
    [self changeItmeState:selectIndex];
}

/** 设置选中后的颜色 */
- (void)setSelectColor:(UIColor *)selectColor{
    _selectColor = selectColor;
    /** 刷新当前颜色 */
    [self reloadColor];
}

/** 设置普通状态下的颜色 */
- (void)setNormalColor:(UIColor *)normalColor{
    _normalColor = normalColor;
    /** 刷新当前颜色 */
    [self reloadColor];
}

/** 刷新当前颜色 */
- (void)reloadColor{
    for (YUSegmentedItem *item in self.itemAry) {
        NSInteger index = [self.itemAry indexOfObject:item];
        if (index == _selectIndex) {
            item.title_lb.textColor = self.selectColor;
        }else{
            item.title_lb.textColor = self.normalColor;
        }
    }
    self.bottom_view.backgroundColor = self.selectColor;
}

@end

@implementation YUSegmentedItem

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.title_lb = [UILabel new];
        [self addSubview:self.title_lb];
        self.title_lb.font = [UIFont systemFontOfSize:14];
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
    }
    return self;
}

@end
