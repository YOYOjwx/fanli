//
//  HomeController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "HomeController.h"
#import "YUSegmented.h"
#import "DiscountCell.h"
#import "DateCell.h"
#import "XRWaterfallLayout.h"
#import "EarnCoinsHeader.h"
#import "ListModel.h"
#import "YURefreshHeader.h"
#import "SearchController.h"
#import "YUWebController.h"
#import "TaobaoDetail.h"
#import "ExchangeSuccessController.h"

@interface HomeController ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** collectionView */
@property (nonatomic, strong) UICollectionView *collectionView;
/** 选择菜单控件 */
@property (nonatomic, strong) YUSegmented *segmented;
/** 视图头部 */
@property (nonatomic, strong) EarnCoinsHeader *header;
/** 存放菜单数组 */
@property (nonatomic, strong) NSMutableArray *menusArray;
/** 存放商品字典 */
@property (nonatomic, strong) NSMutableDictionary *dic;
/** 存放当前选择的哪一个菜单 */
@property (nonatomic, strong) NSString *menuStr;
/** 购物车按钮 */
@property (nonatomic, strong) UIButton *shopping_cart_btn;

@end

@implementation HomeController

- (NSMutableDictionary *)dic{
    if (!_dic) {
        _dic = [NSMutableDictionary dictionary];
        self.menuStr = @"";
    }
    return _dic;
}

- (NSMutableArray *)menusArray{
    if (!_menusArray) {
        _menusArray = [NSMutableArray array];
    }
    return _menusArray;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 创建子视图 */
    [self creatSubViews];
}

- (void)creatSubViews{
    WS(weakSelf);
    /** 创建collectionView */
    XRWaterfallLayout *layout = [XRWaterfallLayout waterFallLayoutWithColumnCount:2];
    layout.columnSpacing = 5;
    layout.rowSpacing = 5;
    layout.itemHeightBlock = ^CGFloat(CGFloat itemWidth, NSIndexPath *indexPath) {
        if (indexPath.row) {
            return itemWidth + 88.5;
        }else{
            return 70;
        }
    };
    layout.sectionInset = UIEdgeInsetsMake(kMainWidth * 0.53 + 40, 0, 0, 0);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.alwaysBounceVertical = YES;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.leading.top.bottom.equalTo(@0);
    }];
    /** 注册cell */
    [self.collectionView registerClass:[DiscountCell class] forCellWithReuseIdentifier:@"DiscountCell"];
    [self.collectionView registerClass:[DateCell class] forCellWithReuseIdentifier:@"DateCell"];
    [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    
    self.header = [[EarnCoinsHeader alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainWidth * 0.53)];
    [self.collectionView addSubview:self.header];
    _header.url = @"openApi/category/focus";
    UIImageView *image_view = [UIImageView new];
    [_header addSubview:image_view];
    _header.click = ^(CateItemModel *model) {
        [TaobaoDetail openCycleDetail:weakSelf.navigationController cateItem:model];
    };
    image_view.image = [UIImage imageNamed:@"首页黑渐变透明"];
    [image_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    [self getMenus];
    /** 设置下啦刷新 */
    self.collectionView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.dic removeObjectForKey:self.menuStr];
        [weakSelf getListOfGoods];
    }];
    /** 设置item */
    UIButton *task_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [task_btn setImage:[UIImage imageNamed:@"home_task"] forState:UIControlStateNormal];
    [task_btn setTitle:@" 任务" forState:UIControlStateNormal];
    task_btn.titleLabel.font = [UIFont boldSystemFontOfSize:12];
    [task_btn setTitleColor:[UIColor colorWithRed:1 green:0.65 blue:0 alpha:1] forState:UIControlStateNormal];
    [task_btn addTarget:self action:@selector(pushTaskWeb) forControlEvents:UIControlEventTouchUpInside];
    task_btn.frame = CGRectMake(0, 0, 55, 24);
    UIBarButtonItem *task_item = [[UIBarButtonItem alloc] initWithCustomView:task_btn];
    self.navigationItem.rightBarButtonItem = task_item;
    
    UIView *search_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth - 90, 30)];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushSearchVC)];
    [search_view addGestureRecognizer:tap];
    UISearchBar *search = [UISearchBar new];
    [search_view addSubview:search];
    search.searchBarStyle = UIBarStyleBlackTranslucent;
    search.placeholder = @"复制商品标题，拿返利更方便";
    search.userInteractionEnabled = NO;
    search.layer.masksToBounds = YES;
    search.layer.cornerRadius = 15;
    [search mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.top.bottom.equalTo(@0);
    }];
    self.navigationItem.titleView = search_view;
    /** 创建购物车 */
    self.shopping_cart_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.shopping_cart_btn];
    [self.shopping_cart_btn setTitle:@" 购物车" forState:UIControlStateNormal];
    [self.shopping_cart_btn setImage:[UIImage imageNamed:@"购物车"] forState:UIControlStateNormal];
    self.shopping_cart_btn.titleLabel.font = CurrencyFont(14);
    self.shopping_cart_btn.backgroundColor = RGBCOLOR(0, 0, 0, 0.4);
    self.shopping_cart_btn.layer.masksToBounds = YES;
    self.shopping_cart_btn.layer.cornerRadius = 4;
    [self.shopping_cart_btn addTarget:self action:@selector(shoppingCart) forControlEvents:UIControlEventTouchUpInside];
    [self.shopping_cart_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.bottom.equalTo(@-10);
        make.width.equalTo(@80);
        make.height.equalTo(@35);
    }];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dic[self.menuStr] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.item) {
        DiscountCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DiscountCell" forIndexPath:indexPath];
        cell.model = self.dic[self.menuStr][indexPath.item - 1];
        return cell;
    }else{
        DateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DateCell" forIndexPath:indexPath];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CateItemModel *model = self.dic[self.menuStr][indexPath.item - 1];
    NSString *itemID = nil;
    if (model.saveMoneyUrl.length) {
        itemID = model.saveMoneyUrl;
    }else{
        itemID = model.tbItemId;
    }
    [TaobaoDetail showTaobaoDetail:self itemID:itemID pid:model.taobaoPid success:^(AlibcTradeResult *result) {
        
    } failure:nil];
}

- (void)shoppingCart{
    [TaobaoDetail openShoppingCart:self success:^(AlibcTradeResult *result) {
        ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
        esc.navigation_title = @"支付成功";
        esc.success_title = @"购买商品支付成功";
        esc.btn_name = @"查看订单";
        esc.hidesBottomBarWhenPushed = YES;
        esc.pushController = @"TaobaoOrderController";
        [self.navigationController pushViewController:esc animated:YES];
    } failure:nil];
}

- (void)pushSearchVC{
    SearchController *sc = [[SearchController alloc] init];
    sc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sc animated:NO];
}

- (void)pushTaskWeb{
    YUWebController *orderVC = [[YUWebController alloc] init];
    orderVC.url = [NSURL URLWithString:OrderDetailURL];
    orderVC.title_string = @"下单任务";
    UIBarButtonItem *rule_item = [[UIBarButtonItem alloc] initWithTitle:@"规则" style:UIBarButtonItemStyleDone target:self action:@selector(pushRuleWebView)];
    rule_item.tintColor = [UIColor blackColor];
    [rule_item setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    orderVC.rightBarButtonItems = @[rule_item];
    orderVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderVC animated:YES];
}

- (void)pushRuleWebView{
    YUWebController *orderVC = self.navigationController.childViewControllers.lastObject;
    YUWebController *ruleVC = [[YUWebController alloc] init];
    ruleVC.title_string = @"下单任务细则";
    ruleVC.url = [NSURL URLWithString:OrderTaskRuleURL];
    [orderVC.navigationController pushViewController:ruleVC animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = scrollView.contentOffset.y;
    if (y > kMainWidth * 0.53) {
        [self.segmented mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.leading.equalTo(_header);
            make.height.equalTo(@40);
            make.top.equalTo(self.view);
        }];
    }else{
        [self.segmented mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.width.leading.equalTo(_header);
            make.height.equalTo(@40);
            make.top.equalTo(_header.mas_bottom);
        }];
    }
}

/** 获取二级菜单 */
- (void)getMenus{
    [[YURequest creatMarketRequest] POST:@"openApi/category/secondMenus" parameters:@{@"xid":@"697"} success:^(id JSON) {
        NSMutableArray *ary = [NSMutableArray array];
        for (NSDictionary *dic in JSON) {
            ListModel *model = [[ListModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.menusArray addObject:model];
            [ary addObject:model.name];
        }
        self.segmented = [[YUSegmented alloc] initWithArray:ary column:3 click:^(NSInteger index) {
            ListModel *model = self.menusArray[index];
            self.menuStr = model.cateId.stringValue;
            NSMutableArray *array = self.dic[self.menuStr];
            if (array) {
                [self.collectionView reloadData];
            }else{
                [self getListOfGoods];
            }
        }];
        ListModel *model = self.menusArray.firstObject;
        self.menuStr = model.cateId.stringValue;
        [self getListOfGoods];
        [self.collectionView addSubview:self.segmented];
        [self.segmented mas_makeConstraints:^(MASConstraintMaker *make) {
            make.width.leading.equalTo(_header);
            make.height.equalTo(@40);
            make.top.equalTo(_header.mas_bottom);
        }];
        
    } failure:nil];
}

/** 获取商品列表 */
- (void)getListOfGoods{
    NSInteger curPage = [self.dic[self.menuStr] count] / 20 + 1;
    [[YURequest creatMarketRequest] POST:@"openApi/category/detail" parameters:@{@"xid":self.menuStr,@"curPage":@(curPage)} success:^(id JSON) {
        NSMutableArray *array = self.dic[self.menuStr];
        if (!array) array = [NSMutableArray array];
        for (NSDictionary *dic in JSON) {
            CateItemModel *model = [[CateItemModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [array addObject:model];
        }
        /** 这里判断是否还需要上拉加载更多 */
        if (array.count % 20 == 0 || array.count != 0) {
            self.collectionView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(getListOfGoods)];
        }else{
            self.collectionView.mj_footer = nil;
        }
        [self.dic setValue:array forKey:self.menuStr];
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

@end
