//
//  CateItemModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"
typedef NS_ENUM (int, ClickType) {
    /** 不可点击 */
    ClickTypeNone = 0,
    /** 专辑列表 */
    ClickTypeBrand,
    /** 专辑详情 */
    ClickTypeBrandDetail,
    /** 分类(商品列表) */
    ClickTypeCategory,
    /** 网页 */
    ClickTypeWebView,
    /** 淘宝SDK打开 */
    ClickTypeTAESdk,
    /** 金币商城 */
    ClickTypeCoinsMarket,
    /** 邀请好友 */
    ClickTypeInviteFriend,
    /** 帖子详情 */
    ClickTypeTopicDetail,
    /** 跳品牌汇 */
    ClickTypeVarietyGather
};

@interface CateItemModel : BaseModel

/** 分类id */
@property (nonatomic, assign) int cateId;
/** 淘宝商品混淆ID */
@property (nonatomic, strong) NSString *tbItemId;
/** 卖家昵称 */
@property (nonatomic, strong) NSString *sellerNick;
/** 商品图片，目前为一张，考虑向后兼容，放在一个list里面 */
@property (nonatomic, strong) NSArray<NSString *> *pics;
/** 轮播图的图片 */
@property (nonatomic, strong) NSString *pic;
/** 商品名称 */
@property (nonatomic, strong) NSString *title;
/** 商品原价，保留2位小数 */
@property (nonatomic, strong) NSString *price;
/** 商品促销价，保留2位小数 */
@property (nonatomic, strong) NSString *umpPrice;
/** 优惠券面值 */
@property (nonatomic, strong) NSString *youhuiPrice;
/** 商品状态 0:下架 1:可用 */
@property (nonatomic, assign) int status;
/** 返金币数量 */
@property (nonatomic, assign) int coins;
/** 1:集市店  2:商城 */
@property (nonatomic, assign) int itemType;
/** 是否为淘客商品 */
@property (nonatomic, assign) BOOL isTk;
/** 淘宝客pid */
@property (nonatomic, strong) NSString *taobaoPid;
/** 发货地 */
@property (nonatomic, strong) NSString *location;
/** 月销量 */
@property (nonatomic, assign) int monthlySales;
/** 点击类型 */
@property (nonatomic, assign) ClickType clickType;
/** 只有clickType=4时用到clickUrl网页跳转地址（clickType=8时，clickUrl：帖子topicId） */
@property (nonatomic, strong) NSString *clickUrl;
/** 商品或专辑剩余下架时间 */
@property (nonatomic, assign) int leftTime;
/** 是否收藏 */
@property (nonatomic, assign) BOOL fav;
/** 商品被收藏数目 */
@property (nonatomic, assign) int favorites;
/** 库存数量 */
@property (nonatomic, assign) int stockCount;
/** 商品限购类型，0：普通商品，1：库存限购商品 */
@property (nonatomic, assign) int limitType;
/** 是否售罄 */
@property (nonatomic, assign) BOOL sellOut;
/** 优惠卷url,当该值为null或者空串时，不显示领优惠卷按钮 */
@property (nonatomic, strong) NSString *saveMoneyUrl;

@end
