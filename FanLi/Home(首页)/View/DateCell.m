//
//  DateCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/27.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "DateCell.h"
#import "CalendarModel.h"

@interface DateCell ()

@end

@implementation DateCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        self.contentView.backgroundColor = [UIColor whiteColor];
        /** 设置cell自动宽高的约束 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.equalTo(@0);
            make.width.mas_equalTo((kMainWidth - 5) / 2);
            make.height.equalTo(@70);
        }];
        UIImageView *icon_image = [UIImageView new];
        [self.contentView addSubview:icon_image];
        icon_image.image = [UIImage imageNamed:@"更新"];
        [icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(@20);
            make.width.height.equalTo(@12);
        }];
        UILabel *hour_lb = [UILabel new];
        [self.contentView addSubview:hour_lb];
        hour_lb.text = @"10:00";
        hour_lb.font = [UIFont boldSystemFontOfSize:14];
        [hour_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(icon_image.mas_trailing).offset(5);
        }];
        UIImageView *line_image = [UIImageView new];
        [self.contentView addSubview:line_image];
        line_image.image = [UIImage imageNamed:@"首页虚线"];
        [line_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@12);
            make.bottom.equalTo(@-12);
            make.leading.equalTo(hour_lb.mas_trailing).offset(20);
        }];
        CalendarModel *calendar = [[CalendarModel alloc] initWithDate:[NSDate date]];
        UILabel *date_lb = [UILabel new];
        [self.contentView addSubview:date_lb];
        date_lb.textColor = UIColorFromHex(0x999999);
        date_lb.font = CurrencyFont(12);
        date_lb.text = [NSString stringWithFormat:@"%@/%@\n%@",calendar.month,calendar.day,calendar.week];
        date_lb.numberOfLines = 0;
        date_lb.textAlignment = NSTextAlignmentCenter;
        [date_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(line_image.mas_trailing);
            make.trailing.equalTo(@0);
            make.centerY.equalTo(weakSelf.mas_centerY);
        }];
    }
    return self;
}

@end
