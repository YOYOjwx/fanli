//
//  DiscountCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "DiscountCell.h"

@interface DiscountCell ()

/** 商品图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 收藏按钮 */
@property (nonatomic, strong) UIButton *fllow_btn;
/** 原价数字 */
@property (nonatomic, strong) UILabel *origin_money_lb;
/** 商品介绍内容 */
@property (nonatomic, strong) UILabel *content_lb;
/** 领省劵按钮 */
@property (nonatomic, strong) UIButton *coupon_btn;
/** 省多少钱 */
@property (nonatomic, strong) UILabel *coupon_money_lb;

@end

@implementation DiscountCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        self.contentView.layer.masksToBounds = YES;
        self.contentView.backgroundColor = [UIColor whiteColor];
        /** 创建商品图片 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.trailing.equalTo(@0);
            make.height.equalTo(weakSelf.icon_image.mas_width);
        }];
        /** 创建收藏按钮 */
        self.fllow_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.fllow_btn];
        self.fllow_btn.backgroundColor = RGBCOLOR(0, 0, 0, 0.3);
        self.fllow_btn.layer.masksToBounds = YES;
        self.fllow_btn.layer.cornerRadius = 12.5;
        [self.fllow_btn setImage:[UIImage imageNamed:@"favorite"] forState:UIControlStateNormal];
        [self.fllow_btn setImage:[UIImage imageNamed:@"favorite-click"] forState:UIControlStateSelected];
        [self.fllow_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.fllow_btn addTarget:self action:@selector(fllow:) forControlEvents:UIControlEventTouchUpInside];
        self.fllow_btn.titleLabel.font = CurrencyFont(10);
        [self.fllow_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@10);
            make.trailing.equalTo(@-10);
            make.height.equalTo(@25);
            make.width.equalTo(@60);
        }];
        /** 创建原价数字 */
        UIView *origin_money_view = [UIView new];
        [self.contentView addSubview:origin_money_view];
        origin_money_view.backgroundColor = RGBCOLOR(85, 85, 85, 0.7);
        self.origin_money_lb = [UILabel new];
        [origin_money_view addSubview:self.origin_money_lb];
        self.origin_money_lb.font = CurrencyFont(12);
        self.origin_money_lb.textColor = [UIColor whiteColor];
        [self.origin_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
        [origin_money_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.bottom.equalTo(weakSelf.icon_image).offset(-10);
            make.width.equalTo(weakSelf.origin_money_lb).multipliedBy(1.2);
            make.height.equalTo(weakSelf.origin_money_lb).multipliedBy(1.4);
        }];
        origin_money_view.layer.masksToBounds = YES;
        origin_money_view.layer.cornerRadius = 10;
        /** 创建商品介绍 */
        self.content_lb = [UILabel new];
        [self.contentView addSubview:self.content_lb];
        self.content_lb.font = CurrencyFont(14);
        self.content_lb.numberOfLines = 2;
        [self.content_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@10);
            make.trailing.equalTo(@-10);
            make.top.equalTo(weakSelf.icon_image.mas_bottom).offset(10);
        }];
        /** 创建领券按钮 */
        self.coupon_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.coupon_btn];
        self.coupon_btn.layer.masksToBounds = YES;
        self.coupon_btn.layer.cornerRadius = 4;
        self.coupon_btn.layer.borderWidth = 1;
        self.coupon_btn.layer.borderColor = UIColorFromHex(0xFF5E56).CGColor;
        self.coupon_btn.titleLabel.font = CurrencyFont(12);
        self.coupon_btn.titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.coupon_btn setTitleColor:UIColorFromHex(0xFF5E56) forState:UIControlStateNormal];
        [self.coupon_btn setImage:[UIImage imageNamed:@"价格箭头"] forState:UIControlStateNormal];
        [self.coupon_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.content_lb.mas_bottom).offset(8);
            make.leading.trailing.equalTo(weakSelf.content_lb);
        }];
        [self.coupon_btn.imageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.equalTo(@0);
            make.width.equalTo(weakSelf.coupon_btn).multipliedBy(0.56);
        }];
        [self.coupon_btn.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.centerY.equalTo(@0);
            make.leading.equalTo(weakSelf.coupon_btn.imageView.mas_trailing);
        }];
        /** 创建领劵省钱数目 */
        self.coupon_money_lb = [UILabel new];
        [self.coupon_btn addSubview:self.coupon_money_lb];
        self.coupon_money_lb.textColor = [UIColor whiteColor];
        self.coupon_money_lb.font = CurrencyFont(13);
        [self.coupon_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(weakSelf.coupon_btn.imageView);
        }];
    }
    return self;
}

- (void)setModel:(CateItemModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.pics.firstObject] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _origin_money_lb.text = [NSString stringWithFormat:@"原%@元",model.price];
    [_coupon_btn setTitle:model.umpPrice forState:UIControlStateNormal];
    _content_lb.text = model.title;
    _coupon_money_lb.text = [@"领券省" stringByAppendingString:model.youhuiPrice];
    _fllow_btn.selected = model.fav;
    [_fllow_btn setTitle:[NSString stringWithFormat:@"  %@ ",@(model.favorites).stringValue] forState:UIControlStateNormal];
}

/** 关注按钮 */
- (void)fllow:(UIButton *)btn{
    btn.enabled = NO;
    [SVProgressHUD show];
    if (btn.selected) {
        [self delFllow];
    }else{
        [self addFllow];
    }
}

/** 添加收藏 */
- (void)addFllow{
    [[YURequest creatMarketRequest] POST:@"openApi/item/favorites.json" parameters:@{@"tbItemId":_model.tbItemId} success:^(id JSON) {
        [SVProgressHUD showSuccessWithStatus:@"添加收藏成功"];
        _fllow_btn.selected = !_fllow_btn.selected;
        _model.favorites++;
        _model.fav = YES;
        self.model = _model;
        _fllow_btn.enabled = YES;
    } failure:nil];
}

/** 删除收藏 */
- (void)delFllow{
    [[YURequest creatMarketRequest] POST:@"openApi/item/delFavorites.json" parameters:@{@"tbItemId":_model.tbItemId} success:^(id JSON) {
        [SVProgressHUD showSuccessWithStatus:@"取消收藏成功"];
        _fllow_btn.selected = !_fllow_btn.selected;
        _model.favorites--;
        _model.fav = NO;
        self.model = _model;
        _fllow_btn.enabled = YES;
    } failure:nil];
}

@end
