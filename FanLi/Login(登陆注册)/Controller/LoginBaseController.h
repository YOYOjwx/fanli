//
//  LoginBaseController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/13.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"

@interface LoginBaseController : BaseViewController

/** 登录成功的block */
@property (nonatomic, strong) void (^success)(User *user);
/** 登录成功的方法 */
- (void)loginSuccess:(User *)user;

@end
