//
//  LoginBaseController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/13.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "LoginBaseController.h"

@interface LoginBaseController ()

@end

@implementation LoginBaseController

/** 登录成功 */
- (void)loginSuccess:(User *)user{
    if (self.success) {
        self.success(user);
    }
    BaseViewController *loginVC = self.navigationController.childViewControllers[0];
    [self.navigationController popToRootViewControllerAnimated:NO];
    [loginVC dismissViewControllerAnimated:NO completion:nil];
}

@end
