//
//  LoginController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "LoginBaseController.h"

@interface LoginController : LoginBaseController

/** 跳转登陆页面 */
+ (void)goToLoginSuccess:(void (^)(User *user))success;
/** 判断是否是登录状态,如果不是登录状态。会自动进行登录 */
+ (BOOL)isLoginStatus:(void (^)(User *user))success;

@end
