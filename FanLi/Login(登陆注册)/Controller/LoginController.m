//
//  LoginController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "LoginController.h"
#import "QQQuickLogin.h"
#import "WeiboQuickLogin.h"
#import "TaobaoQuickLogin.h"
#import "QuickLoginButton.h"
#import "YUNavigationController.h"
#import "RegisterController.h"
#import "PhoneLoginController.h"

@interface LoginController ()

/** qq快捷登陆 */
@property (nonatomic, strong) QQQuickLogin *qqQuick;

@end

@implementation LoginController

#pragma mark 懒加载
- (QQQuickLogin *)qqQuick{
    if (!_qqQuick) {
        _qqQuick = [[QQQuickLogin alloc] init];
    }
    return _qqQuick;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 去除左边回退按钮 */
    self.navigationItem.leftBarButtonItem = nil;
    /** 设置背景颜色 */
    self.view.backgroundColor = [UIColor whiteColor];
    /** 创建子试图 */
    [self creatSubViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    /** 隐藏导航栏 */
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    /** 显示导航栏 */
    self.navigationController.navigationBarHidden = NO;
}

/** 创建子试图 */
- (void)creatSubViews{
    WS(weakSelf);
    /** 创建退出按钮 */
    UIButton *exit_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:exit_btn];
    [exit_btn setImage:[UIImage imageNamed:@"登录关闭"] forState:UIControlStateNormal];
    [exit_btn addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    exit_btn.backgroundColor = RGBCOLOR(0, 0, 0, 0.3);
    exit_btn.layer.masksToBounds = YES;
    exit_btn.layer.cornerRadius = 13;
    [exit_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@30);
        make.trailing.equalTo(@-20);
        make.width.height.equalTo(@26);
    }];
    /** 创建appicon */
    UIImageView *appIcon_image = [UIImageView new];
    [self.view addSubview:appIcon_image];
    appIcon_image.image = [UIImage imageNamed:@"登录页logo"];
    [appIcon_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@120);
        make.centerX.equalTo(@0);
    }];
    /** 创建QQ登录 */
    QuickLoginButton *qq_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:qq_btn];
    [qq_btn setTitle:@"   QQ登录" forState:UIControlStateNormal];
    [qq_btn setImage:[UIImage imageNamed:@"登陆页QQ"] forState:UIControlStateNormal];
    [qq_btn addTarget:self action:@selector(qqLogin) forControlEvents:UIControlEventTouchUpInside];
    qq_btn.titleColor = UIColorFromHex(0x3982D8);
    [qq_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@40);
        make.trailing.equalTo(weakSelf.view.mas_centerX).offset(-5);
        make.bottom.equalTo(@-55);
        make.height.equalTo(@40);
    }];
    /** 创建微博登录 */
    QuickLoginButton *weibo_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:weibo_btn];
    [weibo_btn setTitle:@"   微博登录" forState:UIControlStateNormal];
    [weibo_btn setImage:[UIImage imageNamed:@"登陆页微博"] forState:UIControlStateNormal];
    [weibo_btn addTarget:self action:@selector(weiboLogin) forControlEvents:UIControlEventTouchUpInside];
    weibo_btn.titleColor = UIColorFromHex(0xFF9000);
    [weibo_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.width.height.equalTo(qq_btn);
        make.leading.equalTo(weakSelf.view.mas_centerX).offset(5);
    }];
    /** 创建淘宝登录 */
    QuickLoginButton *taobao_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:taobao_btn];
    [taobao_btn setTitle:@"淘宝登录" forState:UIControlStateNormal];
    taobao_btn.titleColor = UIColorFromHex(0xFF9000);
    [taobao_btn addTarget:self action:@selector(taobaoLogin) forControlEvents:UIControlEventTouchUpInside];
    [taobao_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.height.equalTo(qq_btn);
        make.trailing.equalTo (weibo_btn);
        make.bottom.equalTo(qq_btn.mas_top).offset(-10);
    }];
    /** 创建手机登录 */
    QuickLoginButton *phone_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:phone_btn];
    [phone_btn setTitle:@"手机登录" forState:UIControlStateNormal];
    phone_btn.titleColor = UIColorFromHex(0xFF5E56);
    [phone_btn addTarget:self action:@selector(phoneLogin) forControlEvents:UIControlEventTouchUpInside];
    [phone_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.height.trailing.equalTo(taobao_btn);
        make.bottom.equalTo(taobao_btn.mas_top).offset(-10);
    }];
    /** 手机注册 */
    QuickLoginButton *register_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:register_btn];
    register_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [register_btn setTitle:@"手机注册" forState:UIControlStateNormal];
    [register_btn addTarget:self action:@selector(registerUser) forControlEvents:UIControlEventTouchUpInside];
    register_btn.titleColor = UIColorFromHex(0xFFFFFF);
    register_btn.layer.borderColor = UIColorFromHex(0xFF5E56).CGColor;
    [register_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.height.equalTo(taobao_btn);
        make.bottom.equalTo(phone_btn.mas_top).offset(-10);
    }];
}

/** 退出登录 */
- (void)exit{
    [self dismissViewControllerAnimated:YES completion:nil];
}

/** 用户注册 */
- (void)registerUser{
    RegisterController *registerVC = [[RegisterController alloc] init];
    registerVC.success = self.success;
    [self.navigationController pushViewController:registerVC animated:YES];
}

/** 手机登录 */
- (void)phoneLogin{
    PhoneLoginController *phoneVC = [[PhoneLoginController alloc] init];
    phoneVC.success = self.success;
    [self.navigationController pushViewController:phoneVC animated:YES];
}

/** 淘宝登录 */
- (void)taobaoLogin{
    [TaobaoQuickLogin taobaoLoginWith:self success:^(User *user) {
        [self loginSuccess:user];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

/** QQ登录 */
- (void)qqLogin{
    [self.qqQuick qqQuickLoginSuccess:^(User *user) {
        [self loginSuccess:user];
    } failure:^(NSString *msg) {
        [SVProgressHUD showErrorWithStatus:msg];
    }];
}

/** 微博登录 */
- (void)weiboLogin{
    [[WeiboQuickLogin shareWeibo] weiboQuickLoginSuccess:^(User *user) {
        [self loginSuccess:user];
    } failure:nil];
}

/** 弹出登录页面 */
+ (void)goToLoginSuccess:(void (^)(User *))success{
    LoginController *login = [self new];
    login.success = success;
    YUNavigationController *naC = [[YUNavigationController alloc] initWithRootViewController:login];
    [[[[UIApplication sharedApplication] delegate] window].rootViewController presentViewController:naC animated:YES completion:nil];
}

/** 判断是否是登录状态 */
+ (BOOL)isLoginStatus:(void (^)(User *))success{
    if (IS_LOGIN) {
        [User getSelfUserSuccess:success failure:nil];
        return YES;
    }else{
        [self goToLoginSuccess:success];
        return NO;
    }
}

@end
