//
//  PhoneLoginController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "PhoneLoginController.h"
#import "NSString+Common.h"
#import "RegisterController.h"

@interface PhoneLoginController ()
{
    /** 手机号码 */
    UITextField *_phone_tf;
    /** 密码 */
    UITextField *_pw_tf;
}

@end

@implementation PhoneLoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.title = @"手机登录";
    /** 设置背景颜色 */
    self.view.backgroundColor = UIColorFromHex(0xF4F4F4);
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 背景视图 */
    UIView *bg_view = [UIView new];
    [self.view addSubview:bg_view];
    bg_view.backgroundColor = [UIColor whiteColor];
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(@0);
        make.height.equalTo(@100);
    }];
    /** 手机号码 */
    _phone_tf = [UITextField new];
    [bg_view addSubview:_phone_tf];
    _phone_tf.placeholder = @"请输入手机号码";
    _phone_tf.font = CurrencyFont(14);
    _phone_tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    _phone_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_phone_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.equalTo(@0);
        make.leading.equalTo(@15);
        make.height.equalTo(@50);
    }];
    UIView *line = [UIView new];
    [bg_view addSubview:line];
    line.backgroundColor = UIColorFromHex(0xEFEFF4);
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_phone_tf);
        make.trailing.equalTo(@-15);
        make.top.equalTo(_phone_tf.mas_bottom);
        make.height.equalTo(@1);
    }];
    /** 密码 */
    _pw_tf = [UITextField new];
    [bg_view addSubview:_pw_tf];
    _pw_tf.secureTextEntry = YES;
    _pw_tf.font = CurrencyFont(14);
    _pw_tf.placeholder = @"登陆密码";
    [_pw_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(_phone_tf);
        make.bottom.equalTo(@0);
        make.top.equalTo(_phone_tf.mas_bottom);
    }];
    /** 注册按钮 */
    UIButton *login_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:login_btn];
    login_btn.titleLabel.font = CurrencyFont(14);
    [login_btn setTitle:@"登录" forState:UIControlStateNormal];
    login_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [login_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    login_btn.layer.masksToBounds = YES;
    login_btn.layer.cornerRadius = 4;
    [login_btn addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    [login_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@15);
        make.trailing.equalTo(@-15);
        make.top.equalTo(bg_view.mas_bottom).offset(15);
        make.height.equalTo(@40);
    }];
    /** 忘记密码 */
    UIButton *forget_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:forget_btn];
    [forget_btn setTitle:@"忘记密码?" forState:UIControlStateNormal];
    [forget_btn setTitleColor:UIColorFromHex(0x999999) forState:UIControlStateNormal];
    forget_btn.titleLabel.font = CurrencyFont(12);
    [forget_btn addTarget:self action:@selector(forgetPassword) forControlEvents:UIControlEventTouchUpInside];
    [forget_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.equalTo(login_btn);
        make.top.equalTo(login_btn.mas_bottom).offset(20);
    }];
}

/** 登录 */
- (void)login{
    NSString *error = nil;
    if (![_phone_tf.text checkPhoneNumber])
        error = @"请输入正确的手机号码";
    if (_phone_tf.text.length < 6)
        error = @"密码小于6位";
    if (error == nil) {//没有错误，进行登录
        [User loginWithPhone:_phone_tf.text password:_pw_tf.text success:^(User *user) {
            [self loginSuccess:user];
        } failure:nil];
    }else{
        [SVProgressHUD showErrorWithStatus:error];
    }
}

/** 忘记密码 */
- (void)forgetPassword{
    RegisterController *forgetVC = [[RegisterController alloc] init];
    forgetVC.isForget = YES;
    forgetVC.success = self.success;
    [self.navigationController pushViewController:forgetVC animated:YES];
}

@end
