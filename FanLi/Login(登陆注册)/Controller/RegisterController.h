//
//  RegisterController.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/28.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "LoginBaseController.h"

@interface RegisterController : LoginBaseController

/** 是否是忘记密码 */
@property (nonatomic, assign) BOOL isForget;

@end
