//
//  RegisterController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/28.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "RegisterController.h"
#import "NSString+Common.h"
#import "User.h"
#import "YUNavigationController.h"
#import <UMMobClick/MobClick.h>

@interface RegisterController ()
{
    /** 手机号码 */
    UITextField *_phone_tf;
    /** 验证码 */
    UITextField *_code_tf;
    /** 密码 */
    UITextField *_pw_tf;
}

@end

@implementation RegisterController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 标题 */
    self.navigationItem.title = self.isForget?@"修改密码":@"手机注册";
    /** 创建视图 */
    [self creatSubViews];
}

/** 创建视图 */
- (void)creatSubViews{
    /** 背景视图 */
    UIView *bg_view = [UIView new];
    [self.view addSubview:bg_view];
    bg_view.backgroundColor = [UIColor whiteColor];
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(@0);
        make.height.equalTo(@150);
    }];
    /** 手机号码 */
    _phone_tf = [UITextField new];
    [bg_view addSubview:_phone_tf];
    _phone_tf.placeholder = @"请输入手机号码";
    _phone_tf.font = CurrencyFont(14);
    _phone_tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    _phone_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_phone_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.equalTo(@0);
        make.leading.equalTo(@15);
        make.height.equalTo(@50);
    }];
    UIView *line1 = [UIView new];
    [bg_view addSubview:line1];
    line1.backgroundColor = UIColorFromHex(0xEFEFF4);
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_phone_tf);
        make.trailing.equalTo(@-15);
        make.top.equalTo(_phone_tf.mas_bottom);
        make.height.equalTo(@1);
    }];
    /** 获取验证吗按钮 */
    UIButton *code_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bg_view addSubview:code_btn];
    [code_btn setTitle:@"获取验证码" forState:UIControlStateNormal];
    code_btn.titleLabel.font = CurrencyFont(14);
    code_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [code_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [code_btn addTarget:self action:@selector(getVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
    code_btn.layer.masksToBounds = YES;
    code_btn.layer.cornerRadius = 4;
    [code_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(@0);
        make.trailing.equalTo(line1);
        make.height.equalTo(@35);
        make.width.equalTo(code_btn.mas_height).multipliedBy(2.6);
    }];
    /** 验证码 */
    _code_tf = [UITextField new];
    [bg_view addSubview:_code_tf];
    _code_tf.placeholder = @"请输入收到的验证码";
    _code_tf.font = CurrencyFont(14);
    _code_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_code_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.height.equalTo(_phone_tf);
        make.trailing.equalTo(code_btn.mas_leading);
        make.top.equalTo(_phone_tf.mas_bottom);
    }];
    UIView *line2 = [UIView new];
    [bg_view addSubview:line2];
    line2.backgroundColor = UIColorFromHex(0xEFEFF4);
    [line2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.height.equalTo(line1);
        make.top.equalTo(_code_tf.mas_bottom);
    }];
    /** 密码 */
    _pw_tf = [UITextField new];
    [bg_view addSubview:_pw_tf];
    _pw_tf.secureTextEntry = YES;
    _pw_tf.font = CurrencyFont(14);
    _pw_tf.placeholder = self.isForget?@"新密码仅限于: 6-20位, 字母和数字":@"登陆密码";
    [_pw_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(_phone_tf);
        make.bottom.equalTo(@0);
        make.top.equalTo(_code_tf.mas_bottom);
    }];
    /** 注册按钮 */
    UIButton *register_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:register_btn];
    register_btn.titleLabel.font = CurrencyFont(14);
    [register_btn setTitle:self.isForget?@"完成":@"注册" forState:UIControlStateNormal];
    register_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [register_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    register_btn.layer.masksToBounds = YES;
    register_btn.layer.cornerRadius = 4;
    [register_btn addTarget:self action:@selector(registerUser) forControlEvents:UIControlEventTouchUpInside];
    [register_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@15);
        make.trailing.equalTo(@-15);
        make.top.equalTo(bg_view.mas_bottom).offset(15);
        make.height.equalTo(@40);
    }];
}

/** 获取验证码 */
- (void)getVerificationCode:(UIButton *)btn{
    if ([_phone_tf.text checkPhoneNumber]) {
        btn.enabled = NO;
        [User sendAuthcodeWithPhone:_phone_tf.text success:^() {
            [self sendVerificationCodeAnimation:btn];
        } failure:nil];
    }else{
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
    }
}

/** 注册 */
- (void)registerUser{
    [MobClick event:@"startRegisterOrLogin"];
    NSString *error = nil;
    if (![_phone_tf.text checkPhoneNumber])
        error = @"请输入正确的手机号码";
    if (_code_tf.text.length == 0)
        error = @"请输入验证码";
    if (_pw_tf.text.length < 6)
        error = @"密码小于6位";
    if (error == nil) {//没有错误，进行手机注册
        [User registerWithPhone:_phone_tf.text password:_pw_tf.text code:_code_tf.text isForger:self.isForget success:^(User *user) {
            [MobClick event:@"finishLogin"];
            [self loginSuccess:user];
        } failure:nil];
    }else{
        [SVProgressHUD showErrorWithStatus:error];
    }
}

/** 发送验证码 */
- (void)sendVerificationCodeAnimation:(UIButton *)btn{
    [btn setTitle:@"重新发送(30)" forState:UIControlStateNormal];
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        int time = [btn.titleLabel.text substringWithRange:NSMakeRange(5, 2)].intValue;
        [btn setTitle:[NSString stringWithFormat:@"重新发送(%d)",--time] forState:UIControlStateNormal];
        if (time == 0) {
            [timer invalidate];
            [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
            btn.enabled = YES;
        }
    }];
}


@end
