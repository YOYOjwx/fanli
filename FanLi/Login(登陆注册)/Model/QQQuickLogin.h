//
//  QQQuickLogin.h
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "QuickLogin.h"
#import <TencentOpenAPI/TencentOAuth.h>

@interface QQQuickLogin : QuickLogin<TencentSessionDelegate>

/** QQ快捷登陆方法 */
- (void)qqQuickLoginSuccess:(void(^)(User *user))success failure:(void(^)(NSString *msg))failure;
/** QQ注册应用 */
+ (void)qqRegisted;

@end

