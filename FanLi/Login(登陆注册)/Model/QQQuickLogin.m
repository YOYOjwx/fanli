//
//  QQQuickLogin.m
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#define QQ_APP_ID @"1104169428"

#import "QQQuickLogin.h"

@interface QQQuickLogin ()

/** QQ快捷登陆 */
@property (nonatomic, strong) TencentOAuth *tencentOAuth;
/** 登录成功的Block */
@property (nonatomic, copy) void(^success)(User *user);
/** 登录失败的Block */
@property (nonatomic, copy) void(^failure)(NSString *msg);

@end

@implementation QQQuickLogin

+ (void)qqRegisted{
    [[[super alloc] init] tencentOAuth];
}

#pragma mark 懒加载
- (TencentOAuth *)tencentOAuth{
    if (!_tencentOAuth) {
        _tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQ_APP_ID andDelegate:self];
    }
    return _tencentOAuth;
}

- (void)qqQuickLoginSuccess:(void (^)(User *))success failure:(void (^)(NSString *))failure{
    [self.tencentOAuth authorize:@[kOPEN_PERMISSION_GET_SIMPLE_USER_INFO] inSafari:NO];
    self.success = success;
    self.failure = failure;
}

/** 登陆成功的回调 */
- (void)tencentDidLogin{
    [SVProgressHUD show];
    [[YURequest creatRequestWith:@"https://openmobile.qq.com/"] GET:@"user/get_simple_userinfo" parameters:@{@"access_token":self.tencentOAuth.accessToken,@"oauth_consumer_key":self.tencentOAuth.appId,@"openid":self.tencentOAuth.openId} success:^(id JSON) {
        if (JSON == nil) return;
        NSData *data = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [SVProgressHUD show];
        [[YURequest creatBaseRequest] POST:@"user/login/qq.json" parameters:@{@"jsonContent":jsonString,@"openid":self.tencentOAuth.openId,@"deviceId":DEVICE_ID?DEVICE_ID:@"0",@"inviteUid":INVITE_UID} success:^(id JSON) {
            User *user = [User shareUser];
            [user setValuesForKeysWithDictionary:JSON];
            if (self.success) {
                [User getCoinSuccess:self.success];
            }
        } failure:nil];
    } failure:nil];
}

/** 登录失败后的回调 cancelled:代表用户是否主动退出登录 */
- (void)tencentDidNotLogin:(BOOL)cancelled{
    if (!cancelled) {
        if (self.failure) {
            self.failure(@"网络错误");
        }
    }
}

/** 登录时网络有问题的回调 */
- (void)tencentDidNotNetWork{
    if (self.failure) {
        self.failure(@"网络错误");
    }
}

@end
