//
//  TaobaoQuickLogin.h
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "QuickLogin.h"
#import <AlibabaAuthSDK/ALBBSDK.h>

@interface TaobaoQuickLogin : QuickLogin

+ (void)taobaoLoginWith:(UIViewController *)vc success:(void(^)(User *user))success failure:(void(^)(NSString *msg))failure;

+ (void)logout;

@end
