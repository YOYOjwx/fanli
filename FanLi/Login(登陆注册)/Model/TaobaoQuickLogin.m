//
//  TaobaoQuickLogin.m
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "TaobaoQuickLogin.h"

@implementation TaobaoQuickLogin

+ (void)taobaoLoginWith:(UIViewController *)vc success:(void(^)(User *user))success failure:(void(^)(NSString *msg))failure{
    [[ALBBSDK sharedInstance] auth:vc successCallback:^(ALBBSession *session) {
        ALBBUser *user = session.getUser;
        [SVProgressHUD show];
        [[YURequest creatBaseRequest] POST:@"user/login/taobao.json" parameters:@{@"taoUserId":user.openId,@"avatarUrl":user.avatarUrl,@"nick":user.nick,@"inviteUid":INVITE_UID} success:^(id JSON) {
            User *user = [User shareUser];
            [user setValuesForKeysWithDictionary:JSON];
            if (success) {
                [User getCoinSuccess:success];
            }
        } failure:nil];
    } failureCallback:nil];
}

+ (void)logout{
    [[ALBBSDK sharedInstance] logout];
}

@end
