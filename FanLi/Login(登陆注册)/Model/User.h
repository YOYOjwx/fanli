//
//  User.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM (int, GenderType) {
    /** 男性 */
    Male = 1,
    /** 女性 */
    Female
};
typedef NS_ENUM (int, UserType) {
    /** 普通用户 */
    UserTypeNormal,
    /** 商家 */
    UserTypeNomalVendor,
    /** 服务商家 */
    UserTypeServiceVendor
};
typedef NS_ENUM (int, UserState) {
    /** 正常 */
    Normal,
    /** 禁用 */
    Disable
};
typedef NS_ENUM (int, FollowState) {
    /** 未关注 */
    NotFollow,
    /** 已经关注 */
    Follow,
    /** 互相关注 */
    MutualFollow
};

@interface User : BaseModel

/** 标示用户唯一ID */
@property (nonatomic, assign) NSInteger uid;
/** token */
@property (nonatomic, strong) NSString *token;
/** 昵称 */
@property (nonatomic, strong) NSString *nickname;
/** 性别 */
@property(nonatomic, assign) GenderType gender;
/** 预产期 */
@property (nonatomic, strong) NSDate *babyBirthday;
/** 宝宝性别 */
@property (nonatomic, assign) GenderType babyGender;
/** 头像图片URL */
@property (nonatomic, strong) NSString *profileImage;
/** 金币 */
@property (nonatomic, assign) int ycoins;
/** 提问数 */
@property (nonatomic, assign) int questionCount;
/** 回答数 */
@property (nonatomic, assign) int answerCount;
/** 被感谢次数 */
@property (nonatomic, assign) int appreciationCount;
/** 用户状态 */
@property (nonatomic, assign) UserState status;
/** 是否是妈妈顾问团 */
@property (nonatomic, assign) BOOL isCounsellor;
/** 用户等级 */
@property (nonatomic, assign) int level;
/** 上周回答被采纳数 */
@property (nonatomic, assign) int accInLastWeek;
/** 昨日获取金币数 */
@property (nonatomic, assign) int coinsYesterday;
/** 关注的状态 */
@property (nonatomic, assign) FollowState followStatus;
/** 手机号 */
@property (nonatomic, strong) NSString *phoneNumber;
/** 城市 */
@property (nonatomic, strong) NSString *city;
/** 拥有的勋章列表 */
@property (nonatomic, strong) NSArray *medalIdList;
/** 定位位置 */
@property (nonatomic, strong) NSString *position;
/** 是否已经做过网络请求 */
@property (nonatomic, assign) BOOL isNetwork;
/** 创建单利,主要用户全局获取自己的信息 */
+ (User *)shareUser;
/** 登录接口 */
+ (void)loginWithPhone:(NSString *)phone password:(NSString *)password success:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** 发动验证码 */
+ (void)sendAuthcodeWithPhone:(NSString *)phone success:(void(^)())success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** 注册接口 忘记密码接口 */
+ (void)registerWithPhone:(NSString *)phone password:(NSString *)password code:(NSString *)code isForger:(BOOL)isForget success:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** 获取单个用户的信息 */
+ (void)getUserForQueryUid:(NSString *)queryUid success:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** 获取自己用户的信息 */
+ (void)getSelfUserSuccess:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** 获取返利金币和累积金币接口 */
+ (void)getCoinSuccess:(void(^)(User *user))success;
/** 保存uid和token */
- (void)saveUidAndToken;
/** 退出登录 */
- (void)logout;

/** 即将返利的金币 */
@property (nonatomic, strong) NSString *available_coins;
/** 累计返利的金币 */
@property (nonatomic, strong) NSString *accumulated_rebates;


@end
