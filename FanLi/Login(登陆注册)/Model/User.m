//
//  User.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "User.h"

@implementation User

+ (User *)shareUser{
    static User *user = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        user = [[User alloc] init];
        user.available_coins = @"0";
        user.accumulated_rebates = @"0";
    });
    return user;
}

/** 登录接口 */
+ (void)loginWithPhone:(NSString *)phone password:(NSString *)password success:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    [SVProgressHUD show];
    [[YURequest creatBaseRequest] POST:@"user/login.json" parameters:@{@"username":phone,@"password":password,@"deviceId":DEVICE_ID?DEVICE_ID:@"0"} success:^(id JSON) {
        User *user = [User shareUser];
        [user setValuesForKeysWithDictionary:JSON];
        if (success) {
            [self getCoinSuccess:success];
        }
    } failure:failure];
}

/** 发动验证码 */
+ (void)sendAuthcodeWithPhone:(NSString *)phone success:(void(^)())success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    [SVProgressHUD show];
    [[YURequest creatBaseRequest] POST:@"authcode/send.json" parameters:@{@"phoneNumber":phone,@"deviceId":DEVICE_ID?DEVICE_ID:@"0"} success:^(id JSON) {
        if (success) {
            success();
        }
    } failure:failure];
}

/** 注册接口 忘记密码接口 */
+ (void)registerWithPhone:(NSString *)phone password:(NSString *)password code:(NSString *)code isForger:(BOOL)isForget success:(void (^)(User *))success failure:(void (^)(NSError *, NSString *, NSInteger))failure{
    [SVProgressHUD show];
    /** 区分跳转的借口 */
    NSString *postStr = isForget?@"user/phone/password.json":@"user/phone/reg.json";
    [[YURequest creatBaseRequest] POST:postStr parameters:@{@"phoneNumber":phone,@"password":password,@"authcode":code,@"deviceId":DEVICE_ID?DEVICE_ID:@"0",@"inviteUid":INVITE_UID} success:^(id JSON) {
        User *user = [User shareUser];
        [user setValuesForKeysWithDictionary:JSON];
        if (success) {
            [self getCoinSuccess:success];
        }
    } failure:failure];
}

/** 获取单个用户的信息 */
+ (void)getUserForQueryUid:(NSString *)queryUid success:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    if (success) {
        [[YURequest creatBaseRequest] POST:@"user/profile/view.json" parameters:@{@"queryUid":queryUid} success:^(id JSON) {
            User *user = [[User alloc] init];
            [user setValuesForKeysWithDictionary:JSON];
            success(user);
        } failure:failure];
    }
}

/** 获取自己用户的信息 */
+ (void)getSelfUserSuccess:(void(^)(User *user))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    User *user = [User shareUser];
    if (success) {
        if (user.isNetwork) {
            success(user);
        }else{
            [[YURequest creatBaseRequest] POST:@"user/profile/view.json" parameters:@{@"queryUid":UD_UID} success:^(id JSON) {
                [user setValuesForKeysWithDictionary:JSON];
                [self getCoinSuccess:success];
            } failure:failure];
        }
    }
}

/** 获取返利金币和累积金币接口 */
+ (void)getCoinSuccess:(void(^)(User *user))success{
    User *user = [User shareUser];
    [user saveUidAndToken];
    dispatch_queue_t queue = dispatch_queue_create(NULL, DISPATCH_QUEUE_CONCURRENT);
    dispatch_group_t group = dispatch_group_create();
    dispatch_group_enter(group);
    dispatch_group_enter(group);
    dispatch_group_async(group, queue, ^{
        [SVProgressHUD show];
        [[YURequest creatBaseRequest] POST:@"ycoin/myHistoryTotal.json" parameters:nil success:^(id JSON) {
            user.accumulated_rebates = [JSON stringValue];
            dispatch_group_leave(group);
        } failure:nil];
    });
    dispatch_group_async(group, queue, ^{
        [SVProgressHUD show];
        [[YURequest creatMarketRequest] POST:@"openApi/order/mytotal" parameters:nil success:^(id JSON) {
            user.available_coins = [JSON stringValue];
            dispatch_group_leave(group);
        } failure:nil];
    });
    dispatch_group_notify(group, dispatch_get_main_queue(), ^{
        /** 保存登录状态 */
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"IS_LOGIN"];
        /** 保存网络获取状态 */
        user.isNetwork = YES;
        success(user);
    });
}

/** 退出登录 */
- (void)logout{
    self.nickname = nil;
    self.uid = 1000;
    self.token = @"a5e313ac2df3f6a42626d3b34c62669c";
    self.gender = 0;
    self.babyBirthday = nil;
    self.profileImage = nil;
    self.ycoins = 0;
    self.questionCount = 0;
    self.answerCount = 0;
    self.appreciationCount = 0;
    self.status = 0;
    self.isCounsellor = NO;
    self.level = 0;
    self.accInLastWeek = 0;
    self.coinsYesterday = 0;
    self.followStatus = 0;
    self.phoneNumber = nil;
    self.city = nil;
    self.medalIdList = nil;
    self.position = nil;
    self.isNetwork = NO;
    self.available_coins = @"0";
    self.accumulated_rebates = @"0";
    /** 保存默认uid和token */
    [self saveUidAndToken];
}

/** 保存uid和token */
- (void)saveUidAndToken{
    /** 替换默认的uid和token */
    [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%ld",(long)self.uid] forKey:@"uid"];
    [[NSUserDefaults standardUserDefaults] setValue:self.token forKey:@"token"];
}

@end
