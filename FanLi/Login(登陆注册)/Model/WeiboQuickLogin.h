//
//  WeiboQuickLogin.h
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "QuickLogin.h"
#import "WeiboSDK.h"
#import <TencentOpenAPI/QQApiInterface.h>

@interface WeiboQuickLogin : QuickLogin<WeiboSDKDelegate>

/** 单利，用于接受代理 */
+ (WeiboQuickLogin *)shareWeibo;
/** 登录方法 */
- (void)weiboQuickLoginSuccess:(void(^)(User *user))success failure:(void(^)(NSString *msg))failure;

@end
