//
//  WeiboQuickLogin.m
//  FanLi
//
//  Created by 费猫 on 2017/5/15.
//  Copyright © 2017年 费猫. All rights reserved.
//

#define Weibo_AppKey @"621565140"
#define Weibo_AppSecret @"06e50023fae31dcc411bd48953047800"
#define kAppRedirectURI @"https://api.weibo.com/oauth2/default.html"

#import "WeiboQuickLogin.h"

/** 微博快捷登陆 */
@interface WeiboQuickLogin ()

/** 登录成功的Block */
@property (nonatomic, copy) void(^success)(User *user);
/** 登录失败的Block */
@property (nonatomic, copy) void(^failure)(NSString *msg);

@end

@implementation WeiboQuickLogin

+ (WeiboQuickLogin *)shareWeibo{
    static WeiboQuickLogin *weibo = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        weibo = [[WeiboQuickLogin alloc] init];
    });
    return weibo;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        [WeiboSDK registerApp:Weibo_AppKey];
    }
    return self;
}

/** 登录的方法 */
- (void)weiboQuickLoginSuccess:(void (^)(User *))success failure:(void (^)(NSString *))failure{
    WBAuthorizeRequest *request = [WBAuthorizeRequest request];
    request.redirectURI = kAppRedirectURI;
    request.scope = @"all";
    [WeiboSDK sendRequest:request];
    self.success = success;
    self.failure = failure;
}

/** 收到一个来自微博客户端程序的响应 */
- (void)didReceiveWeiboResponse:(WBBaseResponse *)response{
    if (response.userInfo == nil) return;
    [SVProgressHUD show];
    [[YURequest creatRequestWith:@"https://api.weibo.com/2/"] GET:@"users/show.json" parameters:@{@"access_token":response.userInfo[@"access_token"],@"uid":response.userInfo[@"uid"]} success:^(id JSON) {
        NSData *data = [NSJSONSerialization dataWithJSONObject:JSON options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        [SVProgressHUD show];
        [[YURequest creatBaseRequest] POST:@"user/login/weibo.json" parameters:@{@"jsonContent":jsonString,@"deviceId":DEVICE_ID?DEVICE_ID:@"0",@"inviteUid":INVITE_UID} success:^(id JSON) {
            User *user = [User shareUser];
            [user setValuesForKeysWithDictionary:JSON];
            if (self.success) {
                [User getCoinSuccess:self.success];
            }
        } failure:nil];
    } failure:nil];
}

/** 收到一个来自微博客户端程序的请求 */
- (void)didReceiveWeiboRequest:(WBBaseRequest *)request{
    
}


@end
