//
//  AppDelegate.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "AppDelegate.h"
#import "YUTabBarController.h"
#import "NSString+Common.h"
#import "WeiboQuickLogin.h"
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <WebKit/WebKit.h>
#import "SAMKeychain.h"
#import "ShareModel.h"
#import "SelectedShareView.h"
#import "InviteUserListModel.h"
#import "DownAppShare.h"
#import "WXApi.h"
#import <UMMobClick/MobClick.h>

@interface AppDelegate ()

@property (nonatomic, strong) WKWebView *wk;

@end

@implementation AppDelegate

- (WKWebView *)wk{
    if (!_wk) {
        _wk = [[WKWebView alloc] init];
    }
    return _wk;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    if (!FIRST_ENTRY) {//如果是第一次, 进入引导页
        /** 设置子控制器，为引导页 */
        self.window.rootViewController = [[NSClassFromString(@"GuideController") alloc] init];
    } else {//否则直接进入应用
        self.window.rootViewController = [[NSClassFromString(@"YUTabBarController") alloc] init];
    }
    /** 关键数据初始化方法 */
    [self initialization:application];
    [self.window makeKeyAndVisible];
    return YES;
}

/** 关键数据初始化方法 */
- (void)initialization:(UIApplication *)application{
    /** 加载视图基础信息初始化 */
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeClear];
    /** 保存DeviceID */
    [self saveDeviceID];
    /** 设置默认uid和token */
    [self setDefaultUidAndToken];
    /** 获取分享网址 */
    [self getShareURL];
    /** 设置全局UserAgent */
    [self changeUserAgent];
    /** 微信注册 */
    [WXApi registerApp:@"wx28e4be94fb6fa4e4"];
    /** 友盟统计 */
    UMConfigInstance.appKey = @"54d73503fd98c57a71000292";
    UMConfigInstance.channelId = ChannelId;
    [MobClick setAppVersion:[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    [MobClick startWithConfigure:UMConfigInstance];
    /** 获取邀请人信息 */
    [[YURequest creatBaseRequest] POST:@"user/inviteUid/get.json" parameters:nil success:^(id JSON) {
        [[NSUserDefaults standardUserDefaults] setValue:[NSString stringWithFormat:@"%@",JSON] forKey:@"inviteUid"];
    } failure:nil];
    // 百川平台基础SDK初始化，加载并初始化各个业务能力插件
    AlibcTradeSDK *ali = [AlibcTradeSDK sharedInstance];
    [ali asyncInitWithSuccess:nil failure:nil];
    //配置全局的淘客参数
    //如果没有阿里妈妈的淘客账号,setTaokeParams函数需要调用
    AlibcTradeTaokeParams *taokeParams = [[AlibcTradeTaokeParams alloc] init];
    //mm_XXXXX为你自己申请的阿里妈妈淘客pid
    taokeParams.pid = @"mm_42516789_9158929_32060477";
    [ali setTaokeParams:taokeParams];
    // 设置全局配置，是否强制使用h5
    [ali setIsForceH5:NO];
}

/** 应用跳转处理 */
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    if (![[AlibcTradeSDK sharedInstance] application:application openURL:url sourceApplication:sourceApplication annotation:annotation]) {
        return [self handleOpenURL:url];
    }
    return YES;
}

/** 9.0以后使用新API接口 */
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options{
    if (![[AlibcTradeSDK sharedInstance] application:app openURL:url options:options]) {
        return [self handleOpenURL:url];
    }
    return YES;
}

/** 判断进来第三方URL的类型 */
- (BOOL)handleOpenURL:(NSURL *)url{
    NSString *absoluteString=url.absoluteString;
    if ([absoluteString judgeURLSource:@"tencent"]){
        [QQApiInterface handleOpenURL:url delegate:[SelectedShareView share]];
        return [TencentOAuth HandleOpenURL:url];
    }else if ([absoluteString judgeURLSource:@"wb"]) {
        return [WeiboSDK handleOpenURL:url delegate:[WeiboQuickLogin shareWeibo]];
    }else if ([absoluteString judgeURLSource:@"wx"]){
        return [WXApi handleOpenURL:url delegate:[SelectedShareView share]];
    }
    return YES;
}




/** 更改UserAgent */
- (void)changeUserAgent{
    NSString *agentString = [NSString stringWithFormat:@"/yoyo360/%@(yoyo360-ios)",[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"]];
    [self.wk evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSString* oldAgent = result;
        if (![oldAgent hasSuffix:agentString]) {
            NSString *newAgent = [oldAgent stringByAppendingString:agentString];
            NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:newAgent, @"UserAgent", nil];
            [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
        }
    }];
}

/** 保存DeviceID */
- (void)saveDeviceID{
    NSString *sercice = [[NSBundle mainBundle] bundleIdentifier];
    /** 从钥匙串读取UUID */
    NSString *deviceId = [SAMKeychain passwordForService:sercice account:@"deviceId"];
    if (deviceId.length == 0) {
        /** 保存一个UUID字符串到钥匙串 */
        CFUUIDRef uuid = CFUUIDCreate(NULL);
        assert(uuid != NULL);
        CFStringRef uuidStr = CFUUIDCreateString(NULL, uuid);
        [[NSUserDefaults standardUserDefaults] setValue:deviceId forKey:@"deviceId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [SAMKeychain setPassword:[NSString stringWithFormat:@"%@", uuidStr] forService:sercice account:@"deviceId"];
    }else if (DEVICE_ID.length == 0){
        [[NSUserDefaults standardUserDefaults] setValue:deviceId forKey:@"deviceId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    /**注意: setPassword和passwordForSevice方法中的**services 和 accounts 参数应该是一致的。*/
}

/** 设置默认的uid和token */
- (void)setDefaultUidAndToken{
    if (!UD_UID) {
        [[NSUserDefaults standardUserDefaults] setValue:@"1000" forKey:@"uid"];
    }
    if (!UD_TOKEN) {
        [[NSUserDefaults standardUserDefaults] setValue:@"a5e313ac2df3f6a42626d3b34c62669c" forKey:@"token"];
    }
}

/** 获取用户推广网址 */
- (void)getShareURL{
    [[YURequest creatBaseRequest] POST:@"user/share/url/get.json" parameters:nil success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            if ([dic[@"urlKey"] isEqualToString:@"fanliShareOrder"]) {
                ShareModel *share = [ShareModel share];
                [share setValuesForKeysWithDictionary:dic];
            }
            if ([dic[@"urlKey"] isEqualToString:@"fanliInviteUserList"]) {
                InviteUserListModel *invite = [InviteUserListModel share];
                [invite setValuesForKeysWithDictionary:dic];
            }
            if ([dic[@"urlKey"] isEqualToString:@"fanliDownApp"]) {
                DownAppShare *downApp = [DownAppShare share];
                [downApp setValuesForKeysWithDictionary:dic];
            }
        }
    } failure:nil];
}




- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end
