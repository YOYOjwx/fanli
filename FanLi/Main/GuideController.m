//
//  GuideController.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/14.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "GuideController.h"

@interface GuideController ()<UIScrollViewDelegate>

/** 页面指示 */
@property (nonatomic, strong) UIPageControl *pageControl;
/** 存放引导图片的数组 */
@property (nonatomic, strong) NSArray *imageArr;

@end

@implementation GuideController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.imageArr = @[@"guide-1", @"guide-2", @"guide-3",@"guide-4"];
    [self initScrollView];
    [self initPageControl];
}

/** 初始化滑动视图 */
- (void)initScrollView{
    /** scrollView */
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    scrollView.pagingEnabled = YES;
    scrollView.bounces = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.delegate = self;
    scrollView.contentSize = CGSizeMake(kMainWidth * self.imageArr.count, kMainHeight);
    for (int i = 0; i < self.imageArr.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kMainWidth * i, 0, kMainWidth, kMainHeight)];
        imageView.image = [UIImage imageNamed:self.imageArr[i]];
        [scrollView addSubview:imageView];
        if (i == 3) {//最后一个图片
            imageView.userInteractionEnabled = YES;
            UIButton *finish_btn = [UIButton buttonWithType:UIButtonTypeCustom];
            [imageView addSubview:finish_btn];
            finish_btn.backgroundColor = [UIColor whiteColor];
            [finish_btn setTitle:@"立即进入" forState:UIControlStateNormal];
            [finish_btn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [finish_btn addTarget:self action:@selector(finish) forControlEvents:UIControlEventTouchUpInside];
            finish_btn.layer.masksToBounds = YES;
            finish_btn.layer.cornerRadius = 15;
            finish_btn.titleLabel.font = CurrencyFont(14);
            [finish_btn mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(@0);
                make.top.equalTo(imageView).offset(kMainHeight * 15 / 18);
                make.height.equalTo(@30);
                make.width.equalTo(finish_btn.mas_height).multipliedBy(3);
            }];
        }
    }
    [self.view addSubview:scrollView];
}

/** 初始化页面指示 */
- (void)initPageControl{
    self.pageControl = [UIPageControl new];
    [self.view addSubview:self.pageControl];
    self.pageControl.numberOfPages = self.imageArr.count;
    self.pageControl.pageIndicatorTintColor = RGBCOLOR(255, 255, 255 ,0.4);
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.enabled = NO;
    [self.pageControl mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.bottom.equalTo(@-25);
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    //计算当前在第几页
    self.pageControl.currentPage = (NSInteger)(scrollView.contentOffset.x / kMainWidth);
}

//进入应用方法
- (void)finish{
    [self firstEntryAssignment];
}

/** 引导页结束，赋值 */
- (void)firstEntryAssignment{
    [UIApplication sharedApplication].delegate.window.rootViewController = [[NSClassFromString(@"YUTabBarController") alloc] init];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:FIRST_ENTRY_KEY];
}

@end
