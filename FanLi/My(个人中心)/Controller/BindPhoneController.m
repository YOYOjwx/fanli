//
//  BindPhoneController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BindPhoneController.h"
#import "NSString+Common.h"
#import "BingAlipayController.h"

@interface BindPhoneController ()
{
    /** 手机号码 */
    UITextField *_phone_tf;
    /** 验证码 */
    UITextField *_code_tf;
}
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation BindPhoneController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"绑定手机";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 创建scrollView */
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    /** 背景视图 */
    UIView *bg_view = [UIView new];
    [self.scrollView addSubview:bg_view];
    bg_view.backgroundColor = [UIColor whiteColor];
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.equalTo(@0);
        make.width.equalTo(@(kMainWidth));
        make.height.equalTo(@100);
    }];
    /** 手机号码 */
    _phone_tf = [UITextField new];
    [bg_view addSubview:_phone_tf];
    _phone_tf.placeholder = @"请输入手机号码";
    _phone_tf.font = CurrencyFont(14);
    _phone_tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    _phone_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_phone_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.equalTo(@0);
        make.leading.equalTo(@15);
        make.height.equalTo(@50);
    }];
    UIView *line1 = [UIView new];
    [bg_view addSubview:line1];
    line1.backgroundColor = UIColorFromHex(0xEFEFF4);
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_phone_tf);
        make.trailing.equalTo(@-15);
        make.top.equalTo(_phone_tf.mas_bottom);
        make.height.equalTo(@1);
    }];
    /** 获取验证吗按钮 */
    UIButton *code_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bg_view addSubview:code_btn];
    [code_btn setTitle:@"获取验证码" forState:UIControlStateNormal];
    code_btn.titleLabel.font = CurrencyFont(14);
    code_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [code_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [code_btn addTarget:self action:@selector(getVerificationCode:) forControlEvents:UIControlEventTouchUpInside];
    code_btn.layer.masksToBounds = YES;
    code_btn.layer.cornerRadius = 4;
    [code_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@-7.5);
        make.trailing.equalTo(line1);
        make.height.equalTo(@35);
        make.width.equalTo(code_btn.mas_height).multipliedBy(2.6);
    }];
    /** 验证码 */
    _code_tf = [UITextField new];
    [bg_view addSubview:_code_tf];
    _code_tf.placeholder = @"请输入收到的验证码";
    _code_tf.font = CurrencyFont(14);
    _code_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_code_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.height.equalTo(_phone_tf);
        make.trailing.equalTo(code_btn.mas_leading);
        make.top.equalTo(_phone_tf.mas_bottom);
    }];
    /** 注册按钮 */
    UIButton *register_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scrollView addSubview:register_btn];
    register_btn.titleLabel.font = CurrencyFont(14);
    [register_btn setTitle:@"确认绑定" forState:UIControlStateNormal];
    register_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [register_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    register_btn.layer.masksToBounds = YES;
    register_btn.layer.cornerRadius = 4;
    [register_btn addTarget:self action:@selector(confirmBinding) forControlEvents:UIControlEventTouchUpInside];
    [register_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(bg_view).offset(15);
        make.trailing.equalTo(bg_view).offset(-15);
        make.top.equalTo(bg_view.mas_bottom).offset(15);
        make.height.equalTo(@40);
    }];
    /** 提示信息 */
    UILabel * descLabel = [UILabel new];
    [self.scrollView addSubview:descLabel];
    descLabel.numberOfLines = 0;
    descLabel.font = CurrencyFont(13);
    descLabel.textColor = UIColorFromHex(0x999999);
    descLabel.text = @"提示:\r\n为了保障您的帐号安全，金币兑换之前先绑定手机号码。";
    [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(register_btn).offset(5);
        make.trailing.equalTo(register_btn).offset(-5);
        make.top.equalTo(register_btn.mas_bottom).offset(30);
    }];
}

/** 获取验证码 */
- (void)getVerificationCode:(UIButton *)btn{
    if ([_phone_tf.text checkPhoneNumber]) {
        btn.enabled = NO;
        [[YURequest creatBaseRequest] POST:@"authcode/pay/phone/sendcode.json" parameters:@{@"phone":_phone_tf.text} success:^(id JSON) {
            [self sendVerificationCodeAnimation:btn];
        } failure:nil];
    }else{
        [SVProgressHUD showErrorWithStatus:@"请输入正确的手机号码"];
    }
}

/** 进行绑定 */
- (void)confirmBinding{
    NSString *error = nil;
    if (![_phone_tf.text checkPhoneNumber])
        error = @"请输入正确的手机号码";
    if (_code_tf.text.length == 0)
        error = @"请输入验证码";
    if (error == nil) {//没有错误，进行绑定
        [[YURequest creatBaseRequest] POST:@"user/pay/phone/bind.json" parameters:@{@"phone":_phone_tf.text,@"codeStr":_code_tf.text} success:^(id JSON) {
            BingAlipayController *alipay = [[BingAlipayController alloc] init];
            self.payExt.cashPhone = _phone_tf.text;
            alipay.payExt = self.payExt;
            [self.navigationController pushViewController:alipay animated:YES];
        } failure:nil];
    }else{
        [SVProgressHUD showErrorWithStatus:error];
    }
}

/** 发送验证码 */
- (void)sendVerificationCodeAnimation:(UIButton *)btn{
    [btn setTitle:@"重新发送(30)" forState:UIControlStateNormal];
    [NSTimer scheduledTimerWithTimeInterval:1 repeats:YES block:^(NSTimer * _Nonnull timer) {
        int time = [btn.titleLabel.text substringWithRange:NSMakeRange(5, 2)].intValue;
        [btn setTitle:[NSString stringWithFormat:@"重新发送(%d)",--time] forState:UIControlStateNormal];
        if (time == 0) {
            [timer invalidate];
            [btn setTitle:@"获取验证码" forState:UIControlStateNormal];
            btn.enabled = YES;
        }
    }];
}

@end
