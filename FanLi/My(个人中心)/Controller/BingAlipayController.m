//
//  BingAlipayController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BingAlipayController.h"
#import "YUWebController.h"
#import "NSString+Common.h"
#import "ExchangeCoinsController.h"

@interface BingAlipayController ()
{
    /** 姓名 */
    UITextField *_name_tf;
    /** 验证码 */
    UITextField *_alipay_tf;
}
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation BingAlipayController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"绑定支付宝";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 创建scrollView */
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.backgroundColor = UIColorFromHex(0xF0F0F0);
    
    /** 背景视图 */
    UIView *bg_view = [UIView new];
    [self.scrollView addSubview:bg_view];
    bg_view.backgroundColor = [UIColor whiteColor];
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.equalTo(@0);
        make.width.equalTo(@(kMainWidth));
        make.height.equalTo(@100);
    }];
    /** 手机号码 */
    _name_tf = [UITextField new];
    [bg_view addSubview:_name_tf];
    _name_tf.placeholder = @"请输入您的真实姓名";
    _name_tf.font = CurrencyFont(14);
    _name_tf.clearButtonMode = UITextFieldViewModeWhileEditing;
    [_name_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.equalTo(@0);
        make.leading.equalTo(@15);
        make.height.equalTo(@50);
    }];
    UIView *line1 = [UIView new];
    [bg_view addSubview:line1];
    line1.backgroundColor = UIColorFromHex(0xEFEFF4);
    [line1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(_name_tf);
        make.trailing.equalTo(@-15);
        make.top.equalTo(_name_tf.mas_bottom);
        make.height.equalTo(@1);
    }];
    /** 验证码 */
    _alipay_tf = [UITextField new];
    [bg_view addSubview:_alipay_tf];
    _alipay_tf.placeholder = @"请输入支付宝账号";
    _alipay_tf.font = CurrencyFont(14);
    _alipay_tf.keyboardType = UIKeyboardTypeNumberPad;
    [_alipay_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.height.equalTo(_name_tf);
        make.trailing.equalTo(_name_tf);
        make.top.equalTo(_name_tf.mas_bottom);
    }];
    /** 注册按钮 */
    UIButton *register_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scrollView addSubview:register_btn];
    register_btn.titleLabel.font = CurrencyFont(14);
    [register_btn setTitle:@"确认绑定" forState:UIControlStateNormal];
    register_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    [register_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    register_btn.layer.masksToBounds = YES;
    register_btn.layer.cornerRadius = 4;
    [register_btn addTarget:self action:@selector(confirmBinding) forControlEvents:UIControlEventTouchUpInside];
    [register_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(bg_view).offset(15);
        make.trailing.equalTo(bg_view).offset(-15);
        make.top.equalTo(bg_view.mas_bottom).offset(15);
        make.height.equalTo(@40);
    }];
    /** 提示信息 */
    UILabel * descLabel = [UILabel new];
    [self.scrollView addSubview:descLabel];
    descLabel.numberOfLines = 0;
    descLabel.font = CurrencyFont(13);
    descLabel.textColor = UIColorFromHex(0x999999);
    descLabel.text = @"提示:\r\n您还没设置过收款支付宝账户, 请填写通过实名认证的支付宝账户信息, 否则无法正常收款。";
    [descLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(register_btn).offset(5);
        make.trailing.equalTo(register_btn).offset(-5);
        make.top.equalTo(register_btn.mas_bottom).offset(30);
    }];
    /** 创建查看教程item */
    UIBarButtonItem *courseItem = [[UIBarButtonItem alloc] initWithTitle:@"查看教程" style:UIBarButtonItemStyleDone target:self action:@selector(SeeCourseItem)];
    [courseItem setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    courseItem.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = courseItem;
}

/** 查看教程 */
- (void)SeeCourseItem{
    YUWebController *web = [[YUWebController alloc] init];
    web.title_string = @"支付宝账号绑定指南";
    web.url = [NSURL URLWithString:SeeCourseURL];
    [self.navigationController pushViewController:web animated:YES];
}

/** 确认绑定支付宝 */
- (void)confirmBinding{
    NSString *error = nil;
    if (_name_tf.text.length == 0)
        error = @"请输入姓名";
    if (_alipay_tf.text.length == 0){
        error = @"请输入支付宝账号";
    }else if (!([_alipay_tf.text checkPhoneNumber] || [_alipay_tf.text checkEmailAddress]))
        error = @"您填写的支付宝账号有误, 正确的支付宝账号为邮箱或手机号";
    if (error == nil) {//没有错误，进行绑定
        [self bindAlipay];
    }else{
        [SVProgressHUD showErrorWithStatus:error];
    }
}

/** 绑定支付宝 */
- (void)bindAlipay{
    [[YURequest creatBaseRequest] POST:@"user/pay/account/bind.json" parameters:@{@"accountType":@"1",@"account":_alipay_tf.text,@"accountName":_name_tf.text} success:^(id JSON) {
        self.payExt.accountName = _name_tf.text;
        self.payExt.account = _alipay_tf.text;
        self.payExt.accountType = Alipay;
        UIViewController *myVC = self.navigationController.childViewControllers.firstObject;
        [self.navigationController popToRootViewControllerAnimated:YES];
        ExchangeCoinsController *ec = [[ExchangeCoinsController alloc] init];
        ec.payExt = self.payExt;
        [myVC.navigationController pushViewController:ec animated:YES];
    } failure:nil];
}

@end
