//
//  CoinsMarketController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CoinsMarketController.h"
#import "UIImage+Common.h"
#import "CoinsMarketCell.h"
#import "ExchangeGiftController.h"

@interface CoinsMarketController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;
/** 我的金币 */
@property (nonatomic, strong) UILabel *my_coins_lb;
/** 需要金币 */
@property (nonatomic, strong) UILabel *need_coins_lb;
/** 几件商品 */
@property (nonatomic, strong) UILabel *number_of_goods_lb;
/** 兑换 */
@property (nonatomic, strong) UIButton *exchange_btn;
/** 选择商品的数组 */
@property (nonatomic, strong) NSMutableArray *selectArray;
/** 网络商品数组 */
@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation CoinsMarketController

- (NSMutableArray *)selectArray{
    if (!_selectArray) {
        _selectArray = [NSMutableArray array];
    }
    return _selectArray;
}

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"金币商城";
    /** 创建子视图 */
    [self creatSubViews];
    /** 获取网络数据 */
    [self getGiftData];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.my_coins_lb.text = @([User shareUser].ycoins).stringValue;
}

/** 创建子视图 */
- (void)creatSubViews{
    WS(weakSelf);
    /** 创建底部视图 */
    UIView *bottom_view = [UIView new];
    [self.view addSubview:bottom_view];
    bottom_view.backgroundColor = [UIColor whiteColor];
    [bottom_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.leading.trailing.equalTo(@0);
        make.height.equalTo(@60);
    }];
    /** 创建兑换按钮 */
    self.exchange_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [bottom_view addSubview:self.exchange_btn];
    [self.exchange_btn setTitle:@"兑换" forState:UIControlStateNormal];
    self.exchange_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    self.exchange_btn.titleLabel.font = CurrencyFont(16);
    [self.exchange_btn setBackgroundImage:[UIImage imageWithColor:UIColorFromHex(0xC8C8C8)] forState:UIControlStateNormal];
    [self.exchange_btn setBackgroundImage:[UIImage imageWithColor:UIColorFromHex(0xFF5E56)] forState:UIControlStateSelected];
    self.exchange_btn.userInteractionEnabled = NO;
    self.exchange_btn.layer.masksToBounds = YES;
    self.exchange_btn.layer.cornerRadius = 4;
    [self.exchange_btn addTarget:self action:@selector(exchange) forControlEvents:UIControlEventTouchUpInside];
    [self.exchange_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@10);
        make.bottom.trailing.equalTo(@-10);
        make.width.equalTo(@100);
    }];
    /** 我的金币文本 */
    UILabel *my_coins_text = [UILabel new];
    [bottom_view addSubview:my_coins_text];
    my_coins_text.font = CurrencyFont(14);
    my_coins_text.textColor = UIColorFromHex(0x666666);
    my_coins_text.text = @"我的金币:";
    [my_coins_text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@10);
        make.bottom.equalTo(bottom_view.mas_centerY).offset(-3);
    }];
    /** 我的金币 */
    self.my_coins_lb = [UILabel new];
    [bottom_view addSubview:self.my_coins_lb];
    self.my_coins_lb.font = CurrencyFont(14);
    self.my_coins_lb.textColor = UIColorFromHex(0xFF5E56);
    self.my_coins_lb.text = @([User shareUser].ycoins).stringValue;
    [self.my_coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(my_coins_text);
        make.leading.equalTo(my_coins_text.mas_trailing);
    }];
    /** 选择了几件商品 */
    self.number_of_goods_lb = [UILabel new];
    [bottom_view addSubview:self.number_of_goods_lb];
    self.number_of_goods_lb.font = CurrencyFont(14);
    self.number_of_goods_lb.textColor = UIColorFromHex(0x666666);
    self.number_of_goods_lb.text = @"0件商品，需要金币:";
    [self.number_of_goods_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@10);
        make.top.equalTo(bottom_view.mas_centerY).offset(3);
    }];
    /** 需要的金币 */
    self.need_coins_lb = [UILabel new];
    [bottom_view addSubview:self.need_coins_lb];
    self.need_coins_lb.font = CurrencyFont(14);
    self.need_coins_lb.textColor = UIColorFromHex(0xFF5E56);
    self.need_coins_lb.text = @"0";
    [self.need_coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(weakSelf.number_of_goods_lb);
        make.leading.equalTo(weakSelf.number_of_goods_lb.mas_trailing);
    }];
    /** 创建collectionView */
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    CGFloat width = (kMainWidth - 5 * 3) / 2;
    layout.itemSize = CGSizeMake(width, width * 1.310);
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.view addSubview:self.collectionView];
    self.collectionView.allowsMultipleSelection = YES;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(@0);
        make.bottom.equalTo(bottom_view.mas_top);
    }];
    [self.collectionView registerClass:[CoinsMarketCell class] forCellWithReuseIdentifier:@"CoinsMarketCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CoinsMarketCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CoinsMarketCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftModel *model = self.array[indexPath.item];
    [self.selectArray addObject:model];
    [self changeSelectedCoins];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    GiftModel *model = self.array[indexPath.item];
    [self.selectArray removeObject:model];
    [self changeSelectedCoins];
}

- (void)changeSelectedCoins{
    NSInteger coins = 0;
    for (GiftModel *model in self.selectArray) {
        coins += model.ycoinPrice;
    }
    self.need_coins_lb.text = @(coins).stringValue;
    if (coins > [User shareUser].ycoins || coins == 0) {
        self.exchange_btn.userInteractionEnabled = NO;
        self.exchange_btn.selected = NO;
    }else{
        self.exchange_btn.selected = YES;
        self.exchange_btn.userInteractionEnabled = YES;
    }
}

/** 进行兑换按钮 */
- (void)exchange{
    ExchangeGiftController *egc = [[ExchangeGiftController alloc] init];
    egc.gifts = [NSArray arrayWithArray:self.selectArray];
    [self.navigationController pushViewController:egc animated:YES];
    for (CoinsMarketCell *cell in [self.collectionView visibleCells]) {
        cell.selected = NO;
    }
    self.exchange_btn.selected = NO;
    self.exchange_btn.userInteractionEnabled = NO;
    [self.selectArray removeAllObjects];
    [self changeSelectedCoins];
    [self.collectionView reloadData];
}

- (void)getGiftData{
    [[YURequest creatBaseRequest] POST:@"gift/list.json" parameters:@{@"pagesize":@"10000"} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            GiftModel *model = [[GiftModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        [self.collectionView reloadData];
    } failure:nil];
}

@end
