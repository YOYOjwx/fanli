//
//  CoinsRecordController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CoinsRecordController.h"
#import "CoinsRecordCell.h"
#import "YURefreshHeader.h"

@interface CoinsRecordController ()

/** 存放数据 */
@property (nonatomic, strong) NSMutableArray *array;
/** 没有更多了 */
@property (nonatomic, strong) UIView *footer_view;
/** 背景图片 */
@property (nonatomic, strong) UIImageView *bg_image;

@end

@implementation CoinsRecordController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (UIView *)footer_view{
    if (!_footer_view) {
        _footer_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 40)];
        UILabel *no_more_lb = [UILabel new];
        [_footer_view addSubview:no_more_lb];
        no_more_lb.text = @"没有更多了!";
        no_more_lb.font = CurrencyFont(14);
        [no_more_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
    }
    return _footer_view;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置背景颜色 */
    self.tableView.backgroundColor = [UIColor clearColor];
    /** 设置上边距 */
    self.tableView.contentInset = UIEdgeInsetsMake(10, 0, 0, 0);
    /** 去除横向 */
    self.tableView.separatorStyle = NO;
    /** 设置行高 */
    self.tableView.rowHeight = 80;
    /** 注册Cell */
    [self.tableView registerClass:[CoinsRecordCell class] forCellReuseIdentifier:@"CoinsRecordCell"];
    /** 创建背景视图 */
    self.bg_image = [UIImageView new];
    [self.view addSubview:self.bg_image];
    self.bg_image.image = [UIImage imageNamed:@"任何地方空白页面"];
    [self.bg_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
    }];
    WS(weakSelf);
    /** 下啦加载刷新 */
    self.tableView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.array removeAllObjects];
        [weakSelf getRecordData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CoinsRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CoinsRecordCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

/** 获取网络数据 */
- (void)getRecordData{
    NSInteger curpage = self.array.count / 20 + 1;
    [RecordModel getRecordWithCurPage:curpage payType:payTypeCash success:^(NSMutableArray<RecordModel *> *array) {
        if (array.count % 20 != 0 || array.count == 0) {//代表没有更多数据了
            self.tableView.mj_footer = nil;
            self.tableView.tableFooterView = self.footer_view;
        }else{
            self.tableView.tableFooterView = nil;
            self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getRecordData)];
        }
        [self.array addObjectsFromArray:array];
        if (self.array.count != 0) {
            self.bg_image.hidden = YES;
        }else{
            self.bg_image.hidden = NO;
            self.tableView.tableFooterView = nil;
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

@end
