//
//  EarnCoinsController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "EarnCoinsController.h"
#import "EarnListCell.h"
#import "EarnCoinsHeader.h"
#import "YUWebController.h"
#import "InviteUserListModel.h"
#import "SelectedShareView.h"
#import "DownAppShare.h"
#import "NoobTaskController.h"
#import "CoinsMarketController.h"
#import "TaobaoDetail.h"

@interface EarnCoinsController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation EarnCoinsController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"赚钱任务";
    /** 创建金币商城按钮 */
    UIBarButtonItem *market = [[UIBarButtonItem alloc] initWithTitle:@"金币商城" style:UIBarButtonItemStylePlain target:self action:@selector(market)];
    market.tintColor = UIColorFromHex(0x666666);
    [market setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(14)} forState:UIControlStateNormal];
//    self.navigationItem.rightBarButtonItem = market;
    /** 获取数组 */
    [self getArray];
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.rowHeight = 65;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = NO;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    /** 注册cell */
    [self.tableView registerClass:[EarnListCell class] forCellReuseIdentifier:@"EarnListCell"];
    EarnCoinsHeader *header = [[EarnCoinsHeader alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, kMainWidth * 0.216)];
    header.dic = @{@"xid":@"1337"};
    header.url = @"openApi/category/detail";
    header.click = ^(CateItemModel *model) {
        [TaobaoDetail openCycleDetail:self.navigationController cateItem:model];
    };
    self.tableView.tableHeaderView = header;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    EarnListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EarnListCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 35;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    /** 因为这里只有一个头部视图所以不想做重用了 */
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 35)];
    view.backgroundColor = UIColorFromHex(0xF0F0F0);
    UILabel *label = [UILabel new];
    label.text = @"赚钱任务";
    label.font = CurrencyFont(13);
    label.textColor = UIColorFromHex(0x666666);
    [view addSubview:label];
    [label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(@0);
        make.leading.equalTo(@10);
    }];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.row) {
        case 0:[self noobTask];break;/** 新手任务 */
        case 1:[self InvitingFriends];break;/** 邀请好友 */
        case 2:[self shareOrder];break;/** 分享订单 */
        case 3:[self orderTask];break;/** 下单任务 */
        default:break;
    }
}

/** 新手任务 */
- (void)noobTask{
    NoobTaskController *ntc = [[NoobTaskController alloc] init];
    [self.navigationController pushViewController:ntc animated:YES];
}

/** 邀请好友 */
- (void)InvitingFriends{
    InviteUserListModel *invite = [InviteUserListModel share];
    NSString *inviteUserListUrl = [invite.urlValue stringByReplacingOccurrencesOfString:@"{uid}" withString:UD_UID];
    inviteUserListUrl = [inviteUserListUrl stringByReplacingOccurrencesOfString:@"{token}" withString:UD_TOKEN];
    YUWebController *inviteWeb = [[YUWebController alloc] init];
    inviteWeb.url = [NSURL URLWithString:inviteUserListUrl];
    inviteWeb.title_string = @"邀请好友";
    inviteWeb.hidesBottomBarWhenPushed = YES;
    UIBarButtonItem *inviteItem = [[UIBarButtonItem alloc] initWithTitle:@"邀请" style:UIBarButtonItemStyleDone target:self action:@selector(inviteUser)];
    inviteItem.tintColor = [UIColor blackColor];
    [inviteItem setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    inviteWeb.rightBarButtonItems = @[inviteItem];
    [self.navigationController pushViewController:inviteWeb animated:YES];
}

/** 邀请好友分享朋友圈 */
- (void)inviteUser{
    [SelectedShareView shareWithURL:[DownAppShare share].urlValue imageUrl:[DownAppShare share].urlIcon title:@"你的好友喊你来领红包" description:@"使用返利大王就能获得60集分宝,赶紧来领取。" array:@[@"QQ",@"QQ空间",@"微信",@"朋友圈"] success:nil];
}

/** 分享订单 */
- (void)shareOrder{
    [self.navigationController pushViewController:[NSClassFromString(@"TaobaoOrderController") new] animated:YES];
}

/** 下单任务 */
- (void)orderTask{
    YUWebController *orderVC = [[YUWebController alloc] init];
    orderVC.url = [NSURL URLWithString:OrderDetailURL];
    orderVC.title_string = @"下单任务";
    UIBarButtonItem *rule_item = [[UIBarButtonItem alloc] initWithTitle:@"规则" style:UIBarButtonItemStyleDone target:self action:@selector(pushRuleWebView)];
    rule_item.tintColor = [UIColor blackColor];
    [rule_item setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    orderVC.rightBarButtonItems = @[rule_item];
    [self.navigationController pushViewController:orderVC animated:YES];
}
- (void)pushRuleWebView{
    YUWebController *orderVC = self.navigationController.childViewControllers.lastObject;
    YUWebController *ruleVC = [[YUWebController alloc] init];
    ruleVC.title_string = @"下单任务细则";
    ruleVC.url = [NSURL URLWithString:OrderTaskRuleURL];
    [orderVC.navigationController pushViewController:ruleVC animated:YES];
}

- (void)getArray{
    NSArray *temArray = @[
  @{@"icon":@"新手任务",@"title":@"新手任务",@"detail":@"首次复制淘宝标题得金币"},
  @{@"icon":@"邀请好友-任务",@"title":@"邀请好友",@"detail":@"邀请好友成功得金币"},
  @{@"icon":@"分享订单",@"title":@"分享订单",@"detail":@"分享购买订单得5金币"},
  @{@"icon":@"下单任务",@"title":@"下单任务",@"detail":@"最高奖励1300金币!"}];
    for (NSDictionary *dic in temArray) {
        EarnListModel *model = [[EarnListModel alloc] init];
        [model setValuesForKeysWithDictionary:dic];
        [self.array addObject:model];
    }
}

/** 金币商城 */
- (void)market{
    CoinsMarketController *cmc = [[CoinsMarketController alloc] init];
    [self.navigationController pushViewController:cmc animated:YES];
}

@end
