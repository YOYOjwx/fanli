//
//  ExchangeCoinsController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"
#import "UserPayExt.h"

@interface ExchangeCoinsController : BaseViewController

@property (nonatomic, strong) UserPayExt *payExt;

@end
