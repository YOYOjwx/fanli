//
//  ExchangeCoinsController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ExchangeCoinsController.h"
#import "YUWebController.h"
#import "UIImage+Common.h"
#import "ExchangeSuccessController.h"

@interface ExchangeCoinsController ()

@property (nonatomic, strong) UIScrollView *scrollView;
/** 可用金币数量 */
@property (nonatomic, strong) UILabel *available_coins_lb;
/** 输入金币数目 */
@property (nonatomic, strong) UITextField *coins_tf;
/** 兑换 */
@property (nonatomic, strong) UIButton *exchange_btn;

@end

@implementation ExchangeCoinsController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"兑换集分宝";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    WS(weakSelf);
    /** 创建scrollView */
    self.scrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:self.scrollView];
    self.scrollView.alwaysBounceVertical = YES;
    self.scrollView.backgroundColor = UIColorFromHex(0xF0F0F0);
    /** 可用金币 */
    UILabel *available_coins_text = [UILabel new];
    [self.scrollView addSubview:available_coins_text];
    available_coins_text.text = @"可用金币: ";
    available_coins_text.textColor = UIColorFromHex(0x999999);
    available_coins_text.font = CurrencyFont(12);
    [available_coins_text mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@15);
        make.top.equalTo(@10);
    }];
    User *user = [User shareUser];
    self.available_coins_lb = [UILabel new];
    [self.scrollView addSubview:self.available_coins_lb];
    self.available_coins_lb.textColor = UIColorFromHex(0xFF5E56);
    self.available_coins_lb.font = CurrencyFont(12);
    self.available_coins_lb.text = [NSString stringWithFormat:@"%d",user.ycoins];
    [self.available_coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(available_coins_text);
        make.leading.equalTo(available_coins_text.mas_trailing);
    }];
    /** 创建输入金币数目 */
    UIView *tf_bg_view = [UIView new];
    [self.scrollView addSubview:tf_bg_view];
    tf_bg_view.backgroundColor = [UIColor whiteColor];
    [tf_bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(available_coins_text.mas_bottom).offset(10);
        make.leading.equalTo(@0);
        make.width.mas_equalTo(kMainWidth);
        make.height.equalTo(@50);
    }];
    self.coins_tf = [UITextField new];
    [tf_bg_view addSubview:self.coins_tf];
    self.coins_tf.placeholder = @"输入需要兑换的集分宝数, 1金币＝1集分宝";
    self.coins_tf.textColor = UIColorFromHex(0x666666);
    [self.coins_tf addTarget:self action:@selector(valueChange:) forControlEvents:UIControlEventAllEditingEvents];
    self.coins_tf.font = CurrencyFont(14);
    self.coins_tf.keyboardType = UIKeyboardTypeNumberPad;
    [self.coins_tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.trailing.equalTo(@0);
        make.leading.equalTo(@15);
    }];
    /** 立即兑换 */
    self.exchange_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scrollView addSubview:self.exchange_btn];
    [self.exchange_btn setTitle:@"立即兑换" forState:UIControlStateNormal];
    [self.exchange_btn setBackgroundImage:[UIImage imageWithColor:UIColorFromHex(0xE0E0E0)] forState:UIControlStateNormal];
    [self.exchange_btn setBackgroundImage:[UIImage imageWithColor:UIColorFromHex(0xFF5E56)] forState:UIControlStateSelected];
    self.exchange_btn.layer.masksToBounds = YES;
    self.exchange_btn.layer.cornerRadius = 4;
    self.exchange_btn.titleLabel.font = CurrencyFont(14);
    self.exchange_btn.userInteractionEnabled = NO;
    [self.exchange_btn addTarget:self action:@selector(exchangeCoins) forControlEvents:UIControlEventTouchUpInside];
    [self.exchange_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tf_bg_view.mas_bottom).offset(15);
        make.leading.equalTo(@15);
        make.trailing.equalTo(tf_bg_view).offset(-15);
        make.height.equalTo(@40);
    }];
    /** 集分宝怎么用 */
    UIButton *set_points_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.scrollView addSubview:set_points_btn];
    [set_points_btn setTitle:@" 集分宝怎么用" forState:UIControlStateNormal];
    [set_points_btn setImage:[UIImage imageNamed:@"集分宝说明"] forState:UIControlStateNormal];
    set_points_btn.titleLabel.font = CurrencyFont(12);
    [set_points_btn setTitleColor:UIColorFromHex(0x666666) forState:UIControlStateNormal];
    [set_points_btn addTarget:self action:@selector(jiFenBaoUse) forControlEvents:UIControlEventTouchUpInside];
    [set_points_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.exchange_btn.mas_bottom).offset(30);
        make.trailing.equalTo(weakSelf.exchange_btn);
    }];
    /** 介绍信息 */
    UILabel *introduce_lb = [UILabel new];
    [self.scrollView addSubview:introduce_lb];
    introduce_lb.text = [NSString stringWithFormat:@"提示:\r\n您的收款支付宝账户为: %@\r\n兑换的申请将会在2个工作日内审核并发放到您的支付宝账户上。",self.payExt.account];
    introduce_lb.numberOfLines = 0;
    introduce_lb.font = CurrencyFont(13);
    introduce_lb.textColor = UIColorFromHex(0x999999);
    [introduce_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(weakSelf.exchange_btn).offset(15);
        make.trailing.equalTo(weakSelf.exchange_btn).offset(-15);
        make.top.equalTo(set_points_btn.mas_bottom).offset(40);
    }];
}

/** 集分宝怎么用 */
- (void)jiFenBaoUse{
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:JiFenBaoURL];
    web.title_string = @"使用说明";
    [self.navigationController pushViewController:web animated:YES];
}

/** tf文本改变 */
- (void)valueChange:(UITextField *)tf{
    if (tf.text.length) {
        self.exchange_btn.userInteractionEnabled = YES;
        self.exchange_btn.selected = YES;
    }else{
        self.exchange_btn.userInteractionEnabled = NO;
        self.exchange_btn.selected = NO;
    }
    int coins = tf.text.intValue;
    if (coins > [User shareUser].ycoins) {
        tf.text = [NSString stringWithFormat:@"%d",[User shareUser].ycoins];
    }
}

/** 提现 */
- (void)exchangeCoins{
    NSString *error = nil;
    if (self.coins_tf.text.intValue < 120)
        error = @"金币最少兑换120个";
    if (error) {
        [SVProgressHUD showErrorWithStatus:error];
    }else{
        [[YURequest creatBaseRequest] POST:@"ycoin/apply.json" parameters:@{@"count":self.coins_tf.text,@"type":@"-1"} success:^(id JSON) {
            NSNumber *coins = JSON;
            [User shareUser].ycoins -= coins.intValue;
            UIViewController *myVC = self.navigationController.childViewControllers.firstObject;
            [self.navigationController popToRootViewControllerAnimated:NO];
            ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
            esc.navigation_title = @"兑换成功";
            esc.success_title = @"兑换请求成功";
            esc.btn_name = @"查看兑换记录";
            esc.pushController = @"ExchangeRecordController";
            esc.hidesBottomBarWhenPushed = YES;
            [myVC.navigationController pushViewController:esc animated:YES];
        } failure:nil];
    }
}

@end
