//
//  ExchangeGiftController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"
#import "GiftModel.h"

@interface ExchangeGiftController : BaseViewController

@property (nonatomic, strong) NSArray<GiftModel *> *gifts;

@end

@interface ExchangeGiftCell : UITableViewCell

@property (nonatomic, strong) GiftModel *model;

@end
