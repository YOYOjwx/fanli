//
//  ExchangeGiftController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ExchangeGiftController.h"
#import "YUTextField.h"
#import "NSString+Common.h"
#import "YUPickerView.h"

@interface ExchangeGiftController ()<UITableViewDataSource>

/** 收货人 */
@property (nonatomic, strong) YUTextField *name;
/** 手机号码 */
@property (nonatomic, strong) YUTextField *phone;
/** 邮政编码 */
@property (nonatomic, strong) YUTextField *postcode;
/** 城市 */
@property (nonatomic, strong) YUTextField *city;
/** 详细地址 */
@property (nonatomic, strong) YUTextField *location;

@end

@implementation ExchangeGiftController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 填写标题 */
    self.navigationItem.title = @"兑换礼品";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    WS(weakSelf);
    UIScrollView *scrollView = [UIScrollView new];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    UIView *contentView = [UIView new];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
        make.width.equalTo(weakSelf.view);
    }];
    
    UIView *bg_view = [UIView new];
    [scrollView addSubview:bg_view];
    bg_view.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [bg_view addGestureRecognizer:tap];
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(@0);
    }];
    UIView *red1 = [UIView new];
    [scrollView addSubview:red1];
    red1.backgroundColor = UIColorFromHex(0xFF5E56);
    [red1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@15);
        make.leading.equalTo(@30);
        make.width.equalTo(@3);
        make.height.equalTo(@15);
    }];
    UILabel *lb1 = [UILabel new];
    [scrollView addSubview:lb1];
    lb1.text = @"兑换的奖品";
    lb1.font = CurrencyFont(14);
    [lb1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(red1);
        make.leading.equalTo(red1.mas_trailing).offset(10);
    }];
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [scrollView addSubview:tableView];
    tableView.dataSource = self;
    tableView.rowHeight = 70;
    tableView.separatorStyle = NO;
    tableView.userInteractionEnabled = NO;
    [tableView registerClass:[ExchangeGiftCell class] forCellReuseIdentifier:@"ExchangeGiftCell"];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(red1.mas_bottom).offset(5);
        make.leading.equalTo(red1);
        make.trailing.equalTo(@-10);
        make.height.mas_equalTo(weakSelf.gifts.count * 70 + 10);
    }];
    UIView *red2 = [UIView new];
    [scrollView addSubview:red2];
    red2.backgroundColor = red1.backgroundColor;
    [red2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(tableView.mas_bottom).offset(5);
        make.leading.equalTo(@30);
        make.width.equalTo(@3);
        make.height.equalTo(@15);
    }];
    UILabel *lb2 = [UILabel new];
    [scrollView addSubview:lb2];
    lb2.text = @"收货地址";
    lb2.font = CurrencyFont(14);
    [lb2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(red2);
        make.leading.equalTo(red2.mas_trailing).offset(10);
    }];
    self.name = [YUTextField new];
    [scrollView addSubview:self.name];
    self.name.title = @"收货人";
    self.name.textField.placeholder = @"请输入收货人姓名";
    [self.name mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(red2.mas_bottom).offset(15);
        make.leading.equalTo(red2);
        make.trailing.equalTo(@-30);
        make.height.equalTo(@50);
    }];
    self.phone = [YUTextField new];
    [scrollView addSubview:self.phone];
    self.phone.title = @"手机号码";
    self.phone.textField.placeholder = @"请输入手机号码";
    self.phone.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.phone mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_name.mas_bottom).offset(-0.5);
        make.trailing.height.leading.equalTo(_name);
    }];
    self.postcode = [YUTextField new];
    [scrollView addSubview:self.postcode];
    self.postcode.title = @"邮政编码";
    self.postcode.textField.placeholder = @"请输入邮政编码";
    self.postcode.textField.keyboardType = UIKeyboardTypeNumberPad;
    [self.postcode mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_phone.mas_bottom).offset(-0.5);
        make.trailing.height.leading.equalTo(_name);
    }];
    self.city = [YUTextField new];
    [scrollView addSubview:self.city];
    self.city.title = @"所在省市";
    self.city.textField.placeholder = @"点击设置收货地址";
    self.city.textField.userInteractionEnabled = NO;
    UITapGestureRecognizer *cityTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(setCity)];
    [self.city addGestureRecognizer:cityTap];
    [self.city mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_postcode.mas_bottom).offset(-0.5);
        make.trailing.height.leading.equalTo(_name);
    }];
    self.location = [YUTextField new];
    [scrollView addSubview:self.location];
    self.location.title = @"详细地址";
    self.location.textField.placeholder = @"请务必填写街道(或乡镇、区)和详细地址";
    [self.location mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_city.mas_bottom).offset(-0.5);
        make.trailing.height.leading.equalTo(_name);
    }];
    UIView *line = [UIView new];
    [scrollView addSubview:line];
    line.backgroundColor = UIColorFromHex(0x999999);
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_name.mas_centerY);
        make.bottom.equalTo(_location.mas_centerY);
        make.leading.equalTo(@15);
        make.width.equalTo(@0.5);
    }];
    UILabel *red_lb = [UILabel new];
    [scrollView addSubview:red_lb];
    red_lb.font = CurrencyFont(12);
    red_lb.textColor = UIColorFromHex(0xFF5E56);
    red_lb.numberOfLines = 0;
    red_lb.text = @"请务必填写街道(或乡镇、区，例如: 西湖街道)\r\n和详细地址";
    [red_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(_name);
        make.top.equalTo(_location.mas_bottom).offset(15);
    }];
    UIButton *determine = [UIButton buttonWithType:UIButtonTypeCustom];
    [scrollView addSubview:determine];
    determine.backgroundColor = UIColorFromHex(0xFF5E56);
    [determine setTitle:@"确定" forState:UIControlStateNormal];
    determine.titleLabel.font = CurrencyFont(14);
    determine.layer.masksToBounds = YES;
    determine.layer.cornerRadius = 5;
    [determine addTarget:self action:@selector(determine) forControlEvents:UIControlEventTouchUpInside];
    [determine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(red_lb.mas_bottom).offset(15);
        make.leading.trailing.equalTo(red_lb);
        make.height.equalTo(@40);
    }];
    [bg_view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(determine.mas_bottom).offset(15);
    }];
    UILabel *explain = [UILabel new];
    [scrollView addSubview:explain];
    explain.textColor = UIColorFromHex(0x666666);
    explain.font = CurrencyFont(12);
    explain.numberOfLines = 0;
    explain.text = @"说明:\r\n\r\n1、奖品以商家发送的为准，赠送小礼品属于商家行为，与柚柚育儿无关，图片仅作为参考。\r\n\r\n2、兑换成功后，奖品会在两周内到货，视地区远近，到货时间会有不同。\r\n\r\n3、审核通过后，将会直接发货，地址修改无效。\r\n\r\n客服反馈QQ: 2848399874";
    [explain mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(determine);
        make.top.equalTo(bg_view.mas_bottom).offset(15);
    }];
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(explain.mas_bottom).offset(15);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.gifts.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ExchangeGiftCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ExchangeGiftCell" forIndexPath:indexPath];
    cell.model = self.gifts[indexPath.row];
    return cell;
}

/** 确认发货 */
- (void)determine{
    NSString *error = nil;
    if (!error && _name.textField.text.length == 0)
        error = @"请输入收货人姓名";
    if (!error && ![_phone.textField.text checkPhoneNumber])
        error = @"请输入正确的手机号";
    if (!error && ![_postcode.textField.text checkZipCode])
        error = @"请输入正确的邮政编码";
    if (!error && _city.textField.text.length == 0)
        error = @"所在省市不能为空";
    if (!error && _location.textField.text.length == 0)
        error = @"详细地址不能为空";
    if (error) {
        [SVProgressHUD showErrorWithStatus:error];
    }else{/** 没有错误开始确定发货 */
        
    }
}

/** 设置发货地址 */
- (void)setCity{
    YUPickerView *picker = [YUPickerView new];
    [self.view addSubview:picker];
    SetCellModel *model = [[SetCellModel alloc] init];
    model.setType = PickerTypeCity;
    model.title = @"选择所在省市";
    picker.model = model;
    picker.determine = ^(NSString *location) {
        _city.textField.text = location;
    };
    [picker mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
}

/** 键盘回弹 */
- (void)dismissKeyboard{
    [self.view endEditing:YES];
}

@end

@interface ExchangeGiftCell ()

/** 商品图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 商品标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 商品金币数目 */
@property (nonatomic, strong) UILabel *coins_lb;

@end

@implementation ExchangeGiftCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = NO;
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@10);
            make.width.equalTo(@80);
            make.height.equalTo(@60);
            make.leading.equalTo(@0);
        }];
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.numberOfLines = 2;
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_icon_image);
            make.leading.equalTo(_icon_image.mas_trailing).offset(10);
            make.trailing.equalTo(@-10);
        }];
        UIImageView *coins_icon_image = [UIImageView new];
        [self.contentView addSubview:coins_icon_image];
        coins_icon_image.image = [UIImage imageNamed:@"金币图标"];
        [coins_icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_icon_image);
            make.leading.equalTo(_title_lb);
        }];
        self.coins_lb = [UILabel new];
        [self.contentView addSubview:self.coins_lb];
        self.coins_lb.textColor = UIColorFromHex(0x999999);
        self.coins_lb.font = CurrencyFont(12);
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(coins_icon_image);
            make.leading.equalTo(coins_icon_image.mas_trailing);
        }];
    }
    return self;
}

- (void)setModel:(GiftModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _title_lb.text = model.giftDesc;
    _coins_lb.text = @(model.ycoinPrice).stringValue;
}

@end
