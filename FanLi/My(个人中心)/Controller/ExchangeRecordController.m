//
//  ExchangeRecordController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ExchangeRecordController.h"
#import "YUSegmented.h"
#import "CoinsRecordController.h"
#import "PrizeRecordController.h"

@interface ExchangeRecordController ()<UIScrollViewDelegate>

/** scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;
/** segmented */
@property (nonatomic, strong) YUSegmented *segmented;

@end

@implementation ExchangeRecordController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"兑换记录";
    /** 设置背景颜色 */
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    /** 创建子视图 */
    [self creatSubViews];
}

- (void)creatSubViews{
    WS(weakSelf);
    /** segmented */
    self.segmented = [[YUSegmented alloc] initWithArray:@[@"集分宝",@"奖品"] column:2 click:^(NSInteger index) {
        [self.scrollView setContentOffset:CGPointMake(index * kMainWidth, 0) animated:YES];
    }];
    [self.view addSubview:self.segmented];
    [self.segmented mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(@0);
        make.top.equalTo(@0.5);
        make.height.equalTo(@35);
    }];
    /** scrollView */
    self.scrollView = [UIScrollView new];
    [self.view addSubview:self.scrollView];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(@0);
        make.top.equalTo(weakSelf.segmented.mas_bottom);
    }];
    UIView *contentView = [UIView new];
    [self.scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.height.top.leading.bottom.equalTo(@0);
        make.width.equalTo(weakSelf.view).multipliedBy(2.0);
    }];
    /** 创建集分宝记录 */
    CoinsRecordController *crc = [[CoinsRecordController alloc] initWithStyle:UITableViewStylePlain];
    [self addChildViewController:crc];
    [self.scrollView addSubview:crc.tableView];
    [crc.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.equalTo(@0);
        make.bottom.equalTo(weakSelf.view);
        make.width.mas_equalTo(kMainWidth);
    }];
    /** 创建奖品记录 */
    PrizeRecordController *prc = [[PrizeRecordController alloc] initWithStyle:UITableViewStylePlain];
    [self addChildViewController:prc];
    [self.scrollView addSubview:prc.tableView];
    [prc.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.top.equalTo(@0);
        make.bottom.equalTo(weakSelf.view);
        make.width.mas_equalTo(kMainWidth);
    }];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat x = scrollView.contentOffset.x;
    self.segmented.selectIndex = x / kMainWidth;
}

@end
