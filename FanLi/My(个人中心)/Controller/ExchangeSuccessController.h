//
//  ExchangeSuccessController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"

@interface ExchangeSuccessController : BaseViewController

/** 标题名字 */
@property (nonatomic, strong) NSString *navigation_title;
/** 成功的文字显示 */
@property (nonatomic, strong) NSString *success_title;
/** 按钮的名字 */
@property (nonatomic, strong) NSString *btn_name;
/** 接下来要跳转的子控制器 */
@property (nonatomic, strong) NSString *pushController;

@end
