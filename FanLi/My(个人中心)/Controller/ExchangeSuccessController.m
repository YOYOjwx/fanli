//
//  ExchangeSuccessController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ExchangeSuccessController.h"
#import "YUWebController.h"

@interface ExchangeSuccessController ()

/** scrollView */
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation ExchangeSuccessController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = self.navigation_title;
    /** 创建子试图 */
    [self creatSubViews];
}

/** 创建子试图 */
- (void)creatSubViews{
    WS(weakSelf);
    /** 创建scrollView */
    self.scrollView = [UIScrollView new];
    [self.view addSubview:self.scrollView];
    self.scrollView.backgroundColor = UIColorFromHex(0xF0F0F0);
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    UIView *view = [UIView new];
    [self.scrollView addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
        make.width.equalTo(weakSelf.view);
    }];
    /** 创建成功背景图 */
    UIImageView *success_image = [UIImageView new];
    [self.scrollView addSubview:success_image];
    success_image.userInteractionEnabled = YES;
    success_image.image = [UIImage imageNamed:@"成功页背景"];
    [success_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(@65);
        make.height.equalTo(success_image.mas_width).multipliedBy(0.814);
    }];
    /** 创建成功按钮 */
    UIButton *success_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [success_image addSubview:success_btn];
    [success_btn setTitle:[NSString stringWithFormat:@" %@ ",self.success_title] forState:UIControlStateNormal];
    [success_btn setTitleColor:UIColorFromHex(0x4A4A4A) forState:UIControlStateNormal];
    [success_btn setImage:[UIImage imageNamed:@"支付成功"] forState:UIControlStateNormal];
    success_btn.titleLabel.font = CurrencyFont(16);
    [success_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.top.equalTo(@45);
    }];
    /** 创建push按钮 */
    UIButton *push_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [success_image addSubview:push_btn];
    push_btn.backgroundColor = UIColorFromHex(0xF8E71C);
    push_btn.titleLabel.font = CurrencyFont(13);
    [push_btn setTitleColor:UIColorFromHex(0x8B572A) forState:UIControlStateNormal];
    [push_btn setTitle:self.btn_name forState:UIControlStateNormal];
    [push_btn addTarget:self action:@selector(pushSubController) forControlEvents:UIControlEventTouchUpInside];
    push_btn.layer.masksToBounds = YES;
    push_btn.layer.cornerRadius = 17.5;
    [push_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(@0);
        make.width.equalTo(@120);
        make.height.equalTo(@35);
        make.bottom.equalTo(@-10);
    }];
    /** 创建WebView */
    YUWebController *webVC = [[YUWebController alloc] init];
    webVC.hideProgress = YES;
    webVC.url = [NSURL URLWithString:OrderSchedulelURL];
    [self addChildViewController:webVC];
    [self.scrollView addSubview:webVC.view];
    [webVC.view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(@0);
        make.top.equalTo(success_image.mas_bottom).offset(120);
        make.height.equalTo(@0);
    }];
    /** webView加载完成后执行的方法 */
    __weak __typeof(&*webVC)weakWebVc = webVC;
    webVC.webDidFinish = ^(WKWebView *webView) {
        [webView evaluateJavaScript:@"document.body.clientHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            [weakWebVc.view mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(result);
            }];
        }];
    };
    /** 更新scrollView的contentSize */
    [view mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(webVC.view);
    }];
}

- (void)pushSubController{
    [self.navigationController pushViewController:[NSClassFromString(self.pushController) new] animated:YES];
}

@end
