//
//  MessageCenterController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MessageCenterController.h"
#import "MessageCell.h"

@interface MessageCenterController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation MessageCenterController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"系统消息";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 创建tableView */
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = NO;
    self.tableView.estimatedRowHeight = 40;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    /** 旋转180度 */
    [self getMessageData];
    self.tableView.transform = CGAffineTransformMakeRotation(2 * M_PI_2);
    self.tableView.showsVerticalScrollIndicator = NO;
    /** 注册cell */
    [self.tableView registerClass:[MessageCell class] forCellReuseIdentifier:@"messageCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (void)addMJFooter{
    MJRefreshAutoNormalFooter *mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getMessageData)];
    mj_footer.refreshingTitleHidden = YES;
    mj_footer.transform = CGAffineTransformMakeRotation(2 * M_PI_2);
    self.tableView.mj_footer = mj_footer;
}

- (void)getMessageData{
    NSInteger curPage = self.array.count / 20 + 1;
    int maxSeqId = 0;
    if (self.array.count != 0) {
        maxSeqId = [self.array.lastObject seqId];
    }
    [[YURequest creatBaseRequest] POST:@"message/list.json" parameters:@{@"curPage":@(curPage),@"maxSeqId":@(maxSeqId)} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            MessageModel *model = [[MessageModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        if ([JSON count] % 20 != 0 || [JSON count] == 0) {
            self.tableView.mj_footer = nil;
        }else{
            [self addMJFooter];
        }
        [self.tableView reloadData];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.tableView.mj_footer endRefreshing];
    }];
}

@end
