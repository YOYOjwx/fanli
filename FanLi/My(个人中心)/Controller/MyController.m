//
//  MyController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MyController.h"
#import "LoginController.h"
#import "MyItemCell.h"
#import "MyHeader.h"
#import "YUWebController.h"
#import "CoinHeader.h"
#import "ExchangeCoinsController.h"
#import "BindPhoneController.h"
#import "BingAlipayController.h"
#import "SetController.h"
#import "TaobaoOrderController.h"
#import "ExchangeRecordController.h"
#import "InviteUserListModel.h"
#import "SelectedShareView.h"
#import "DownAppShare.h"
#import "MessageCenterController.h"
#import "MyFollowController.h"
#import "EarnCoinsController.h"
#import "MarketOrderController.h"

@interface MyController ()<UICollectionViewDelegate,UICollectionViewDataSource>

/** 存放模型的数组 */
@property (nonatomic, strong) NSMutableArray *array;
/** 个人背景图片 */
@property (nonatomic, strong) UIImageView *bg_image;

@end

@implementation MyController

- (void)loadView{
    /** 创建collectionView */
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    /** 预估高度 */
    layout.estimatedItemSize = CGSizeMake(50, 50);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView = collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 创建数组 */
    [self creatArray];
    /** 创建子视图 */
    [self initSubViews];
    /** 刷新视图 */
    [self.collectionView performSelector:@selector(reloadData) withObject:nil afterDelay:0];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationItem.title = @"访客";
    /** 设置标题 */
    if (IS_LOGIN) {
        [User getSelfUserSuccess:^(User *user) {
            self.navigationItem.title = user.nickname.length?user.nickname:[NSString stringWithFormat:@"%ld",(long)user.uid];
            [self.collectionView reloadData];
        } failure:nil];
    }else{
        self.navigationItem.title = @"访客";
        [self.collectionView reloadData];
    }
    
}

/** 创建子视图 */
- (void)initSubViews{
    /** 内容小于高度仍然可以滑动 */
    self.collectionView.alwaysBounceVertical = YES;
    /** 注册cell */
    [self.collectionView registerClass:[MyItemCell class] forCellWithReuseIdentifier:@"MyItem"];
    /** 注册个人信息header */
    [self.collectionView registerClass:[MyHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MyHeader"];
    /** 注册金币header */
    [self.collectionView registerClass:[CoinHeader class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Coin"];
    self.bg_image = [UIImageView new];
    [self.view insertSubview:self.bg_image atIndex:0];
    self.bg_image.image = [UIImage imageNamed:@"个人中心背景"];
    self.bg_image.contentMode = UIViewContentModeScaleAspectFill;
    [self.bg_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.centerX.equalTo(@0);
        make.height.mas_equalTo(kMainWidth * 0.3734);
    }];
    UIView *bg_view = [UIView new];
    [self.view insertSubview:bg_view atIndex:1];
    bg_view.backgroundColor = UIColorFromHex(0xF0F0F0);
    [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bg_image.mas_bottom);
        make.leading.trailing.equalTo(@0);
        make.height.equalTo(@100);
    }];
    /** 设置item */
    UIBarButtonItem *setItem = [[UIBarButtonItem alloc] initWithTitle:@"设置" style:UIBarButtonItemStyleDone target:self action:@selector(setUser)];
    [setItem setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    setItem.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = setItem;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.array.count;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.array[section] count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    if (!section) {
        return CGSizeMake(kMainWidth, kMainWidth * 0.3734);
    }else{
        return CGSizeMake(kMainWidth, 90);
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    WS(weakSelf);
    if (indexPath.section) {
        CoinHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"Coin" forIndexPath:indexPath];
        header.user = [User shareUser];
        header.click = ^{
            [weakSelf pushWebURL:CoinCenterURL title:@"金币说明"];
        };
        return header;
    }else{
        MyHeader *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"MyHeader" forIndexPath:indexPath];
        header.user = [User shareUser];
        header.click = ^{
            [weakSelf pushWebURL:CoinCenterURL title:@"金币说明"];
        };
        return header;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MyItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyItem" forIndexPath:indexPath];
    cell.model = self.array[indexPath.section][indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MyItemModel *model = self.array[indexPath.section][indexPath.item];
    switch (model.type) {
        case 0:[self TaobaoOrder];break;/*淘宝订单*/
        case 1:[self MarketOrder];break;/*商城订单*/
        case 2:[self MyCollection];break;/*我的收藏*/
        case 3:[self ExchangeCoins];break;/*兑换集分宝*/
        case 4:[self ExchangeRecord];break;/*兑换记录*/
        case 5:[self InvitingFriends];break;/*邀请好友*/
        case 6:[self MessageCenter];break;/*消息中心*/
        case 7:[self pushWebURL:DWConcactURL title:@"联系客服"];break;/*联系客服*/
        case 8:[self EarnCoins];break;/*赚金币*/
        case 9:[self pushWebURL:HelpCenterURL title:@"帮助中心"];break;/*帮助中心*/
        default:break;
    }
}

/** 设置用户信息 */
- (void)setUser{
    [LoginController isLoginStatus:^(User *user) {
        SetController *set = [[SetController alloc] init];
        set.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:set animated:YES];
    }];
}

/** 淘宝订单 */
- (void)TaobaoOrder{
    [LoginController isLoginStatus:^(User *user) {
        TaobaoOrderController *toc = [[TaobaoOrderController alloc] init];
        toc.hidesBottomBarWhenPushed = YES;
        toc.user = user;
        [self.navigationController pushViewController:toc animated:YES];
    }];
}

/** 商城订单 */
- (void)MarketOrder{
    [LoginController isLoginStatus:^(User *user) {
        MarketOrderController *moc = [[MarketOrderController alloc] init];
        moc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:moc animated:YES];
    }];
}

/** 我的收藏 */
- (void)MyCollection{
    [LoginController isLoginStatus:^(User *user) {
        MyFollowController *mfc = [[MyFollowController alloc] init];
        mfc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:mfc animated:YES];
    }];
}

/** 兑换集分宝 */
- (void)ExchangeCoins{
    [LoginController isLoginStatus:^(User *user) {
        [UserPayExt getUserPayExt:^(UserPayExt *payExt) {
            if (payExt.cashPhone.length && payExt.accountType == NoAssignment) {//只绑定了手机号
                BingAlipayController *bindAlipay = [[BingAlipayController alloc] init];
                bindAlipay.payExt = payExt;
                bindAlipay.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:bindAlipay animated:YES];
            }else if (payExt.accountType == NoAssignment) {//手机号支付宝都没绑定
                BindPhoneController *bindVC = [[BindPhoneController alloc] init];
                bindVC.payExt = payExt;
                bindVC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:bindVC animated:YES];
            }else{//已经成功绑定了支付宝和手机号。目前只有支付宝，没有财付通
                ExchangeCoinsController *ec = [[ExchangeCoinsController alloc] init];
                ec.payExt = payExt;
                ec.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:ec animated:YES];
            }
        }];
    }];
}

/** 兑换记录 */
- (void)ExchangeRecord{
    [LoginController isLoginStatus:^(User *user) {
        ExchangeRecordController *erc = [[ExchangeRecordController alloc] init];
        erc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:erc animated:YES];
    }];
}

/** 邀请好友 */
- (void)InvitingFriends{
    [LoginController isLoginStatus:^(User *user) {
        InviteUserListModel *invite = [InviteUserListModel share];
        NSString *inviteUserListUrl = [invite.urlValue stringByReplacingOccurrencesOfString:@"{uid}" withString:UD_UID];
        inviteUserListUrl = [inviteUserListUrl stringByReplacingOccurrencesOfString:@"{token}" withString:UD_TOKEN];
        YUWebController *inviteWeb = [[YUWebController alloc] init];
        inviteWeb.url = [NSURL URLWithString:inviteUserListUrl];
        inviteWeb.title_string = @"邀请好友";
        inviteWeb.hidesBottomBarWhenPushed = YES;
        UIBarButtonItem *inviteItem = [[UIBarButtonItem alloc] initWithTitle:@"邀请" style:UIBarButtonItemStyleDone target:self action:@selector(inviteUser)];
        inviteItem.tintColor = [UIColor blackColor];
        [inviteItem setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
        inviteWeb.rightBarButtonItems = @[inviteItem];
        [self.navigationController pushViewController:inviteWeb animated:YES];
    }];
}

/** 邀请好友分享朋友圈 */
- (void)inviteUser{
    [SelectedShareView shareWithURL:[DownAppShare share].urlValue imageUrl:[DownAppShare share].urlIcon title:@"你的好友喊你来领红包" description:@"使用返利大王就能获得60集分宝,赶紧来领取。" array:@[@"QQ",@"QQ空间",@"微信",@"朋友圈"] success:nil];
}


/** 消息中心 */
- (void)MessageCenter{
    [LoginController isLoginStatus:^(User *user) {
        MessageCenterController *mcc = [[MessageCenterController alloc] init];
        mcc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:mcc animated:YES];
    }];
}

/** 赚金币 */
- (void)EarnCoins{
    [LoginController isLoginStatus:^(User *user) {
        EarnCoinsController *ecc = [[EarnCoinsController alloc] init];
        ecc.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ecc animated:YES];
    }];
}

/** 跳转webVC */
- (void)pushWebURL:(NSString *)url title:(NSString *)title{
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:url];
    web.title_string = title;
    web.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:web animated:YES];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat y = scrollView.contentOffset.y;
    if (y > 0) {
        [self.bg_image mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@(-y));
            make.height.mas_equalTo(kMainWidth * 0.3734);
        }];
    }else{
        [self.bg_image mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@0);
            make.height.mas_equalTo(kMainWidth * 0.3734 - y);
        }];
    }
}

/** 创建数组 */
- (void)creatArray{
    /*@{@"icon":@"",@"title":@"",@"type":@(0)}*/
    NSArray *tmpArray =@[
  @[@{@"icon":@"淘宝订单",@"title":@"淘宝订单",@"type":@(0)},
  @{@"icon":@"商城订单",@"title":@"商城订单",@"type":@(1)},
  @{@"icon":@"我的收藏",@"title":@"我的收藏",@"type":@(2)}],
    
  @[@{@"icon":@"兑换集分宝",@"title":@"兑换集分宝",@"type":@(3)},
  @{@"icon":@"兑换记录",@"title":@"兑换记录",@"type":@(4)},
  @{@"icon":@"邀请好友",@"title":@"邀请好友",@"type":@(5)},
  @{@"icon":@"消息中心",@"title":@"消息中心",@"type":@(6)},
  @{@"icon":@"联系客服",@"title":@"联系客服",@"type":@(7)},
  @{@"icon":@"赚金币",@"title":@"赚金币",@"type":@(8)},
  @{@"icon":@"帮助中心",@"title":@"帮助中心",@"type":@(9)}]];
    self.array = [NSMutableArray array];
    for (NSArray *tmpSubAry in tmpArray) {
        NSMutableArray *subArray = [NSMutableArray array];
        for (NSDictionary *dic in tmpSubAry) {
            MyItemModel *model = [[MyItemModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [subArray addObject:model];
        }
        [self.array addObject:subArray];
    }
}

@end
