//
//  MyFollowController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MyFollowController.h"
#import "DiscountCell.h"
#import "YURefreshHeader.h"
#import "TaobaoDetail.h"
#import "ExchangeSuccessController.h"

@interface MyFollowController ()

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation MyFollowController

static NSString * const reuseIdentifier = @"DiscountCell";

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (void)loadView{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    CGFloat width = (kMainWidth - 15) / 2;
    layout.itemSize = CGSizeMake(width, width + 88.5);
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectionView.backgroundColor = UIColorFromHex(0xF0F0F0);
    self.collectionView = collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    /** 设置标题 */
    self.navigationItem.title = @"我的收藏";
    
    UIBarButtonItem *clean = [[UIBarButtonItem alloc] initWithTitle:@"清空" style:UIBarButtonItemStylePlain target:self action:@selector(clean)];
    clean.tintColor = UIColorFromHex(0x666666);
    [clean setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(14)} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = clean;
    [self.collectionView registerClass:[DiscountCell class] forCellWithReuseIdentifier:reuseIdentifier];
    WS(weakSelf);
    self.collectionView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.array removeAllObjects];
        [weakSelf getCateItemData];
    }];
    [self.collectionView.mj_header beginRefreshing];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DiscountCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    cell.model = self.array[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    CateItemModel *model = self.array[indexPath.item];
    [TaobaoDetail showTaobaoDetail:self.navigationController itemID:model.tbItemId pid:model.taobaoPid success:^(AlibcTradeResult *result) {
        ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
        esc.navigation_title = @"支付成功";
        esc.success_title = @"购买商品支付成功";
        esc.btn_name = @"查看订单";
        esc.hidesBottomBarWhenPushed = YES;
        esc.pushController = @"TaobaoOrderController";
        [self.navigationController pushViewController:esc animated:YES];
    } failure:nil];
}

/** 获取网络数据 */
- (void)getCateItemData{
    NSInteger curPage = self.array.count / 20 + 1;
    [[YURequest creatMarketRequest] POST:@"openApi/item/myFavorites.json" parameters:@{@"curPage":@(curPage)} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            CateItemModel *model = [[CateItemModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            model.fav = YES;
            [self.array addObject:model];
        }
        if ([JSON count] % 20 || [JSON count] == 0) {
            self.collectionView.mj_footer = nil;
        }else{
            self.collectionView.mj_footer = [MJRefreshAutoGifFooter footerWithRefreshingTarget:self refreshingAction:@selector(getCateItemData)];
        }
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.collectionView.mj_header endRefreshing];
        [self.collectionView.mj_footer endRefreshing];
    }];
}

/** 清空所有已收藏 */
- (void)clean{
    [SVProgressHUD show];
    [[YURequest creatMarketRequest] POST:@"openApi/item/delAllFavorites.json" parameters:nil success:^(id JSON) {
        [SVProgressHUD showSuccessWithStatus:@"收藏清空成功!"];
        [self.array removeAllObjects];
        [self.collectionView reloadData];
    } failure:nil];
}

@end
