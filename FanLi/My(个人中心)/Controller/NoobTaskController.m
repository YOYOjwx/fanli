//
//  NoobTaskController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "NoobTaskController.h"
#import "NoobTaskCell.h"

@interface NoobTaskController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) UIButton *task_btn;

@end

@implementation NoobTaskController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self getArray];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.leading.bottom.equalTo(@0);
    }];
    /** 设置背景颜色 */
    self.tableView.backgroundColor = UIColorFromHex(0xE6E6E6);
    self.tableView.separatorStyle = NO;
    /** 注册cell */
    [self.tableView registerClass:[NoobTaskCell class] forCellReuseIdentifier:@"NoobTaskCell"];
    self.task_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.task_btn];
    [self.task_btn setTitle:@"领取任务，完成得5金币" forState:UIControlStateNormal];
    [self.task_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.task_btn.backgroundColor = UIColorFromHex(0xFF5E56);
    self.task_btn.layer.masksToBounds = YES;
    self.task_btn.layer.cornerRadius = 4;
    self.task_btn.enabled = NO;
    [self.task_btn addTarget:self action:@selector(receiveTask) forControlEvents:UIControlEventTouchUpInside];
    [self.task_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(@-20);
        make.leading.equalTo(@15);
        make.trailing.equalTo(@-15);
        make.height.equalTo(@40);
    }];
    [self taskStatus];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NoobTaskCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NoobTaskCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 80;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 80)];
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

/** 领取任务 */
- (void)receiveTask{
    [SVProgressHUD showWithStatus:@"任务领取中"];
    [[YURequest creatMarketRequest] POST:@"openApi/task/get.json" parameters:@{@"tType":@"1"} success:^(id JSON) {
        self.task_btn.enabled = NO;
        [SVProgressHUD showSuccessWithStatus:@"任务领取成功"];
        [self.task_btn setTitle:@"领取成功，打开手机淘宝" forState:UIControlStateNormal];
    } failure:nil];
}

- (void)taskStatus{
    [SVProgressHUD show];
    [[YURequest creatMarketRequest] POST:@"openApi/task/status.json" parameters:@{@"tType":@"1"} success:^(id JSON) {
        NSNumber *status = JSON[@"tStatus"];
        switch (status.intValue) {
            case 0:{
                self.task_btn.enabled = YES;
                [self.task_btn setTitle:@"领取任务，完成得5金币" forState:UIControlStateNormal];
                break;
            }
            case 1:{
                [self.task_btn setTitle:@"领取成功，未完成" forState:UIControlStateNormal];
                self.task_btn.backgroundColor = UIColorFromHex(0xCCCCCC);
                break;
            }
            case 2:{
                [self.task_btn setTitle:@"任务已完成" forState:UIControlStateNormal];
                self.task_btn.backgroundColor = UIColorFromHex(0xCCCCCC);
                break;
            }
            default:break;
        }
    } failure:nil];
}

- (void)getArray{
    NSArray *temArray = @[
  @{@"icon":@"pic_1",@"title":@"淘一下",@"detail":@"打开手机淘宝或者天猫，找到心仪的宝贝"},
  @{@"icon":@"pic_2",@"title":@"复制标题",@"detail":@"在商品详情长按宝贝标题选择【复制宝贝标题】"},
  @{@"icon":@"pic_3",@"title":@"粘贴标题",@"detail":@"返利大王首页，单机搜索康自动粘贴"}];
    for (NSDictionary *dic in temArray) {
        EarnListModel *model = [[EarnListModel alloc] init];
        [model setValuesForKeysWithDictionary:dic];
        [self.array addObject:model];
    }
}


@end
