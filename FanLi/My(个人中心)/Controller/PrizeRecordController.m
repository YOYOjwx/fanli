//
//  PrizeRecordController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "PrizeRecordController.h"
#import "PrizeRecordCell.h"
#import "YURefreshHeader.h"

@interface PrizeRecordController ()

@property (nonatomic, strong) NSMutableArray *array;
/** 没有更多了 */
@property (nonatomic, strong) UIView *footer_view;
/** 背景图片 */
@property (nonatomic, strong) UIImageView *bg_image;

@end

@implementation PrizeRecordController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (UIView *)footer_view{
    if (!_footer_view) {
        _footer_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 40)];
        UILabel *no_more_lb = [UILabel new];
        [_footer_view addSubview:no_more_lb];
        no_more_lb.text = @"没有更多了!";
        no_more_lb.font = CurrencyFont(14);
        [no_more_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
    }
    return _footer_view;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置背景颜色 */
    self.tableView.backgroundColor = [UIColor clearColor];
    /** 取消横线 */
    self.tableView.separatorStyle = NO;
    /** 行高 */
    self.tableView.rowHeight = 140;
    /** 注册cell */
    [self.tableView registerClass:[PrizeRecordCell class] forCellReuseIdentifier:@"PrizeRecordCell"];
    /** 创建背景视图 */
    self.bg_image = [UIImageView new];
    [self.view addSubview:self.bg_image];
    self.bg_image.image = [UIImage imageNamed:@"任何地方空白页面"];
    [self.bg_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
    }];
    WS(weakSelf);
    /** 下啦加载刷新 */
    self.tableView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.array removeAllObjects];
        [weakSelf getRecordData];
    }];
    [self.tableView.mj_header beginRefreshing];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.array.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PrizeRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PrizeRecordCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (void)getRecordData{
    NSInteger curpage = self.array.count / 20 + 1;
    [RecordModel getRecordWithCurPage:curpage payType:payTypeGift success:^(NSMutableArray<RecordModel *> *array) {
        if (array.count % 20 != 0 || array.count == 0) {//代表没有更多数据了
            self.tableView.mj_footer = nil;
            self.tableView.tableFooterView = self.footer_view;
        }else{
            self.tableView.tableFooterView = nil;
            self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getRecordData)];
        }
        [self.array addObjectsFromArray:array];
        if (self.array.count != 0) {
            self.bg_image.hidden = YES;
        }else{
            self.bg_image.hidden = NO;
            self.tableView.tableFooterView = nil;
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}


@end
