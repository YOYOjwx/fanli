//
//  SetController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/18.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "SetController.h"
#import "IconCell.h"
#import "ConfirmCell.h"
#import "WordsCell.h"
#import "YUAlertController.h"
#import "YUWebController.h"
#import "SDImageCache.h"
#import "TaobaoQuickLogin.h"
#import "CameraPhoto.h"

@interface SetController ()<UITableViewDelegate,UITableViewDataSource>

/** 存放数据的数组 多维数组 */
@property (nonatomic, strong) NSMutableArray *array;
/** TableView */
@property (nonatomic, strong) UITableView *tableView;

@end

@implementation SetController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.title = @"设置";
    /** 创建数组内容 */
    [self creatArray];
    /** 初始化视图 */
    [self initSubViews];
}

/** 初始化视图 */
- (void)initSubViews{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    [self.view addSubview:self.tableView];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = UIColorFromHex(0xF4F4F4);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLineEtched;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.trailing.bottom.leading.equalTo(@0);
    }];
    /** 注册头像Cell */
    [self.tableView registerClass:[IconCell class] forCellReuseIdentifier:@"icon"];
    /** 注册设置文字cell */
    [self.tableView registerClass:[WordsCell class] forCellReuseIdentifier:@"words"];
    /** 注册开始使用cell */
    [self.tableView registerClass:[ConfirmCell class] forCellReuseIdentifier:@"confirm"];
}
/** 返回分区个数 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.array.count;
}
/** 返回个数 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.array[section] count];
}
/** 返回cell */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SetCellModel *model = self.array[indexPath.section][indexPath.row];
    if (model.setType == SetTypeIcon) {//创建头像cell
        IconCell *cell = [tableView dequeueReusableCellWithIdentifier:@"icon" forIndexPath:indexPath];
        cell.model = model;
        return cell;
    }
    if (model.setType == SetTypeExit) {
        ConfirmCell *cell = [tableView dequeueReusableCellWithIdentifier:@"confirm" forIndexPath:indexPath];
        cell.model = model;
        return cell;
    }
    WordsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"words" forIndexPath:indexPath];
    cell.model = model;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    SetCellModel *model = self.array[indexPath.section][indexPath.row];
    switch (model.value) {
        case 0:[self setHeadPortrait:model indexPath:indexPath];break;
        case 1:[self setNickname:model indexPath:indexPath];break;
        case 4:[self pushDisclaimer];break;
        case 5:[self clearCache];break;
        case 6:[self logout];break;
        default:break;
    }
}

/** 设置头像 */
- (void)setHeadPortrait:(SetCellModel *)model indexPath:(NSIndexPath *)indexPath{
    YUAlertController *alertVC = [YUAlertController alertControllerWithTitle:model.setTitle message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    [alertVC addSubActions:model.actionSheetArr];
    [alertVC selectionResults:^(NSString *content, NSInteger index) {
        UIImagePickerControllerSourceType type;
        if (index == 0) {
            type = UIImagePickerControllerSourceTypeCamera;
        }else{
            type = UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        }
        CameraPhoto *camera = [[CameraPhoto alloc] initWithCameraForSourceType:type success:^(UIImage *image) {
            NSData *data = UIImageJPEGRepresentation(image, 0.5);
            [SVProgressHUD showWithStatus:@"上传中"];
            [[YUUpload creatBaseRequest] upload:@"user/avatar/set.json" parameters:@{@"uid":UD_UID,@"token":UD_TOKEN} data:data fileKey:@"newFile" fileName:@"portrait.png" success:^(id JSON) {
                User *user = [User shareUser];
                user.profileImage = JSON;
                [self ChangeArrayContent:JSON indexPath:indexPath];
            } progress:nil failure:nil];
            
        }];
        [self presentViewController:camera animated:YES completion:nil];
    }];
    [self presentViewController:alertVC animated:YES completion:nil];
}

/** 设置昵称 */
- (void)setNickname:(SetCellModel *)model indexPath:(NSIndexPath *)indexPath{
    YUAlertController *alertVC = [YUAlertController alertControllerWithTitle:model.setTitle message:nil preferredStyle:UIAlertControllerStyleAlert];
    alertVC.content = model.detail;
    [alertVC selectionResults:^(NSString *content, NSInteger index) {
        /** 更改数组内容 */
        User *user = [User shareUser];
        user.nickname = content;
        [self ChangeArrayContent:content indexPath:indexPath];
        [[YURequest creatBaseRequest] POST:@"user/profile/set.json" parameters:@{@"nickname":content} success:^(id JSON) {
            
        } failure:nil];
    }];
    [self presentViewController:alertVC animated:YES completion:nil];
}

/** 跳转免责声明 */
- (void)pushDisclaimer{
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:[BasePHPURL stringByAppendingString:@"index.php?m=Mobile&a=treaty"]];
    web.title_string = @"免责声明";
    [self.navigationController pushViewController:web animated:YES];
}

/** 清除缓存 */
- (void)clearCache{
    [SVProgressHUD showInfoWithStatus:@"清除中..."];
    [[SDImageCache sharedImageCache] clearDiskOnCompletion:^{
        [SVProgressHUD showSuccessWithStatus:@"缓存清除成功"];
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0) , ^{
        NSString *cachPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSArray *files = [[NSFileManager defaultManager] subpathsAtPath:cachPath];
        for (NSString *file in files) {
            NSString *path = [cachPath stringByAppendingPathComponent:file];
            if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
                [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
            }
        }
    });
}

/** 退出登录 */
- (void)logout{
    [[User shareUser] logout];
    /** 淘宝需要退出登录，因为不退出登录，无法更换账号进行淘宝登录，淘宝做了登录保存 */
    [TaobaoQuickLogin logout];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_LOGIN"];
    [self.navigationController popViewControllerAnimated:YES];
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 20;
}

#pragma mark 设置cell自适应高度
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88;
}

/** 更改数组内容 */
- (void)ChangeArrayContent:(NSString *)content indexPath:(NSIndexPath *)indexPath{
    SetCellModel *model = self.array[indexPath.section][indexPath.row];
    model.detail = content;
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

/** 创建数组内容 */
- (void)creatArray{
/*@{@"title":@"",@"detail":@"",@"imageUrl":@"",@"setType":@0,@"setTitle":@"",@"ActionSheetArr":@[],@"pickerType":@0,@"isNumber":@YES};*/
    /** 设置头像:0 */
    /** 设置文字:1 */
    /** 设置Pick:2 */
    /** 底部弹窗:3 */
    /** 跳转控制器:4 */
    /** 什么都不做:5 */
    /** 退出:6 */
    User *user = [User shareUser];
    NSArray *tmpArray = @[@[
  @{@"title":@"头像",@"setType":@0,@"detail":user.profileImage.length?user.profileImage:@"",@"setTitle":@"更换头像",@"ActionSheetArr":@[@"拍照",@"相册"],@"value":@0},
  @{@"title":@"昵称",@"detail":user.nickname.length?user.nickname:@"",@"setType":@1,@"setTitle":@"请输入你的昵称",@"value":@1}],
    
  @[@{@"title":@"UID",@"detail":UD_UID,@"setType":@5,@"value":@2},
  @{@"title":@"客服QQ:",@"detail":@"2086817121",@"setType":@5,@"value":@3}],
    
  @[@{@"title":@"免责声明",@"setType":@4,@"value":@4}],
        
  @[@{@"title":@"清除缓存",@"setType":@2,@"value":@5},
  @{@"title":@"退出登录",@"setType":@6,@"value":@6}]];
    self.array = [NSMutableArray array];
    for (NSArray *tmpSubAry in tmpArray) {
        NSMutableArray *subArray = [NSMutableArray array];
        for (NSDictionary *dic in tmpSubAry) {
            SetCellModel *model = [[SetCellModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [subArray addObject:model];
        }
        [self.array addObject:subArray];
    }
}

@end
