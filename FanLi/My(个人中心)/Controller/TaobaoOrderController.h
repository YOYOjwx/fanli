//
//  TaobaoOrderController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/18.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"

@interface TaobaoOrderController : BaseViewController

@property (nonatomic, strong) User *user;

@end
