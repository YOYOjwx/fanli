//
//  TaobaoOrderController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/18.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "TaobaoOrderController.h"
#import "YUSegmented.h"
#import "OrderCell.h"
#import "TaobaoDetail.h"
#import "ShareModel.h"
#import "SelectedShareView.h"
#import "ExchangeSuccessController.h"
#import <UMMobClick/MobClick.h>

@interface TaobaoOrderController ()<UITableViewDelegate,UITableViewDataSource>

/** tableView */
@property (nonatomic, strong) UITableView *tableView;
/** segmented */
@property (nonatomic, strong) YUSegmented *segmented;
/** 背景图片 */
@property (nonatomic, strong) UIImageView *bg_image;
/** 存放数据的数组 */
@property (nonatomic, strong) NSMutableDictionary *dic;
/** 当前选中某一个模块 0:待返利 1:已返利 */
@property (nonatomic, strong) NSString *type;
/** 没有更多了 */
@property (nonatomic, strong) UIView *footer_view;

@end

@implementation TaobaoOrderController

- (UIView *)footer_view{
    if (!_footer_view) {
        _footer_view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kMainWidth, 40)];
        UILabel *no_more_lb = [UILabel new];
        [_footer_view addSubview:no_more_lb];
        no_more_lb.text = @"没有更多了!";
        no_more_lb.font = CurrencyFont(14);
        [no_more_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
    }
    return _footer_view;
}

- (NSMutableDictionary *)dic{
    if (!_dic) {
        _dic = [NSMutableDictionary dictionary];
    }
    return _dic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"淘宝订单";
    /** 设置背景颜色 */
    self.view.backgroundColor = UIColorFromHex(0xF0F0F0);
    /** 创建子视图 */
    [self creatSubViews];
    [MobClick event:@"changeOrderListPage"];
}

/** 创建子视图 */
- (void)creatSubViews{
    self.type = @"0";
    /** 创建segmened */
    WS(weakSelf);
    self.segmented = [[YUSegmented alloc] initWithArray:@[@"待返利",@"已返利"] column:2 click:^(NSInteger index) {
        weakSelf.type = [NSString stringWithFormat:@"%ld",(long)index];
        if (self.dic[self.type]) {
            [self.tableView reloadData];
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }else{
            [weakSelf getRebateData];
        }
        
    }];
    [self.view addSubview:self.segmented];
    [self.segmented mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@0.5);
        make.leading.trailing.equalTo(@0);
        make.height.equalTo(@35);
    }];
    /** 创建tableView */
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.separatorStyle = NO;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.leading.bottom.equalTo(@0);
        make.top.equalTo(weakSelf.segmented.mas_bottom);
    }];
    /** 注册cell */
    [self.tableView registerClass:[OrderCell class] forCellReuseIdentifier:@"taobaoOrder"];
    /** 创建背景视图 */
    self.bg_image = [UIImageView new];
    [self.view addSubview:self.bg_image];
    self.bg_image.image = [UIImage imageNamed:@"任何地方空白页面"];
    [self.bg_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
    }];
    [self getRebateData];
    /** 添加下拉刷新 */
    self.tableView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.dic removeObjectForKey:weakSelf.type];
        [weakSelf getRebateData];
    }];
    /** 添加上啦加载更多 */
    self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getRebateData)];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.dic[_type] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"taobaoOrder" forIndexPath:indexPath];
    cell.model = self.dic[self.type][indexPath.row];
    cell.share = ^(OrderModel *model) {
        NSString *shareUrl = [[ShareModel share].urlValue stringByReplacingOccurrencesOfString:@"{seqId}" withString:model.seqId.stringValue];
        NSString *description = [NSString stringWithFormat:@"【好物分享】我用%@购买了一件宝贝，小伙伴赶紧来看看吧~~",APPNAME];
        [SelectedShareView shareWithURL:shareUrl imageUrl:[ShareModel share].urlIcon title:@"推荐你下载返利大王" description:description array:@[@"QQ空间",@"朋友圈"] success:^{
            [self shareSuccessAddCoins:model.seqId.stringValue];
        }];
    };
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    OrderModel *model = self.dic[self.type][indexPath.row];
    if (model.tbItemId == nil) return;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [TaobaoDetail showTaobaoDetail:self.navigationController itemID:model.tbItemId pid:model.taobaoPid success:^(AlibcTradeResult *result) {
        ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
        esc.navigation_title = @"支付成功";
        esc.success_title = @"购买商品支付成功";
        esc.btn_name = @"查看订单";
        esc.hidesBottomBarWhenPushed = YES;
        esc.pushController = @"TaobaoOrderController";
        [self.navigationController pushViewController:esc animated:YES];
    } failure:nil];
}

#pragma mark 设置cell自适应高度
- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 40;
}

/** 获取网络数据 */
- (void)getRebateData{
    NSMutableArray *subArray = self.dic[self.type];
    NSInteger curPage = subArray.count / 20 + 1;
    if ([self.type isEqualToString:@"1"]) {
        [[YURequest creatBaseRequest] POST:@"taeorder/list.json" parameters:@{@"sendStatus":@"1",@"curPage":@(curPage)} success:^(id JSON) {
            [self dataAnalysis:JSON];
        } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }];
    }else{
        [[YURequest creatMarketRequest] POST:@"openApi/order/my" parameters:@{@"curPage":@(curPage)} success:^(id JSON) {
            [self dataAnalysis:JSON];
        } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
            [self.tableView.mj_header endRefreshing];
            [self.tableView.mj_footer endRefreshing];
        }];
    }
}

/** 数据解析 */
- (void)dataAnalysis:(NSArray *)array{
    NSMutableArray *subArray = self.dic[self.type];
    if (subArray == nil) {
        subArray = [NSMutableArray array];
    }
    if (array.count % 20 != 0 || array.count == 0) {//代表没有更多数据了
        self.tableView.mj_footer = nil;
        self.tableView.tableFooterView = self.footer_view;
    }else{
        self.tableView.tableFooterView = nil;
        self.tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(getRebateData)];
    }
    for (NSDictionary *dic in array) {
        OrderModel *model = [[OrderModel alloc] init];
        [model setValuesForKeysWithDictionary:dic];
        [subArray addObject:model];
    }
    if (subArray.count != 0) {
        self.bg_image.hidden = YES;
    }else{
        self.bg_image.hidden = NO;
        self.tableView.tableFooterView = nil;
    }
    [self.dic setValue:subArray forKey:self.type];
    [self.tableView reloadData];
    [self.tableView.mj_header endRefreshing];
    [self.tableView.mj_footer endRefreshing];
}

- (void)shareSuccessAddCoins:(NSString *)seqId{
    [[YURequest creatBaseRequest] POST:@"coin/daily/shareOrder.json" parameters:@{@"seqId":seqId} success:^(id JSON) {
        NSNumber *coins = JSON[@"shareOrderLog"][@"coins"];
        [User shareUser].ycoins += coins.intValue;
        [SVProgressHUD showSuccessWithStatus:[NSString stringWithFormat:@"分享购买订单得%@金币",coins]];
    } failure:nil];
}

@end
