//
//  EarnListModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface EarnListModel : BaseModel

/** 图片地址 */
@property (nonatomic, strong) NSString *icon;
/** 标题 */
@property (nonatomic, strong) NSString *title;
/** 副标题 */
@property (nonatomic, strong) NSString *detail;

@end
