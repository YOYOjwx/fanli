//
//  GiftModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface GiftModel : BaseModel

/** 礼品描述 */
@property (nonatomic, strong) NSString *giftDesc;
/** 礼品ID(服务端生成) */
@property (nonatomic, assign) int giftId;
/** 所需金币数 */
@property (nonatomic, assign) int price;
/** 库存量（暂时用不到） */
@property (nonatomic, assign) int quantity;
/** 图片路径 */
@property (nonatomic, strong) NSString *imgUrl;
/** 淘宝信息 */
@property (nonatomic, strong) NSString *taobaoUrl;
/** 淘宝商品混淆ID */
@property (nonatomic, strong) NSString *taobaoItemId;
/** 返利淘商城所需的柚币价格 */
@property (nonatomic, assign) int ycoinPrice;
/** 排序号 */
@property (nonatomic, assign) int sortNo;
/** 备注 */
@property (nonatomic, strong) NSString *memo;
/** 礼物分类编号 */
@property (nonatomic, assign) int gcId;

@end
