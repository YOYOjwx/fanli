//
//  MallOrderModel.h
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface MallOrderModel : BaseModel

/** 订单金额 */
@property (nonatomic, assign) float orderPrice;
/** 返利金币 */
@property (nonatomic, strong) NSNumber *userCoins;
/** 合作方订单号 */
@property (nonatomic, strong) NSString *partnerOrderId;
/** 商城名字 */
@property (nonatomic, strong) NSString *mallName;
/** 商城图片 */
@property (nonatomic, strong) NSString *pic;
/** 订单状态 YES:已经返利 */
@property (nonatomic, assign) BOOL status;

+ (void)getOrderWithCurPage:(NSInteger)curPage orderStatus:(NSString *)orderStatus success:(void (^)(NSMutableArray<MallOrderModel *> *array))success failure:(void (^)(NSError *error, NSString *errorMsg, NSInteger code))failure;

@end
