//
//  MallOrderModel.m
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MallOrderModel.h"

@implementation MallOrderModel

+ (void)getOrderWithCurPage:(NSInteger)curPage orderStatus:(NSString *)orderStatus success:(void (^)(NSMutableArray<MallOrderModel *> *))success failure:(void (^)(NSError *, NSString *, NSInteger))failure{
    NSMutableArray *array = [NSMutableArray array];
    [[YURequest creatMarketRequest] POST:@"openApi/mall/getOrders" parameters:@{@"curPage":@(curPage),@"orderStatus":orderStatus} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            MallOrderModel *model = [[MallOrderModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            if ([orderStatus isEqualToString:@"1"]) {
                model.status = YES;
            }
            [array addObject:model];
        }
        if (success) {
            success(array);
        }
    } failure:failure];
}

@end
