//
//  MessageModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface MessageModel : BaseModel

/** 消息内容 */
@property (nonatomic, strong) NSString *contentObject;
/** id */
@property (nonatomic, strong) NSString *messageId;
/** 消息类型 */
@property (nonatomic, assign) int messageType;
/** 消息ID根上面的那个类似，但是作用不一样 */
@property (nonatomic, assign) int seqId;
/** 时间戳 */
@property (nonatomic, assign) double timestamp;
/** uid */
@property (nonatomic, assign) int uid;

@end
