//
//  MyItemModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface MyItemModel : BaseModel

/** icon名字 */
@property (nonatomic, strong) NSString *icon;
/** 标题 */
@property (nonatomic, strong) NSString *title;
/** 数据类型 */
@property (nonatomic, assign) int type;

@end
