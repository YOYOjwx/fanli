//
//  OrderModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface OrderModel : BaseModel

/** 分享用的外链ID */
@property (nonatomic, strong) NSNumber *seqId;
/** 订单创建,订单付款,订单成功,订单关闭,订单退款 */
@property (nonatomic, strong) NSString *orderStatus;
/** 金币数 */
@property (nonatomic, assign) int itemCoins;
/** 商品图片 */
@property (nonatomic, strong) NSString *picUrl;
/** 商品数量,当为任务奖励订单时，该值为空 */
@property (nonatomic, assign) int itemAmount;
/** 商品标题 */
@property (nonatomic, strong) NSString *tbItemTitle;
/** 商品促销价 */
@property (nonatomic, assign) float itemUpmPrice;
/** 商品原价 */
@property (nonatomic, assign) float itemPrice;
/** 预计返利百分比,1-99的数字,为空则为订单没有确认收货 */
@property (nonatomic, assign) int ycoinsReturnPer;
/** 预计多少天后返利,为空则为订单没有确认收货 */
@property (nonatomic, assign) int ycoinsReturnDays;
/** 2 已确认收货，0 未确认收货 */
@property (nonatomic, assign) int orderStatusCode;
/** 淘宝混淆ID */
@property (nonatomic, strong) NSString *tbItemId;
/** 1:淘宝，2:商城 */
@property (nonatomic, assign) int itemType;
/** 是否为淘客 */
@property (nonatomic, assign) int isTk;
/** 淘宝pid */
@property (nonatomic, strong) NSString *taobaoPid;
/** 0:审核中,1:已返利 */
@property (nonatomic, assign) int sendStatus;

@end
