//
//  RecordModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM (int, PayType) {
    /** 提现 */
    payTypeCash = -1,
    /** 兑换金币 */
    payTypeCoins = -2,
    /** 兑换礼品 */
    payTypeGift = -3//
};

typedef NS_ENUM (int, PayStatus) {
    /** 审核中 */
    PayStatusExamine = 0,
    /** 兑换失败 */
    PayStatusFailure = -1,
    /** 兑换成功 */
    PayStatusSuccess = 1
};

@interface RecordModel : BaseModel

/** 创建时间 */
@property (nonatomic, strong) NSString *createTime;
/** 支付金币数目 */
@property (nonatomic, strong) NSNumber *payAmount;
/** 订单ID */
@property (nonatomic, strong) NSNumber *payId;
/** 礼物说明 */
@property (nonatomic, strong) NSString *giftDesc;
/** 礼物图片 */
@property (nonatomic, strong) NSString *imgUrl;
/** 订单状态 */
@property (nonatomic, assign) PayStatus payStatus;
/** 支付类型 */
@property (nonatomic, assign) PayType payType;

+ (void)getRecordWithCurPage:(NSInteger)curPage payType:(PayType)payType success:(void(^)(NSMutableArray<RecordModel *>  *array))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;

@end
