//
//  RecordModel.m
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "RecordModel.h"

@implementation RecordModel

+ (void)getRecordWithCurPage:(NSInteger)curPage payType:(PayType)payType success:(void (^)(NSMutableArray<RecordModel *> *))success failure:(void (^)(NSError *, NSString *, NSInteger))failure{
    NSMutableArray *array = [NSMutableArray array];
    [[YURequest creatBaseRequest] POST:@"ycoin/applyList1.json" parameters:@{@"curPage":@(curPage),@"payType":@(payType)} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            RecordModel *model = [[RecordModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [array addObject:model];
        }
        if (success) {
            success(array);
        }
    } failure:failure];
}


@end
