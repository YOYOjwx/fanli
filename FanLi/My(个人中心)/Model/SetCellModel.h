//
//  SetCellModel.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM (int, SetType) {
    /** 设置头像 */
    SetTypeIcon = 0,
    /** 设置文字 */
    SetTypeWords = 1,
    /** 设置Pick */
    SetTypePicker = 2,
    /** 底部弹窗 */
    SetTypeActionSheet = 3,
    /** 跳转控制器 */
    SetTypePush = 4,
    /** 什么都不做 */
    SetTypeDone = 5,
    /** 退出 */
    SetTypeExit = 6
};
typedef NS_ENUM (int, PickerType) {
    /** 城市 */
    PickerTypeCity = 0,
    /** 日前 */
    PickerTypeDate = 1,
};

@interface SetCellModel : BaseModel

/** 标题 */
@property (nonatomic, strong) NSString *title;
/** 副标题 */
@property (nonatomic, strong) NSString *detail;
/** 设置类型 */
@property (nonatomic, assign) SetType setType;
/** 设置标题  */
@property (nonatomic, strong) NSString *setTitle;
/** ActionSheet数组 如果类型不是ActionSheet则为空 */
@property (nonatomic, strong) NSArray *actionSheetArr;
/** Picker的类型 */
@property (nonatomic, assign) PickerType pickerType;

@property (nonatomic, assign) int value;

@end
