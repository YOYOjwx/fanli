//
//  UserPayExt.h
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM (int, AccountType) {
    /** 未设置 */
    NoAssignment,
    /** 支付宝 */
    Alipay,
    /** 财付通 */
    Tenpay
};

@interface UserPayExt : BaseModel

/** 支付宝绑定的手机号 */
@property (nonatomic, strong) NSString *cashPhone;
/** 账号类型 */
@property (nonatomic, assign) AccountType accountType;
/** 账号 */
@property (nonatomic, strong) NSString *account;
/** 账号对应的姓名 */
@property (nonatomic, strong) NSString *accountName;
/** 获取柚币支付绑定信息 */
+ (void)getUserPayExt:(void(^)(UserPayExt *payExt))success;

@end
