//
//  UserPayExt.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "UserPayExt.h"

@implementation UserPayExt

+ (void)getUserPayExt:(void (^)(UserPayExt *))success{
    [[YURequest creatBaseRequest] POST:@"user/pay/get.json" parameters:nil success:^(id JSON) {
        UserPayExt *payExt = [[UserPayExt alloc] init];
        [payExt setValuesForKeysWithDictionary:JSON];
        if (success) {
            success(payExt);
        }
    } failure:nil];
}

@end
