//
//  BaseSetCell.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCellModel.h"

@interface BaseSetCell : UITableViewCell

/** 标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 箭头 */
@property (nonatomic, strong) UIImageView *arrow_image;
/** 设置model */
@property (nonatomic, strong) SetCellModel *model;

@end
