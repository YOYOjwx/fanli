//
//  BaseSetCell.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "BaseSetCell.h"

@interface BaseSetCell ()

@end

@implementation BaseSetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        /** 设置cell高度自适应 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.leading.trailing.equalTo(@0);
        }];
        /** 创建标题Label */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x555555);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@12);
            make.leading.equalTo(@10);
            make.trailing.equalTo(weakSelf.mas_centerX);
        }];
        /** 设置箭头 */
        self.arrow_image = [UIImageView new];
        [self.contentView addSubview:self.arrow_image];
        self.arrow_image.image = [UIImage imageNamed:@"cell的箭头"];
        [self.arrow_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.title_lb);
            make.trailing.equalTo(@-10);
            make.width.height.equalTo(@15);
        }];
        /** 设置cell高度自适应 */
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.title_lb.mas_bottom).offset(12);
        }];
    }
    return self;
}

- (void)setModel:(SetCellModel *)model{
    _model = model;
    _title_lb.text = model.title;
}

@end
