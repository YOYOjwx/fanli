//
//  CoinHeader.h
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CoinHeader : UICollectionReusableView

@property (nonatomic, strong) User *user;

@property (nonatomic, copy) void (^click)();

@end
