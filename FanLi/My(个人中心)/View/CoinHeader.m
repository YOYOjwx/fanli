//
//  CoinHeader.m
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CoinHeader.h"
#import "YUWebController.h"

@interface CoinHeader ()

/** 即将可用金币 */
@property (nonatomic, strong) UILabel *available_coins_lb;
/** 累计返利 */
@property (nonatomic, strong) UILabel *accumulated_rebates_lb;

@end

@implementation CoinHeader

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        /** 背景颜色 */
        self.backgroundColor = [UIColor whiteColor];
        /** 即将可用金币文本 */
        UILabel *available_coins_text = [UILabel new];
        [self addSubview:available_coins_text];
        available_coins_text.text = @"即将可用金币:";
        available_coins_text.font = CurrencyFont(12);
        [available_coins_text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@17.5);
            make.leading.equalTo(@30);
        }];
        self.available_coins_lb = [UILabel new];
        [self addSubview:self.available_coins_lb];
        self.available_coins_lb.textColor = UIColorFromHex(0xFF5E56);
        self.available_coins_lb.text = @"0";
        self.available_coins_lb.font = CurrencyFont(24);
        [self.available_coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(available_coins_text);
            make.top.equalTo(available_coins_text.mas_bottom).offset(10);
        }];
        /** 累计返利文本 */
        UILabel *accumulated_rebates_text = [UILabel new];
        [self addSubview:accumulated_rebates_text];
        accumulated_rebates_text.text = @"累计返利:";
        accumulated_rebates_text.font = CurrencyFont(12);
        [accumulated_rebates_text mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@17.5);
            make.leading.equalTo(weakSelf.mas_centerX);
        }];
        self.accumulated_rebates_lb = [UILabel new];
        [self addSubview:self.accumulated_rebates_lb];
        self.accumulated_rebates_lb.textColor = UIColorFromHex(0xFF5E56);
        self.accumulated_rebates_lb.font = CurrencyFont(24);
        self.accumulated_rebates_lb.text = @"0";
        [self.accumulated_rebates_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(accumulated_rebates_text);
            make.top.equalTo(accumulated_rebates_text.mas_bottom).offset(10);
        }];
        UIView *line = [UIView new];
        [self addSubview:line];
        line.backgroundColor = UIColorFromHex(0xF0F0F0);
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(@0);
            make.height.equalTo(@1);
        }];
        /** tap */
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(coinExplain)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

/** 金币说明 */
- (void)coinExplain{
    if (self.click) {
        self.click();
    }
}

- (void)setUser:(User *)user{
    _user = user;
    self.accumulated_rebates_lb.text = user.accumulated_rebates;
    self.available_coins_lb.text = user.available_coins;
}

@end
