//
//  CoinsMarketCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GiftModel.h"

@interface CoinsMarketCell : UICollectionViewCell

@property (nonatomic, strong) GiftModel *model;

@end
