//
//  CoinsMarketCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CoinsMarketCell.h"

@interface CoinsMarketCell ()

/** 商品图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 需要金币 */
@property (nonatomic, strong) UILabel *coins_lb;
/** 商品标题 */
@property (nonatomic, strong) UIButton *title_btn;
/** 选中礼品的样式 */
@property (nonatomic, strong) UIButton *select_gift_btn;

@end

@implementation CoinsMarketCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        /** 创建图片 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.width.equalTo(weakSelf.icon_image.mas_height);
        }];
        UIView *line = [UIView new];
        [self.contentView addSubview:line];
        line.backgroundColor = UIColorFromHex(0xF0F0F0);
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(@0);
            make.top.equalTo(weakSelf.icon_image.mas_bottom);
            make.height.equalTo(@0.5);
        }];
        self.select_gift_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.icon_image addSubview:self.select_gift_btn];
        self.select_gift_btn.userInteractionEnabled = NO;
        self.select_gift_btn.backgroundColor = RGBCOLOR(0, 0, 0, 0.3);
        [self.select_gift_btn setImage:[UIImage imageNamed:@"选中奖品"] forState:UIControlStateNormal];
        self.select_gift_btn.hidden = YES;
        [self.select_gift_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        /** 创建商品标题 */
        self.title_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.title_btn];
        self.title_btn.titleLabel.font = CurrencyFont(12);
        self.title_btn.titleLabel.numberOfLines = 0;
        [self.title_btn setTitleColor:UIColorFromHex(0x333333) forState:UIControlStateNormal];
        self.title_btn.backgroundColor = [UIColor whiteColor];
        [self.title_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.icon_image.mas_bottom);
            make.leading.trailing.bottom.equalTo(@0);
        }];
        [self.title_btn.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@5);
            make.trailing.equalTo(@-5);
        }];
        /** 创建需要金币 */
        self.coins_lb = [UILabel new];
        [self.icon_image addSubview:self.coins_lb];
        self.coins_lb.backgroundColor = UIColorFromHex(0xFF5E56);
        self.coins_lb.textColor = [UIColor whiteColor];
        self.coins_lb.font = CurrencyFont(12);
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@10);
            make.bottom.equalTo(@-10);
            make.height.equalTo(@20);
        }];
    }
    return self;
}

- (void)setSelected:(BOOL)selected{
    [super setSelected:selected];
    self.select_gift_btn.hidden = !selected;
}

- (void)setModel:(GiftModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    [_title_btn setTitle:model.giftDesc forState:UIControlStateNormal];
    _coins_lb.text = [NSString stringWithFormat:@" %d金币 ",model.ycoinPrice];
}

@end
