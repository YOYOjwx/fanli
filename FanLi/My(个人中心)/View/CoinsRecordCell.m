//
//  CoinsRecordCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CoinsRecordCell.h"

@interface CoinsRecordCell ()

/** 集分 */
@property (nonatomic, strong) UILabel *coins_lb;
/** 状态文本 */
@property (nonatomic, strong) UILabel *status_lb;
/** 兑换时间 */
@property (nonatomic, strong) UILabel *date_lb;

@end

@implementation CoinsRecordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = NO;
        WS(weakSelf);
        /** 创建积分 */
        self.coins_lb = [UILabel new];
        [self.contentView addSubview:self.coins_lb];
        self.coins_lb.textColor = UIColorFromHex(0xFF5E56);
        self.coins_lb.font = CurrencyFont(26);
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@15);
            make.leading.equalTo(@25);
        }];
        /** 创建集分宝文字 */
        UILabel *jifenbao_text_lb = [UILabel new];
        [self.contentView addSubview:jifenbao_text_lb];
        jifenbao_text_lb.font = CurrencyFont(10);
        jifenbao_text_lb.text = @"集分宝";
        jifenbao_text_lb.textColor = self.coins_lb.textColor;
        [jifenbao_text_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@27);
            make.top.equalTo(weakSelf.coins_lb.mas_bottom).offset(5);
        }];
        /** 创建顶部横线 */
        UIView *top_line_view = [UIView new];
        [self.contentView addSubview:top_line_view];
        top_line_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [top_line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
        /** 创建提现虚线 */
        UIImageView *dotted_line_image = [UIImageView new];
        [self.contentView addSubview:dotted_line_image];
        dotted_line_image.image = [UIImage imageNamed:@"提现记录虚线"];
        dotted_line_image.contentMode = UIViewContentModeScaleAspectFill;
        dotted_line_image.layer.masksToBounds = YES;
        [dotted_line_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@7.5);
            make.bottom.equalTo(@-7.5);
            make.width.equalTo(@0.5);
            make.leading.equalTo(@100);
        }];
        /** 创建兑换状态 */
        self.status_lb = [UILabel new];
        [self.contentView addSubview:self.status_lb];
        self.status_lb.font = CurrencyFont(14);
        self.status_lb.text = @"审核中...";
        [self.status_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(dotted_line_image.mas_trailing).offset(20);
            make.top.equalTo(@20);
        }];
        /** 创建兑换日期 */
        self.date_lb = [UILabel new];
        [self.contentView addSubview:self.date_lb];
        self.date_lb.font = CurrencyFont(12);
        self.date_lb.textColor = UIColorFromHex(0x999999);
        [self.date_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.status_lb.mas_bottom).offset(5);
            make.leading.equalTo(weakSelf.status_lb);
        }];
    }
    return self;
}

- (void)setModel:(RecordModel *)model{
    _model = model;
    _coins_lb.text = model.payAmount.stringValue;
    _date_lb.text = [model.createTime stringByAppendingString:@" 兑换"];
    switch (model.payStatus) {
        case PayStatusExamine:{
            _status_lb.text = @"审核中...";
            _status_lb.textColor = _coins_lb.textColor;
            break;
        }
        case PayStatusFailure:{
            _status_lb.text = @"兑换失败";
            _status_lb.textColor = [UIColor blackColor];
            break;
        }
        case PayStatusSuccess:{
            _status_lb.text = @"兑换成功";
            _status_lb.textColor = [UIColor blackColor];
            break;
        }
    }
}

@end
