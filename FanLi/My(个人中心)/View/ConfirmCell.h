//
//  ConfirmCell.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SetCellModel.h"

@interface ConfirmCell : UITableViewCell

/** 设置model */
@property (nonatomic, strong) SetCellModel *model;

@end
