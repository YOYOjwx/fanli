//
//  ConfirmCell.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "ConfirmCell.h"

@interface ConfirmCell ()

/** 确认按钮 */
@property (nonatomic, strong) UIButton *confirm_btn;

@end

@implementation ConfirmCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = NO;
        self.confirm_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.confirm_btn];
        self.confirm_btn.enabled = NO;
        [self.confirm_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.confirm_btn.backgroundColor = UIColorFromHex(0xFF5E56);
        self.confirm_btn.titleLabel.font = CurrencyFont(15);
        self.confirm_btn.layer.masksToBounds = YES;
        self.confirm_btn.layer.cornerRadius = 4;
        [self.confirm_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(@20);
            make.trailing.equalTo(@-20);
            make.height.equalTo(@40);
        }];
        
        /** 设置cell自适应高度 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.bottom.equalTo(weakSelf.confirm_btn.mas_bottom).offset(20);
            
        }];
    }
    return self;
}

- (void)setModel:(SetCellModel *)model{
    _model = model;
    [self.confirm_btn setTitle:model.title forState:UIControlStateNormal];
}

@end
