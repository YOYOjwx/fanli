//
//  EarnCoinsHeader.h
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CateItemModel.h"

@interface EarnCoinsHeader : UIView

@property (nonatomic, copy) void (^click)(CateItemModel *model);

/** 请求地址 */
@property (nonatomic, strong) NSString *url;
/** 请求参数 */
@property (nonatomic, strong) NSDictionary *dic;

@end


@interface EarnCoinsHeaderCell : UICollectionViewCell

@property (nonatomic, strong) UIImageView *icon_image;

@end
