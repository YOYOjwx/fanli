//
//  EarnCoinsHeader.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "EarnCoinsHeader.h"
#import "CateItemModel.h"
#import "TaobaoDetail.h"

@interface EarnCoinsHeader ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSMutableArray *array;

@end

@implementation EarnCoinsHeader

- (void)dealloc{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(cycleStart) object:nil];
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(moveCenter) object:nil];
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 0;
        layout.minimumInteritemSpacing = 0;
        layout.itemSize = frame.size;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = UIColorFromHex(0xF0F0F0);
        [self addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        self.collectionView.pagingEnabled = YES;
        self.collectionView.showsHorizontalScrollIndicator = NO;
        [self.collectionView registerClass:[EarnCoinsHeaderCell class] forCellWithReuseIdentifier:@"EarnCoinsHeaderCell"];
    }
    return self;
}

- (void)setUrl:(NSString *)url{
    _url = url;
    [self getCycleData];
}

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    if (self.array.count == 0) {
        return 0;
    }
    return 300;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    EarnCoinsHeaderCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EarnCoinsHeaderCell" forIndexPath:indexPath];
    CateItemModel *model = self.array[indexPath.item];
    [cell.icon_image sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (self.click) {
        self.click(self.array[indexPath.item]);
    }
}

- (void)getCycleData{
    [[YURequest creatMarketRequest] POST:self.url parameters:self.dic success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            CateItemModel *model = [[CateItemModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        [self.collectionView reloadData];
        self.collectionView.contentOffset = CGPointMake(kMainWidth * 150 * self.array.count, 0);
        [self performSelector:@selector(cycleStart) withObject:nil afterDelay:3];
    } failure:nil];
}

- (void)cycleStart{
    [self.collectionView setContentOffset:CGPointMake(self.collectionView.contentOffset.x + kMainWidth, 0) animated:YES];
    if (self.collectionView.contentOffset.x > 299 * self.array.count * kMainWidth) {
        [self moveCenter];
    }
    [self performSelector:@selector(cycleStart) withObject:nil afterDelay:3];
}

/** 滑动结束 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(moveCenter) object:nil];
    [self performSelector:@selector(moveCenter) withObject:nil afterDelay:3];
}

/** 滑动结束了，自己偷偷移动到中间 */
- (void)moveCenter{
    self.collectionView.contentOffset = CGPointMake(kMainWidth * 150 * self.array.count, 0);
}

@end

@implementation EarnCoinsHeaderCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
    }
    return self;
}

@end
