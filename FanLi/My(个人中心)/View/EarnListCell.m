//
//  EarnListCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "EarnListCell.h"

@interface EarnListCell ()

/** 图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 副标题 */
@property (nonatomic, strong) UILabel *detail_lb;

@end

@implementation EarnListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        /** 创建图片 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.layer.cornerRadius = 3;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.equalTo(@10);
            make.bottom.equalTo(@-10);
            make.width.equalTo(weakSelf.icon_image.mas_height);
        }];
        /** 创建标题 */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(10);
            make.bottom.equalTo(weakSelf.contentView.mas_centerY).offset(-2.5);
        }];
        /** 创建副标题 */
        self.detail_lb = [UILabel new];
        [self.contentView addSubview:self.detail_lb];
        self.detail_lb.font = CurrencyFont(12);
        self.detail_lb.textColor = UIColorFromHex(0x666666);
        [self.detail_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(10);
            make.top.equalTo(weakSelf.contentView.mas_centerY).offset(2.5);
        }];
        UIView *line = [UIView new];
        [self.contentView addSubview:line];
        line.backgroundColor = UIColorFromHex(0xF0F0F0);
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}

- (void)setModel:(EarnListModel *)model{
    _model = model;
    _icon_image.image = [UIImage imageNamed:model.icon];
    _title_lb.text = model.title;
    _detail_lb.text = model.detail;
}

@end
