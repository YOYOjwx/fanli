//
//  IconCell.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "IconCell.h"

@interface IconCell ()

/** 头像 */
@property (nonatomic, strong) UIImageView *icon_image;

@end

@implementation IconCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        /** 创建头像 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.layer.cornerRadius = 20;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(weakSelf.arrow_image.mas_leading).offset(-5);
            make.centerY.equalTo(weakSelf.title_lb);
            make.width.height.equalTo(@40);
        }];
        /** 更新设置cell高度自适应 */
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.icon_image.mas_bottom).offset(12);
        }];
    }
    return self;
}

- (void)setModel:(SetCellModel *)model{
    [super setModel:model];
    [self.icon_image sd_setImageWithURL:[NSURL URLWithString:model.detail] placeholderImage:[UIImage imageNamed:@"头像"]];}

@end
