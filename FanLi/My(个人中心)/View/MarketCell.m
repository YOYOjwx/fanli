//
//  MarketCell.m
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MarketCell.h"

@interface MarketCell ()

/** 商品图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 商品标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 订单编号 */
@property (nonatomic, strong) UILabel *order_number_lb;
/** 商品价钱 */
@property (nonatomic, strong) UILabel *money_lb;
/** 返利金币 */
@property (nonatomic, strong) UILabel *coins_lb;
/** 订单状态 */
@property (nonatomic, strong) UILabel *order_status_lb;
/** 返利状态 */
@property (nonatomic, strong) UILabel *fanli_status_lb;

@end

@implementation MarketCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = NO;
        UIView *bg_view = [UIView new];
        [self.contentView addSubview:bg_view];
        bg_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.height.equalTo(@10);
        }];
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.contentMode = UIViewContentModeScaleAspectFit;
        self.icon_image.layer.masksToBounds = YES;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bg_view.mas_bottom).offset(10);
            make.leading.equalTo(@10);
            make.bottom.equalTo(@-10);
            make.width.equalTo(@100);
        }];
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_icon_image.mas_trailing).offset(10);
            make.top.equalTo(_icon_image);
        }];
        self.order_number_lb = [UILabel new];
        [self.contentView addSubview:self.order_number_lb];
        self.order_number_lb.textColor =UIColorFromHex(0x333333);
        self.order_number_lb.font = CurrencyFont(14);
        self.order_number_lb.numberOfLines = 2;
        [self.order_number_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_title_lb);
            make.top.equalTo(_title_lb.mas_bottom);
            make.trailing.equalTo(@-10);
        }];
        self.order_status_lb = [UILabel new];
        [self.contentView addSubview:self.order_status_lb];
        self.order_status_lb.font = CurrencyFont(10);
        self.order_status_lb.textColor = [UIColor whiteColor];
        self.order_status_lb.backgroundColor = UIColorFromHex(0xFF5E56);
        [self.order_status_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(_icon_image);
            make.leading.equalTo(_title_lb);
            make.height.equalTo(@16);
        }];
        self.money_lb = [UILabel new];
        [self.contentView addSubview:self.money_lb];
        self.money_lb.textColor = UIColorFromHex(0xFF5E56);
        self.money_lb.font = [UIFont boldSystemFontOfSize:16];
        [self.money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_title_lb).offset(10.5);
            make.bottom.equalTo(_order_status_lb.mas_top);
        }];
        UILabel *rmb = [UILabel new];
        [self.contentView addSubview:rmb];
        rmb.text = @"￥";
        rmb.textColor = UIColorFromHex(0xFF5E56);
        rmb.font = CurrencyFont(10);
        [rmb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(_money_lb.mas_leading);
            make.bottom.equalTo(_money_lb).offset(-2.5);
        }];
        self.fanli_status_lb = [UILabel new];
        [self.contentView addSubview:self.fanli_status_lb];
        self.fanli_status_lb.font = CurrencyFont(12);
        self.fanli_status_lb.textColor = UIColorFromHex(0x333333);
        [self.fanli_status_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_money_lb);
            make.leading.equalTo(_money_lb.mas_trailing).offset(5);
        }];
        self.coins_lb = [UILabel new];
        [self.contentView addSubview:self.coins_lb];
        self.coins_lb.font = CurrencyFont(12);
        self.coins_lb.textColor = UIColorFromHex(0xFF5E56);
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_money_lb);
            make.leading.equalTo(_fanli_status_lb.mas_trailing);
        }];
    }
    return self;
}

- (void)setModel:(MallOrderModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _title_lb.text = model.mallName;
    _order_number_lb.text = [@"订单编号:" stringByAppendingString:model.partnerOrderId];
    _money_lb.text = [NSString stringWithFormat:@"%0.2f",model.orderPrice];
    _coins_lb.text = [model.userCoins.stringValue stringByAppendingString:@"金币"];
    if (model.status) {
        _fanli_status_lb.text = @"返利到账: ";
        _order_status_lb.text = @" 订单已返利 ";
    }else{
        _order_status_lb.text = @" 订单已追踪 ";
        _fanli_status_lb.text = @"预计返利: ";
    }
}

@end
