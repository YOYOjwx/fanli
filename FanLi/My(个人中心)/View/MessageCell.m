//
//  MessageCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/24.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MessageCell.h"

@interface MessageCell ()

/** 头像 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 文本内容 */
@property (nonatomic, strong) UILabel *content_lb;
/** 日期时间 */
@property (nonatomic, strong) UILabel *date_lb;

@end

@implementation MessageCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = NO;
        /** cell自动约束高度 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.leading.trailing.equalTo(@0);
        }];
        /** 创建头像 */
        self.icon_image  = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.layer.cornerRadius = 20;
        self.icon_image.image = [UIImage imageNamed:@"消息中心头像"];
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@20);
            make.leading.equalTo(@10);
            make.width.height.equalTo(@40);
        }];
        /** 创建文本外框视图 */
        UIView *view_content = [UIView new];
        [self.contentView addSubview:view_content];
        view_content.backgroundColor = [UIColor whiteColor];
        view_content.layer.masksToBounds = YES;
        view_content.layer.cornerRadius = 5;
        [view_content mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.icon_image);
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(20);
            make.trailing.equalTo(@-30);
            make.bottom.equalTo(@0);
        }];
        /** 创建左边箭头 */
        UIImageView *left_arrow_image = [UIImageView new];
        [self.contentView addSubview:left_arrow_image];
        left_arrow_image.image = [UIImage imageNamed:@"消息左箭头"];
        [left_arrow_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.icon_image);
            make.trailing.equalTo(view_content.mas_leading);
        }];
        /** 创建文本内容 */
        self.content_lb = [UILabel new];
        [view_content addSubview:self.content_lb];
        self.content_lb.numberOfLines = 0;
        self.content_lb.font = CurrencyFont(15);
        self.content_lb.textColor = UIColorFromHex(0x333333);
        [self.content_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.equalTo(@10);
            make.trailing.equalTo(@-5);
        }];
        /** 更新文本外框视图的高度 */
        [view_content mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.icon_image);
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(20);
            make.trailing.equalTo(@-30);
            make.bottom.equalTo(weakSelf.content_lb.mas_bottom).offset(10);
        }];
        /** 创建日期 */
        self.date_lb = [UILabel new];
        [self.contentView addSubview:self.date_lb];
        self.date_lb.font = CurrencyFont(10);
        self.date_lb.textColor = UIColorFromHex(0x666666);
        [self.date_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(view_content.mas_bottom).offset(10);
            make.leading.equalTo(view_content);
        }];
        /** 更新cell自动高度的约束 */
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.date_lb.mas_bottom).offset(10);
        }];
        /** cell选中180度 */
        self.contentView.transform = CGAffineTransformMakeRotation(2 * M_PI_2);
    }
    return self;
}

- (void)setModel:(MessageModel *)model{
    _model = model;
    _content_lb.text = model.contentObject;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"M-dd hh:mm";
    _date_lb.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:model.timestamp]];
}

@end
