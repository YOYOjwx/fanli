//
//  MyHeader.m
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MyHeader.h"
#import "QuickLoginButton.h"
#import "LoginController.h"
#import "YUWebController.h"

@interface MyHeader ()

/** 背景图 */
@property (nonatomic, strong) UIImageView *bg_image;
/** 金币 */
@property (nonatomic, strong) UILabel *coin_lb;
/** 价值多人人民币 */
@property (nonatomic, strong) UILabel *rmb_lb;
/** 登录按钮 */
@property (nonatomic, strong) QuickLoginButton *login_btn;
/** UID */
@property (nonatomic, strong) UILabel *uid_lb;

@end

@implementation MyHeader

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        /** 创建金币 */
        self.coin_lb = [UILabel new];
        [self addSubview:self.coin_lb];
        self.coin_lb.text = @"0";
        self.coin_lb.textColor = [UIColor whiteColor];
        self.coin_lb.font = [UIFont boldSystemFontOfSize:36];
        [self.coin_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(@25);
        }];
        /** 创建可用金币 */
        UILabel *available_coin_lb = [UILabel new];
        [self addSubview:available_coin_lb];
        available_coin_lb.text = @"可用金币";
        available_coin_lb.textColor = [UIColor whiteColor];
        available_coin_lb.font = CurrencyFont(12);
        [available_coin_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.coin_lb);
            make.bottom.equalTo(weakSelf.coin_lb.mas_top);
        }];
        /** 创建价值多人人民币 */
        self.rmb_lb = [UILabel new];
        [self addSubview:self.rmb_lb];
        self.rmb_lb.textColor = [UIColor whiteColor];
        self.rmb_lb.text = @"值0.00元";
        self.rmb_lb.font = CurrencyFont(12);
        [self.rmb_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.coin_lb);
            make.top.equalTo(weakSelf.coin_lb.mas_bottom);
        }];
        /** 金币说明 */
        UIButton *coin_explain_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:coin_explain_btn];
        [coin_explain_btn setImage:[UIImage imageNamed:@"金币说明"] forState:UIControlStateNormal];
        [coin_explain_btn addTarget:self action:@selector(coinExplain) forControlEvents:UIControlEventTouchUpInside];
        [coin_explain_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(weakSelf.coin_lb.mas_trailing).offset(10);
        }];
        /** 登录按钮 */
        self.login_btn = [QuickLoginButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:self.login_btn];
        self.login_btn.titleColor = [UIColor whiteColor];
        [self.login_btn setTitle:@"     登录     " forState:UIControlStateNormal];
        [self.login_btn addTarget:self action:@selector(login:) forControlEvents:UIControlEventTouchUpInside];
        [self.login_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.trailing.equalTo(@-25);
        }];
        /** UID */
        self.uid_lb = [UILabel new];
        [self addSubview:self.uid_lb];
        self.uid_lb.font = CurrencyFont(12);
        self.uid_lb.textColor = [UIColor whiteColor];
        self.uid_lb.hidden = YES;
        [self.uid_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.trailing.equalTo(weakSelf.login_btn);
        }];
    }
    return self;
}

- (void)setUser:(User *)user{
    if (IS_LOGIN) {
        self.login_btn.hidden = YES;
    }else{
        self.login_btn.hidden = NO;
    }
    self.uid_lb.hidden = !self.login_btn.hidden;
    self.uid_lb.text = [NSString stringWithFormat:@"ID:%@",UD_UID];
    self.coin_lb.text = [NSString stringWithFormat:@"%d",user.ycoins];
    self.rmb_lb.text = [NSString stringWithFormat:@"值%0.2f元",(float)user.ycoins / 100];
}

/** 登录 */
- (void)login:(QuickLoginButton *)btn{
    [LoginController isLoginStatus:^(User *user) {
        btn.hidden = YES;
        self.uid_lb.hidden = !self.login_btn.hidden;
    }];
}

/** 金币说明 */
- (void)coinExplain{
    if (self.click) {
        self.click();
    }
}

@end
