//
//  MyItemCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyItemModel.h"

@interface MyItemCell : UICollectionViewCell

/** 数据模型 */
@property (nonatomic, strong) MyItemModel *model;

@end
