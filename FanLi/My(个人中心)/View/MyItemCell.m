//
//  MyItemCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/16.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MyItemCell.h"

@interface MyItemCell ()

/** 图标 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 名称 */
@property (nonatomic, strong) UILabel *title_lb;

@end

@implementation MyItemCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        /** 创建图标 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.centerY.equalTo(@-11.25);
        }];
        /** 创建标题 */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(12);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.top.equalTo(weakSelf.icon_image.mas_bottom).offset(8);
        }];
        /** 添加contentView的高度，让Item自动适应宽高 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            
            make.width.height.equalTo(@(kMainWidth / 4));
        }];
    }
    return self;
}

- (void)setModel:(MyItemModel *)model{
    _model = model;
    _icon_image.image = [UIImage imageNamed:model.icon];
    _title_lb.text = model.title;
    if (model.type > 2) {
        [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.equalTo(@0);
            make.width.height.equalTo(@((kMainWidth / 4) - 1));
        }];
        _title_lb.textColor = [UIColor blackColor];
        self.backgroundColor = [UIColor whiteColor];
    }else{
        [self.contentView mas_remakeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.equalTo(@0);
            make.width.equalTo(@(kMainWidth / 3));
            make.height.equalTo(self.contentView.mas_width).multipliedBy(0.6);
        }];
        _title_lb.textColor = UIColorFromHex(0x7D728B);
        self.backgroundColor = [UIColor clearColor];
    }
}

@end
