//
//  NoobTaskCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EarnListModel.h"

@interface NoobTaskCell : UITableViewCell

@property (nonatomic, strong) EarnListModel *model;

@end
