//
//  NoobTaskCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/25.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "NoobTaskCell.h"

@interface NoobTaskCell ()

/** 标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 内容 */
@property (nonatomic, strong) UILabel *detail_lb;
/** 图片说明 */
@property (nonatomic, strong) UIImageView *icon_image;

@end

@implementation NoobTaskCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        self.selectionStyle = NO;
        /** 这是cell自己高度的约束 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.trailing.bottom.equalTo(@0);
        }];
        /** 创建红点 */
        UIView *red_dot_view = [UIView new];
        [self.contentView addSubview:red_dot_view];
        red_dot_view.backgroundColor = UIColorFromHex(0xFF5E56);
        red_dot_view.layer.masksToBounds = YES;
        red_dot_view.layer.cornerRadius = 5;
        [red_dot_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@15);
            make.top.equalTo(@20);
            make.width.height.equalTo(@10);
        }];
        /** 创建标题 */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = [UIFont boldSystemFontOfSize:16];
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(red_dot_view.mas_trailing).offset(15);
            make.centerY.equalTo(red_dot_view);
        }];
        /** 创建文本内容 */
        self.detail_lb = [UILabel new];
        [self.contentView addSubview:self.detail_lb];
        self.detail_lb.font = CurrencyFont(16);
        self.detail_lb.textColor = [UIColor grayColor];
        self.detail_lb.numberOfLines = 0;
        [self.detail_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.title_lb);
            make.top.equalTo(weakSelf.title_lb.mas_bottom).offset(15);
            make.trailing.equalTo(@-15);
        }];
        /** 创建图片显示 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(weakSelf.detail_lb);
            make.top.equalTo(weakSelf.detail_lb.mas_bottom).offset(15);
            make.height.equalTo(@0);
        }];
        /** 更新cell高的 */
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.icon_image.mas_bottom).offset(30);
        }];
        UIView *line_view = [UIView new];
        [self.contentView insertSubview:line_view atIndex:0];
        line_view.backgroundColor = UIColorFromHex(0xE6E6E6);
        [line_view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(red_dot_view.mas_centerY);
            make.centerX.equalTo(red_dot_view.mas_centerX);
            make.width.equalTo(@1);
            make.bottom.equalTo(weakSelf.contentView.mas_bottom).offset(20);
        }];
    }
    return self;
}

- (void)setModel:(EarnListModel *)model{
    _model = model;
    _title_lb.text = model.title;
    _detail_lb.text = model.detail;
    UIImage *image = [UIImage imageNamed:model.icon];
    _icon_image.image = image;
    [_icon_image mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(_detail_lb);
        make.top.equalTo(_detail_lb.mas_bottom).offset(15);
        CGFloat height = (kMainWidth - 55) / image.size.width * image.size.height;
        make.height.mas_equalTo(height);
    }];
}

@end
