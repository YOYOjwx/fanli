//
//  OrderCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OrderModel.h"

@interface OrderCell : UITableViewCell

@property (nonatomic, strong) OrderModel *model;

@property (nonatomic, copy) void (^share)(OrderModel *model);

@end
