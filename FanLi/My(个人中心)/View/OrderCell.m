//
//  OrderCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "OrderCell.h"
#import "YUProgress.h"
#import "QuickLoginButton.h"

@interface OrderCell ()

/** title */
@property (nonatomic, strong) UILabel *title_lb;
/** 实际金钱 */
@property (nonatomic, strong) UILabel *origin_money_lb;
/** 付款金钱 */
@property (nonatomic, strong) UILabel *pay_money_lb;
/** 返利的金钱 */
@property (nonatomic, strong) UILabel *rebate_money_lb;
/** 图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 下方进度模块视图 */
@property (nonatomic, strong) UIView *bottom_view;
/** 进度条 */
@property (nonatomic, strong) YUProgress *progress_view;
/** 进度数字 */
@property (nonatomic, strong) UILabel *progress_lb;
/** 返利时间 */
@property (nonatomic, strong) UILabel *rebate_date_lb;
/** 人民币符号 */
@property (nonatomic, strong) UILabel *rmb_lb;
/** 分享按钮 */
@property (nonatomic, strong) UIButton *share_btn;

@end

@implementation OrderCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        /** cell自动约束高度 */
        [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.bottom.leading.trailing.equalTo(@0);
        }];
        /** 顶部分割线图 */
        UIView *top_view = [UIView new];
        [self.contentView addSubview:top_view];
        top_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [top_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.height.equalTo(@10);
        }];
        /** 创建图片 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        self.icon_image.layer.masksToBounds = YES;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@10);
            make.top.equalTo(top_view.mas_bottom).offset(10);
            make.width.height.equalTo(@100);
        }];
        /** 创建标题 */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.numberOfLines = 0;
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.icon_image);
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(10);
            make.trailing.equalTo(@-10);
        }];
        /** 创建返利 */
        self.rebate_money_lb = [UILabel new];
        [self.contentView addSubview:self.rebate_money_lb];
        self.rebate_money_lb.font = CurrencyFont(10);
        self.rebate_money_lb.textColor = [UIColor whiteColor];
        self.rebate_money_lb.backgroundColor = UIColorFromHex(0xFF5E56);
        [self.rebate_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.title_lb);
            make.bottom.equalTo(weakSelf.icon_image);
            make.height.equalTo(@16);
        }];
        /** 创建人民币符号 */
        self.rmb_lb = [UILabel new];
        [self.contentView addSubview:self.rmb_lb];
        self.rmb_lb.textColor = UIColorFromHex(0xFF5E56);
        self.rmb_lb.text = @"￥";
        self.rmb_lb.font = CurrencyFont(10);
        [self.rmb_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.title_lb);
            make.bottom.equalTo(weakSelf.rebate_money_lb.mas_top).offset(-3);
        }];
        /** 创建付款金币 */
        self.pay_money_lb = [UILabel new];
        [self.contentView addSubview:self.pay_money_lb];
        self.pay_money_lb.textColor = UIColorFromHex(0xFF5E56);
        self.pay_money_lb.font = [UIFont boldSystemFontOfSize:16];
        [self.pay_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(self.rmb_lb.mas_trailing);
            make.centerY.equalTo(self.rmb_lb).offset(-1.5);
        }];
        /** 创建实际金额 */
        self.origin_money_lb = [UILabel new];
        [self.contentView addSubview:self.origin_money_lb];
        self.origin_money_lb.textColor = UIColorFromHex(0x999999);
        self.origin_money_lb.font = CurrencyFont(12);
        [self.origin_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.pay_money_lb);
            make.leading.equalTo(weakSelf.pay_money_lb.mas_trailing).offset(5);
        }];
        UIView *delete_line_view = [UIView new];
        [self.origin_money_lb addSubview:delete_line_view];
        delete_line_view.backgroundColor = UIColorFromHex(0x999999);
        [delete_line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
            make.width.equalTo(weakSelf.origin_money_lb).multipliedBy(1.15);
            make.height.equalTo(@0.5);
        }];
        /** 创建底部视图 */
        self.bottom_view = [UIView new];
        [self.contentView addSubview:self.bottom_view];
        self.bottom_view.layer.masksToBounds = YES;
        [self.bottom_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(@0);
            make.top.equalTo(weakSelf.icon_image.mas_bottom).offset(10);
            make.height.equalTo(@35);
        }];
        UIView *line_view = [UIView new];
        line_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [self.bottom_view addSubview:line_view];
        [line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0.5);
            make.leading.equalTo(weakSelf.icon_image);
            make.top.trailing.equalTo(@0);
        }];
        /** 创建进度条视图 */
        self.progress_view = [YUProgress new];
        [self.bottom_view addSubview:self.progress_view];
        [self.progress_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(weakSelf.icon_image);
            make.width.equalTo(weakSelf.icon_image);
            make.height.equalTo(@5);
        }];
        /** 创建进度条文字显示 */
        self.progress_lb = [UILabel new];
        [self.bottom_view addSubview:self.progress_lb];
        self.progress_lb.textColor = UIColorFromHex(0xFF5E56);
        self.progress_lb.font = CurrencyFont(12);
        [self.progress_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.progress_view.mas_trailing).offset(5);
            make.centerY.equalTo(@0);
        }];
        /** 返利时间 */
        self.rebate_date_lb = [UILabel new];
        [self.bottom_view addSubview:self.rebate_date_lb];
        self.rebate_date_lb.font = CurrencyFont(12);
        self.rebate_date_lb.textColor = UIColorFromHex(0x999999);
        [self.rebate_date_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(@-10);
            make.centerY.equalTo(@0);
        }];
        /** 创建分享按钮 */
        self.share_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.contentView addSubview:self.share_btn];
        [self.share_btn addTarget:self action:@selector(orderShare) forControlEvents:UIControlEventTouchUpInside];
        UILabel *share_lb = [UILabel new];
        [self.share_btn addSubview:share_lb];
        share_lb.text = @"分享";
        share_lb.font = CurrencyFont(12);
        share_lb.textColor = UIColorFromHex(0x999999);
        [share_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(@0);
            make.bottom.equalTo(weakSelf.rebate_money_lb);
        }];
        UIImageView *share_image = [UIImageView new];
        [self.share_btn addSubview:share_image];
        share_image.image = [UIImage imageNamed:@"分享图标"];
        [share_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(share_lb);
            make.bottom.equalTo(share_lb.mas_top).offset(-3);
        }];
        [self.share_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.bottom.equalTo(share_lb);
            make.top.equalTo(share_image);
            make.trailing.equalTo(@-10);
        }];
        /** cell自动约束高度 */
        [self.contentView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(weakSelf.bottom_view);
        }];
    }
    return self;
}

- (void)setModel:(OrderModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.picUrl] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _title_lb.text = model.tbItemTitle;
    _origin_money_lb.text = [NSString stringWithFormat:@"%0.2f",model.itemPrice];
    _pay_money_lb.text = [NSString stringWithFormat:@"%0.2f",model.itemUpmPrice];
    _rebate_money_lb.text = [NSString stringWithFormat:@" 返%d金币 ",model.itemCoins];
    _progress_lb.text = [NSString stringWithFormat:@"%d%%",model.ycoinsReturnPer];
    _progress_view.progress = @(model.ycoinsReturnPer).intValue;
    if (model.ycoinsReturnDays) {
        _rebate_date_lb.text = [NSString stringWithFormat:@"预计进度:%d后返利",model.ycoinsReturnDays];
    }else{
        _rebate_date_lb.text = @"确认收货后才能收到返利";
    }
    if (model.sendStatus == 0) {/** 审核中 */
        _share_btn.hidden = YES;
        [_bottom_view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@35);
        }];
    }else{/** 已返利 */
        _share_btn.hidden = NO;
        [_bottom_view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.height.equalTo(@0);
        }];
    }
    if (model.tbItemId) {/** 代表是商品 */
        _origin_money_lb.hidden = self.rmb_lb.hidden = _pay_money_lb.hidden = NO;
        self.selectionStyle = UITableViewCellSelectionStyleBlue;
    }else{/** 代表是活动或者任务 */
        _origin_money_lb.hidden = self.rmb_lb.hidden = _pay_money_lb.hidden = YES;
        self.selectionStyle = NO;
    }
}

- (void)orderShare{
    if (self.share) {
        self.share(self.model);
    }
}

@end
