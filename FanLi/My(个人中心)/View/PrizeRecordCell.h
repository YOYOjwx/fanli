//
//  PrizeRecordCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordModel.h"

@interface PrizeRecordCell : UITableViewCell

@property (nonatomic, strong) RecordModel *model;

@end
