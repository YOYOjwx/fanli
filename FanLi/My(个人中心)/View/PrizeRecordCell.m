//
//  PrizeRecordCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/23.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "PrizeRecordCell.h"

@interface PrizeRecordCell ()

/** 图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** title */
@property (nonatomic, strong) UILabel *title_lb;
/** 兑换日期 */
@property (nonatomic, strong) UILabel *date_lb;
/** 兑换金币数目 */
@property (nonatomic, strong) UILabel *coins_lb;
/** 兑换状态 */
@property (nonatomic, strong) UILabel *status_lb;

@end

@implementation PrizeRecordCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        self.selectionStyle = NO;
        /** 创建顶部透明视图 */
        UIView *bg_view = [UIView new];
        [self.contentView addSubview:bg_view];
        bg_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [bg_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.height.equalTo(@10);
        }];
        /** 创建图片 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        self.icon_image.layer.masksToBounds = YES;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(bg_view.mas_bottom).offset(10);
            make.leading.equalTo(@10);
            make.bottom.equalTo(@-10);
            make.width.equalTo(weakSelf.icon_image.mas_height);
        }];
        /** 创建title */
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        self.title_lb.text = @"夏季情侣居家浴室按摩拖鞋";
        self.title_lb.numberOfLines = 0;
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.icon_image.mas_trailing).offset(10);
            make.trailing.equalTo(@-10);
            make.top.equalTo(weakSelf.icon_image);
        }];
        /** 创建横线 */
        UIView *line_view = [UIView new];
        [self.contentView addSubview:line_view];
        line_view.backgroundColor = UIColorFromHex(0xE6E6E6);
        [line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.title_lb);
            make.top.equalTo(weakSelf.title_lb.mas_bottom).offset(10);
            make.height.equalTo(@0.5);
            make.width.equalTo(weakSelf.title_lb.mas_width).multipliedBy(0.65);
        }];
        /** 创建兑换日期 */
        self.date_lb = [UILabel new];
        [self.contentView addSubview:self.date_lb];
        self.date_lb.text = @"2017.05.23 15:39:06 兑换";
        self.date_lb.font = CurrencyFont(12);
        self.date_lb.textColor = UIColorFromHex(0x999999);
        [self.date_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.title_lb);
            make.top.equalTo(line_view.mas_bottom).offset(5);
        }];
        /** 创建兑换金币数目 */
        self.coins_lb = [UILabel new];
        [self.contentView addSubview:self.coins_lb];
        self.coins_lb.backgroundColor = UIColorFromHex(0xFF5E56);
        self.coins_lb.font = CurrencyFont(10);
        self.coins_lb.text = @"1600金币";
        self.coins_lb.textColor = [UIColor whiteColor];
        self.coins_lb.textAlignment = NSTextAlignmentCenter;
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(weakSelf.date_lb.mas_bottom).offset(5);
            make.leading.equalTo(weakSelf.title_lb);
            make.width.equalTo(@60);
            make.height.equalTo(@20);
        }];
        /** 创建兑换状态 */
        self.status_lb = [UILabel new];
        [self.contentView addSubview:self.status_lb];
        self.status_lb.backgroundColor = [UIColor whiteColor];
        self.status_lb.layer.masksToBounds = YES;
        self.status_lb.layer.borderWidth = 0.5;
        self.status_lb.layer.borderColor = UIColorFromHex(0xFF5E56).CGColor;
        self.status_lb.textColor = UIColorFromHex(0xFF5E56);
        self.status_lb.font = CurrencyFont(10);
        self.status_lb.textAlignment = NSTextAlignmentCenter;
        [self.status_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(weakSelf.coins_lb.mas_trailing);
            make.top.width.height.equalTo(weakSelf.coins_lb);
        }];
    }
    return self;
}

- (void)setModel:(RecordModel *)model{
    _model = model;
    switch (model.payStatus) {
        case PayStatusSuccess:self.status_lb.text = @"兑换成功";break;
        case PayStatusFailure:self.status_lb.text = @"兑换失败";break;
        case PayStatusExamine:self.status_lb.text = @"审核中...";break;
    }
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.imgUrl] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _title_lb.text = model.giftDesc;
    _date_lb.text = [model.createTime stringByAppendingString:@" 兑换"];
    _coins_lb.text = [model.payAmount.stringValue stringByAppendingString:@"金币"];
}

@end
