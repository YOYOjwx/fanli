//
//  WordsCell.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "WordsCell.h"

@interface WordsCell ()

/** cell详情 */
@property (nonatomic, strong) UILabel *detail_lb;

@end

@implementation WordsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        WS(weakSelf);
        /** 创建cell详情 */
        self.detail_lb = [UILabel new];
        [self.contentView addSubview:self.detail_lb];
        self.detail_lb.textAlignment = NSTextAlignmentRight;
        self.detail_lb.font = CurrencyFont(14);
        self.detail_lb.textColor = UIColorFromHex(0x999999);
        [self.detail_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(weakSelf.title_lb);
            make.trailing.equalTo(weakSelf.arrow_image.mas_leading).offset(-5);
            make.leading.equalTo(weakSelf.title_lb.mas_trailing);
        }];
    }
    return self;
}

- (void)setModel:(SetCellModel *)model{
    [super setModel:model];
    self.detail_lb.text = model.detail;
    if (model.setType == SetTypeDone) {
        self.arrow_image.hidden = YES;
    }else{
        self.arrow_image.hidden = NO;
    }
}

@end
