//
//  YUTextField.h
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YUTextField : UIView

/** 标题 */
@property (nonatomic, strong) NSString *title;
/** textField */
@property (nonatomic, strong) UITextField *textField;

@end
