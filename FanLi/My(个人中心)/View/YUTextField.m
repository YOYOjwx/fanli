//
//  YUTextField.m
//  FanLi
//
//  Created by 费猫 on 2017/5/26.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUTextField.h"

@interface YUTextField ()

@property (nonatomic, strong) UILabel *name_lb;


@end

@implementation YUTextField

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        self.layer.borderWidth = 0.5;
        self.layer.borderColor = UIColorFromHex(0x999999).CGColor;
        self.name_lb = [UILabel new];
        [self addSubview:self.name_lb];
        self.name_lb.font = CurrencyFont(14);
        [self.name_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.leading.equalTo(@10);
            make.width.equalTo(@70);
        }];
        self.textField = [UITextField new];
        [self addSubview:self.textField];
        self.textField.font = CurrencyFont(14);
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_name_lb.mas_trailing).offset(5);
            make.trailing.equalTo(@-10);
            make.top.bottom.equalTo(@0);
        }];
        UIView *gray_view = [UIView new];
        [self addSubview:gray_view];
        gray_view.backgroundColor = UIColorFromHex(0x999999);
        gray_view.layer.masksToBounds = YES;
        gray_view.layer.cornerRadius = 4;
        [gray_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(@0);
            make.centerX.equalTo(weakSelf.mas_leading).offset(-15);
            make.width.height.equalTo(@8);
        }];
        
    }
    return self;
}

- (void)setTitle:(NSString *)title{
    _title = title;
    _name_lb.text = [title stringByAppendingString:@":"];
}

@end
