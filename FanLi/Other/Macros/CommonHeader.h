//
//  CommonHeader.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/10.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#ifndef CommonHeader_h
#define CommonHeader_h

#import "User.h"
#import "YUUpload.h"

//#define FainLi /* 注销掉了为省钱大王 */

#ifdef APPENDAPP
    #define APPNAME @"返利大王"
    #define ChannelId @"AppStore"
#else
    #define ChannelId @"AppStore2"
    #define APPNAME @"省钱大王"
#endif

#endif /* CommonHeader_h */
