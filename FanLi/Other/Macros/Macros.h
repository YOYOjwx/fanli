//
//  Macros.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/14.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

/** 常用的颜色和高度 */
#define kMainWidth [UIScreen mainScreen].bounds.size.width
#define kMainHeight [UIScreen mainScreen].bounds.size.height
#define RGBCOLOR(r,g,b,a) [UIColor colorWithRed:(r)/255.0f green:(g)/255.0f blue:(b)/255.0f alpha:a]
#define UIColorFromHexWithAlpha(hexValue,a) [UIColor colorWithRed:((float)((hexValue & 0xFF0000) >> 16))/255.0 green:((float)((hexValue & 0xFF00) >> 8))/255.0 blue:((float)(hexValue & 0xFF))/255.0 alpha:a]
#define UIColorFromHex(hexValue) UIColorFromHexWithAlpha(hexValue,1.0)

/** block */
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self
/** 通用字体 */
#define CurrencyFont(s) [UIFont systemFontOfSize:(s)]
#define UserDefaults [NSUserDefaults standardUserDefaults]
/** 是否开启引导图 */
#define FIRST_ENTRY [UserDefaults boolForKey:FIRST_ENTRY_KEY]
#define FIRST_ENTRY_KEY @"FIRST_ENTRY"
/** 是否已经登录 */
#define IS_LOGIN [UserDefaults boolForKey:@"IS_LOGIN"]
/** 邀请人UID */
#define INVITE_UID [UserDefaults stringForKey:@"inviteUid"]
/** 保存唯一DEVICEID */
#define DEVICE_ID [UserDefaults stringForKey:@"deviceId"]
/** 用户UID */
#define UD_UID [UserDefaults stringForKey:@"uid"]
/** 用户token */
#define UD_TOKEN [UserDefaults stringForKey:@"token"]

#endif /* Macros_h */
