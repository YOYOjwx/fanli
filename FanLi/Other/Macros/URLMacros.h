//
//  URLMacros.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/4/28.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#define YOYO_KEY      @"084d5d97d935eb4be6de48230824dc33289194ece28525e3d9577e6a80fc94be993f608434738fb0db2be323138cb4f2288b42cdabe30408a03231a271e2ff15"
/** 金币说明 */
#define CoinCenterURL @"http://www.izhequ.com/h5/activity/2/2666263d-5066-459b-8db0-5971d0f10bb1.html"
/** 帮助中心 */
#define HelpCenterURL @"http://www.yoyo360.cn/html/126.html"
// 联系客服
#define DWConcactURL @"http://www.yoyo360.cn/dd/dwconcact.htm"
/** 查看教程 */
#define SeeCourseURL  @"http://www.izhequ.com/h5/activity/2/6218cc9a-e97c-48b4-a77e-f63980b3c256.html"
/** 集分宝怎么用 */
#define JiFenBaoURL @"http://www.yoyo360.cn/doc/jifenbao.html"
/** 下单任务详情地址 */
#define OrderDetailURL [NSString stringWithFormat:@"http://fanlitao-1.wx.jaeapp.com/goldTask/index?uid=%@&token=%@",UD_UID,UD_TOKEN]
/** 下单任务进度地址 */
#define OrderSchedulelURL [NSString stringWithFormat:@"http://fanlitao-1.wx.jaeapp.com/goldTask/part?uid=%@&token=%@",UD_UID,UD_TOKEN]
/** 下单任务规则 */
#define OrderTaskRuleURL @"http://www.izhequ.com/h5/activity/2/4c060f8b-98b9-46f9-bd10-1444f6a70e2d.html"

#define X_VER 637 //内部版本号 ，每次新版本，加1 !

#define HostURL @"http://api.yoyo360.cn/rest/"

#define MarketURL @"http://fanli.wx.jaeapp.com/"

#define BasePHPURL @"http://www.yoyo360.cn/"
