//
//  TaobaoDetail.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AlibcTradeSDK/AlibcTradeSDK.h>
#import "CateItemModel.h"

@interface TaobaoDetail : NSObject

/** 打开商品详情 */
+ (void)showTaobaoDetail:(UIViewController *)controller itemID:(NSString *)itemId pid:(NSString *)pid success:(void(^)(AlibcTradeResult *result))success
failure:(void(^)(NSError *error))failure;

/** 打开购物车 */
+ (void)openShoppingCart:(UIViewController *)controller success:(void(^)(AlibcTradeResult *result))success
failure:(void(^)(NSError *error))failure;

/** 根据cateItem类型打开不同页面 */
+ (void)openCycleDetail:(UIViewController *)controller cateItem:(CateItemModel *)cateItem;

@end
