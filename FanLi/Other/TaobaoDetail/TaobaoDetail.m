//
//  TaobaoDetail.m
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "TaobaoDetail.h"
#import "NSString+Common.h"
#import "LoginController.h"
#import "YUWebController.h"
#import "ExchangeSuccessController.h"

@implementation TaobaoDetail

/** 打开商品详情
 @param controller 跳转淘宝详情的控制器
 @param itemId 商品id
 @param pid 如果是淘客商品，传淘客pid
 @param success 成功回调
 @param failure 失败回调
 */
+ (void)showTaobaoDetail:(UIViewController *)controller itemID:(NSString *)itemId pid:(NSString *)pid success:(void(^)(AlibcTradeResult *result))success
failure:(void(^)(NSError *error))failure{
    AlibcTradeTaokeParams *taokeParams = nil;
    if (pid.length) {
        taokeParams = [[AlibcTradeTaokeParams alloc] init];
        taokeParams.pid = pid;
    }
    id <AlibcTradePage> page = nil;
    if ([itemId judgeURLSource:@"http"]) {
        page = [AlibcTradePageFactory page:itemId];
    }else{
        page = [AlibcTradePageFactory itemDetailPage:itemId];
    }
    [self openTaobao:controller page:page taoKeParams:taokeParams trackParam:nil success:success failure:failure];
}

/** 打开购物车
 @param controller 跳转淘宝详情的控制器
 @param success 成功回调
 @param failure 失败回调
 */
+ (void)openShoppingCart:(UIViewController *)controller success:(void(^)(AlibcTradeResult *result))success
failure:(void(^)(NSError *error))failure{
    id <AlibcTradePage> page = [AlibcTradePageFactory myCartsPage];
    [self openTaobao:controller page:page taoKeParams:nil trackParam:nil success:success failure:failure];
}

/** 打开商品详情，购物车和商品详情公用一个回调 */
+ (void)openTaobao:(UIViewController *)controller
                  page:(id <AlibcTradePage>)page
           taoKeParams:(AlibcTradeTaokeParams *)taoKeParams
            trackParam:(NSDictionary *)trackParam
            success:(void (^)(AlibcTradeResult *result))success
            failure:(void (^)(NSError *error))failure{
    AlibcTradeShowParams *showParams = [[AlibcTradeShowParams alloc] init];
    showParams.openType = AlibcOpenTypeH5;
    //如果控制器是UINavigationController,那么就进行跳转
    if ([controller isKindOfClass:[UINavigationController class]]) {
        showParams.isNeedPush = YES;
    }
    [LoginController isLoginStatus:^(User *user) {
        [[[AlibcTradeSDK sharedInstance] tradeService] show:controller page:page showParams:showParams taoKeParams:taoKeParams trackParam:trackParam tradeProcessSuccessCallback:^(AlibcTradeResult * _Nullable result) {
            /** 订单编号 */
            NSString *orderIds = [result.payResult.paySuccessOrders componentsJoinedByString:@","];
            NSString *paraStr = nil;
            NSDictionary *dic = nil;
            if (taoKeParams) {//如果有值
                paraStr = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&%@=%@&key=%@",@"orderIds",orderIds,@"pid",taoKeParams.pid,@"token",UD_TOKEN,@"uid",UD_UID,YOYO_KEY];
                dic = @{@"orderIds":orderIds,@"pid":taoKeParams.pid};
            }else{
                paraStr = [NSString stringWithFormat:@"%@=%@&%@=%@&%@=%@&key=%@",@"orderIds",orderIds,@"token",UD_TOKEN,@"uid",UD_UID,YOYO_KEY];
                dic = @{@"orderIds":orderIds};
            }
            NSString *sign= [[paraStr stringWithMD5] uppercaseString];
            YURequest *request = [YURequest creatMarketRequest];
            [request setValue:sign forHTTPHeaderField:@"uuToken2"];
            [request POST:@"openApi/order/callback" parameters:dic success:^(id JSON) {
                if (success) {
                    success(result);
                }
            } failure:nil];
        } tradeProcessFailedCallback:failure];
    }];
}

+ (void)openCycleDetail:(UIViewController *)controller cateItem:(CateItemModel *)cateItem{
    switch (cateItem.clickType) {
        case ClickTypeWebView:{
            YUWebController *web = [[YUWebController alloc] init];
            web.url = [NSURL URLWithString:cateItem.clickUrl];
            web.title_string = @"商品详情";
            web.hidesBottomBarWhenPushed = YES;
            if ([controller isKindOfClass:[UINavigationController class]]) {
                [(UINavigationController *)controller pushViewController:web animated:YES];
            }else{
                [controller presentViewController:web animated:YES completion:nil];
            }
            break;
        }
        case ClickTypeTAESdk:{
            [self showTaobaoDetail:controller itemID:cateItem.tbItemId pid:cateItem.taobaoPid success:^(AlibcTradeResult *result) {
                ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
                esc.navigation_title = @"支付成功";
                esc.success_title = @"购买商品支付成功";
                esc.btn_name = @"查看订单";
                esc.hidesBottomBarWhenPushed = YES;
                esc.pushController = @"TaobaoOrderController";
                if ([controller isKindOfClass:[UINavigationController class]]) {
                    [(UINavigationController *)controller pushViewController:esc animated:YES];
                }else{
                    [controller presentViewController:esc animated:YES completion:nil];
                }
            } failure:nil];
            break;
        }
        default:break;
    }
}

@end
