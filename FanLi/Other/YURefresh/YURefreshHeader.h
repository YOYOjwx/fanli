//
//  YURefreshHeader.h
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MJRefreshHeader.h"

/** 对MJRefresh进行了一些封装，做到自己要实现的效果 */
@interface YURefreshHeader : MJRefreshHeader

@end
