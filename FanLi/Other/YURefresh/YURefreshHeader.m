//
//  YURefreshHeader.m
//  FanLi
//
//  Created by 费猫 on 2017/5/19.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YURefreshHeader.h"
#import "YUCircle.h"

@interface YURefreshHeader ()<UIScrollViewDelegate>

@property (nonatomic, strong) YUCircle *circle;

@property (nonatomic, strong) UIImageView *refreshFinish;

@property (nonatomic, assign) float duration;

@end

@implementation YURefreshHeader

+ (instancetype)headerWithRefreshingBlock:(MJRefreshComponentRefreshingBlock)refreshingBlock{
    YURefreshHeader *yu = [super headerWithRefreshingBlock:refreshingBlock];
    /** 创建子视图 */
    yu.duration = 2.0;
    [yu creatSubViews];
    return yu;
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 创建加载过程中的圆圈 */
    self.circle = [YUCircle new];
    [self addSubview:self.circle];
    [self.circle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
        make.width.height.equalTo(@25);
    }];
    /** 创建刷新完成的图片 */
    self.refreshFinish = [UIImageView new];
    [self addSubview:self.refreshFinish];
    self.refreshFinish.hidden = YES;
    self.refreshFinish.image = [UIImage imageNamed:@"刷新图标"];
    [self.refreshFinish mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(@0);
        make.width.height.equalTo(@25);
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    if ([keyPath isEqualToString:@"contentOffset"]) {
        UIScrollView *scrollView = (UIScrollView *)object;
        CGFloat y = -scrollView.contentOffset.y;
        if (!self.isRefreshing) {
            if (y > 66) y = 66;
            if (y < 0) y = 0;
            self.circle.progress = y / 66.f;
            [self.circle setNeedsDisplay];
        }
    }
}

- (void)beginRefreshing{
    [super beginRefreshing];
    self.circle.progress = 1.0f;
    self.refreshFinish.hidden = YES;
    [UIView animateWithDuration:1 animations:^{
        self.circle.transform = CGAffineTransformMakeRotation(_duration);
    }];
    [self performSelector:@selector(animation) withObject:nil afterDelay:0.5];
}

- (void)endRefreshing{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(animation) object:nil];
    self.circle.transform = CGAffineTransformMakeRotation(M_PI * 2);
    self.circle.progress = 0.f;
    self.duration = 2.0;
    self.refreshFinish.hidden = NO;
    [super endRefreshing];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.refreshFinish.hidden = YES;
    });
}

- (void)animation{
    _duration += 2;
    [UIView animateWithDuration:1 animations:^{
        self.circle.transform = CGAffineTransformMakeRotation(_duration);
    }];
    [self performSelector:@selector(animation) withObject:nil afterDelay:0.5];
}


@end
