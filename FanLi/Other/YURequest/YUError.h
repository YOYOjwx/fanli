//
//  YUError.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM (NSInteger, YUErrorCode) {
    YUServersNetworking = -400,
    YUTokenCheckFault = -403,
    YUServersDatabase = -500,
    YUParameter = -501,
    YUServersIO = -502,
    YUDatebaseExistSameRecords = -503,
    YUServerUnknow = -505,
    YUBabyPhotoInviteCode=-600,
    YUReplyNotFound = -1001,
    YUUserNotFound = -1002,
    YUGoldNotEnough = -1003,
    YUAnswerCannotDelete = -1004,
    YUQuestionCannotDelete = -1005,
    YUQuestionNotFound = -1006,
    YURepeatAppreciate = -1007,
    YUAdoptAnswerRefuse = -1008,
    YUExistAdoptedAnswer = -1009,
    YUUsedEmail = -1010,
    YULoginPassword = -1011,
    YUPhoneForbidden = -1012,
    YUHaveCheckedToday = -1013,
    YUTopicNotFound= -1020,
    YUaAcceptSameMachine = -1028,
    YUaAcceptSelf = -1029,
    YUPhoneRegisted = -1031,
    YUValidCode = -1032,
    YUValidMax = -1033,
    YUYCoinMin = -1041,
    YUNoTryItem = -1060,
    YUCommodityNoStoke = -1068,
    YUCommodityOutOfBuyLimit = -1069,
    YUShopOrderNotAllowApply = -1063,
    YUShopOrderNotAllowCanleApply = -1080,
    YULiveRoomNoLiveHostRight = -1091,
    YULiveRoomNameSame =- 1092,
    YULiveRoomNotExit =- 1094,
    YUNoRightUpdateBabyPhotoRelation = -1200,
    YUNoRelationWithBaby = -1202,
    YUExistRelationWithDadAndMom = -1203,
    YUPhoneNumberRepeatBind = -1073,
    YUAlipayAccountRepeatBind = -1035,
    YUTodayHasBeenShared = -1051
};

@interface YUError : NSObject

/** 根据错误代码返回错误信息 */
+ (NSString *)descriptionForCode:(YUErrorCode)code msg:(NSString *)msg;

@end
