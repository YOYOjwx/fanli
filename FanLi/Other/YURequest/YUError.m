//
//  YUError.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/9.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YUError.h"
#import "LoginController.h"

@implementation YUError

+ (NSString *)descriptionForCode:(YUErrorCode)code msg:(NSString *)msg{
    switch (code) {
        case YUServersNetworking:return @"服务器网络故障";
        case YUTokenCheckFault:[self cleanUser];return @"帐号验证失败";
        case YUServersDatabase:return @"服务器数据库错误";
        case YUParameter:return @"参数错误";
        case YUServersIO:return @"服务器IO错误";
        case YUDatebaseExistSameRecords:return @"数据库中存在重复记录";
        case YUServerUnknow:return @"服务器未知错误";
        case YUBabyPhotoInviteCode:return @"验证码验证失败";
        case YUReplyNotFound:return @"回复已经被删除";
        case YUUserNotFound:return @"用户不存在";
        case YUGoldNotEnough:return @"金币不足，请充值";
        case YUAnswerCannotDelete:return @"不能删除别人的回复";
        case YUQuestionCannotDelete:return @"不能删除别人的提问";
        case YUQuestionNotFound:return @"提问已经被删除";
        case YURepeatAppreciate:return @"您已赞过此回复";
        case YUAdoptAnswerRefuse:return @"提问者不是您，无法采纳";
        case YUExistAdoptedAnswer:return @"该问题已经有被采纳的答案";
        case YUUsedEmail:return @"该email已经被注册";
        case YULoginPassword:return @"您的登录密码错误";
        case YUPhoneForbidden:return @"设备被禁用";
        case YUHaveCheckedToday:return @"今天您已经签到过了";
        case YUTopicNotFound:return @"帖子已删除";
        case YUaAcceptSelf:return @"自已不能采纳自己";
        case YUaAcceptSameMachine:return @"同一台手机不能采纳";
        case YUPhoneRegisted:return @"该手机号码已经注册过";
        case YUValidCode:return @"验证码错误";
        case YUValidMax:return @"获取验证码次数过多";
        case YUYCoinMin:return @"柚币小于最少兑换量";
        case YUNoTryItem:return @"试用商品没找到";
        case YUCommodityNoStoke:return @"服务商品库存不足";
        case YUCommodityOutOfBuyLimit:return @"超出限购数量";
        case YUShopOrderNotAllowApply:return @"此订单不允许退款";
        case YUShopOrderNotAllowCanleApply:return @"此订单不允许取消退款申请";
        case YULiveRoomNoLiveHostRight:return @"没有主播权限";
        case YULiveRoomNameSame:return @"直播间名称重复";
        case YULiveRoomNotExit:return @"直播间不存在或直播已结束";
        case YUNoRightUpdateBabyPhotoRelation:return @"没有修改权限";
        case YUNoRelationWithBaby:return @"与宝宝不存在关系";
        case YUExistRelationWithDadAndMom:return @"已存在爸爸妈妈关系";
        case YUPhoneNumberRepeatBind:return @"该手机号已经被绑定";
        case YUAlipayAccountRepeatBind:return @"该支付宝账号已经被绑定";
        case YUTodayHasBeenShared:return @"今日已分享订单";
        default:return [NSString stringWithFormat:@"%@code:%ld:",msg,(long)code];
    }
}

/** 清除用户数据 */
+ (void)cleanUser{
    /** 清除登录状态 */
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"IS_LOGIN"];
    /** 清除用户信息 */
    [[User shareUser] logout];
    /** 让用户继续登录 */
    [LoginController goToLoginSuccess:^(User *user) {
        
    }];
}

@end
