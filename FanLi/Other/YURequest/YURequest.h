//
//  YURequest.h
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/8.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface YURequest : NSMutableURLRequest

/** 创建基础网络请求 */
+ (instancetype)creatBaseRequest;
/** 创建商城网络请求 */
+ (instancetype)creatMarketRequest;
/** 根据URL创建网络请求 */
+ (instancetype)creatRequestWith:(NSString *)url;
/** 解析数据,用于服务所有子类 */
- (void)dataAnalysis:(NSData *)data error:(NSError *)error success:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** POST请求 */
- (void)POST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;
/** GET请求 */
- (void)GET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;

@end
