//
//  YURequest.m
//  YOYO6.0
//
//  Created by 雨天记忆 on 2017/5/8.
//  Copyright © 2017年 雨天记忆. All rights reserved.
//

#import "YURequest.h"
#import "YUError.h"
#import <CommonCrypto/CommonDigest.h>

@implementation YURequest

+ (instancetype)creatBaseRequest{
    YURequest *request = [super requestWithURL:[NSURL URLWithString:HostURL]];
    /** 设置请求头 */
    [request setNetworkRequestHeader];
    return request;
}

+ (instancetype)creatMarketRequest{
    YURequest *request = [super requestWithURL:[NSURL URLWithString:MarketURL]];
    /** 设置请求头 */
    [request setNetworkRequestHeader];
    return request;
}

+ (instancetype)creatRequestWith:(NSString *)url{
    YURequest *request = [super requestWithURL:[NSURL URLWithString:url]];
    return request;
}

/** 设置网络请求头 */
- (void)setNetworkRequestHeader{
    [self setValue:ChannelId forHTTPHeaderField:@"channel"];
    [self setValue:@"iphone" forHTTPHeaderField:@"device"];
    [self setValue:[NSBundle mainBundle].infoDictionary[@"CFBundleShortVersionString"] forHTTPHeaderField:@"ver"];
    [self setValue:@"2" forHTTPHeaderField:@"X-APPID"];//来源，1:柚柚、2:返利大王
    [self setValue:@(X_VER).stringValue forHTTPHeaderField:@"X-VER"];
    [self setValue:DEVICE_ID.length?DEVICE_ID:[NSUUID UUID].UUIDString forHTTPHeaderField:@"deviceId"];
    [self setHeaderOfuuToken];
}

/** 设置uuToken */
-(void)setHeaderOfuuToken{
    NSString *timeString = [NSString stringWithFormat:@"%0.f",[[NSDate date] timeIntervalSince1970] * 1000];
    NSString* uuidString = UD_UID.length?UD_UID:@"0";
    NSString* devieceId = DEVICE_ID.length?DEVICE_ID:@"0";
    NSString* keyString = YOYO_KEY;
    NSString* headString = [NSString stringWithFormat:@"%@%@%@%@",uuidString,devieceId,timeString,keyString];
    const char *cStr = [headString UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSString* md5String =  [NSString stringWithFormat:@"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3],result[4], result[5], result[6], result[7],result[8], result[9], result[10], result[11],result[12], result[13], result[14], result[15]];
    md5String = [md5String lowercaseString];
    [self setValue:timeString forHTTPHeaderField:@"clientTime"];
    [self setValue:md5String forHTTPHeaderField:@"uuToken"];
}

/** POST请求 */
- (void)POST:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void (^)(id))success failure:(void (^)(NSError *, NSString *, NSInteger))failure{
    self.HTTPMethod = @"POST";
    self.URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",self.URL.absoluteString,URLString]];
    NSString *param = [self getRequestParameters:parameters];
    self.HTTPBody = [param dataUsingEncoding:NSUTF8StringEncoding];
    [self netWorkRequestSuccess:success failure:failure];
}

/** GET请求 */
- (void)GET:(NSString *)URLString parameters:(NSDictionary *)parameters success:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    self.HTTPMethod = @"GET";
    NSString *requestURL = [NSString stringWithFormat:@"%@%@?",self.URL.absoluteString,URLString];
    self.URL = [NSURL URLWithString:[requestURL stringByAppendingString:[self getRequestParameters:parameters]]];
    [self netWorkRequestSuccess:success failure:failure];
}

/** 获取请求参数 */
- (NSString *)getRequestParameters:(NSDictionary *)parameters{
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:parameters];
    if (!dic[@"token"]){
        [dic setValue:UD_TOKEN forKey:@"token"];
    }
    if (!dic[@"uid"]){
        [dic setValue:UD_UID forKey:@"uid"];
    }
    NSString *param = @"";
    NSArray *keys = dic.allKeys;
    for (NSString *key in keys) {
        /** 对key和value进行拼接 */
        NSString *kav = [NSString stringWithFormat:@"%@=%@&",key,dic[key]];
        param = [param stringByAppendingString:kav];
    }
    return [param substringToIndex:param.length - 1];
}


/** 网络请求 */
- (void)netWorkRequestSuccess:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:self completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        /** 数据解析 */
        [self dataAnalysis:data error:error success:success failure:failure];
    }] resume];
}

/** 数据请求处理 */
- (void)dataAnalysis:(NSData *)data error:(NSError *)error success:(void(^)(id JSON))success failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure{
    if (error || data == nil) {
        if (failure) {
            failure(error,@"网络错误",error.code);
        }
        [self showError:@"网络错误"];
    }else{
        id JSON = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSInteger code = [JSON[@"code"] integerValue];
        if (code == 0) {
            if (success) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    id result = JSON[@"result"];
                    success(result?result:JSON);
                });
            }
        }else{
            if (failure) {
                failure(nil,JSON[@"errorMsg"],code);
            }
            [self showError:[YUError descriptionForCode:code msg:JSON[@"errorMsg"]]];
        }
    }
    
}

- (void)showError:(NSString *)error{
    [SVProgressHUD showErrorWithStatus:error];
}

@end
