//
//  YUUpload.h
//  FanLi
//
//  Created by 费猫 on 2017/5/18.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YURequest.h"

@interface YUUpload : YURequest

- (void)upload:(NSString *)URLString parameters:(NSDictionary *)parameters data:(NSData *)data fileKey:(NSString *)fileKey fileName:(NSString *)fileName success:(void(^)(id JSON))success progress:(void(^)(float progress))progress failure:(void(^)(NSError *error,NSString *errorMsg, NSInteger code))failure;

@end
