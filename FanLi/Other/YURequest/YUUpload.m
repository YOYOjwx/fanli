//
//  YUUpload.m
//  FanLi
//
//  Created by 费猫 on 2017/5/18.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUUpload.h"

@interface YUUpload ()<NSURLSessionDelegate>

@property (nonatomic, copy) void (^progress)(float progress);

@end

@implementation YUUpload

- (void)upload:(NSString *)URLString parameters:(NSDictionary *)parameters data:(NSData *)data fileKey:(NSString *)fileKey fileName:(NSString *)fileName success:(void (^)(id))success progress:(void (^)(float))progress failure:(void (^)(NSError *, NSString *, NSInteger))failure{
    self.URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",self.URL.absoluteString,URLString]];
    NSString *TWITTERFON_FORM_BOUNDARY = @"0xKhTmLbOuNdArY";
    //根据url初始化request
    //分界线 --AaB03x
    NSString *MPboundary=[[NSString alloc]initWithFormat:@"--%@",TWITTERFON_FORM_BOUNDARY];
    //结束符 AaB03x--
    NSString *endMPboundary = [[NSString alloc]initWithFormat:@"%@--",MPboundary];
    //得到图片的data
    //http body的字符串
    NSMutableString *body = [[NSMutableString alloc]init];
    //参数的集合的所有key的集合
    NSArray *keys = [parameters allKeys];
    
    for (NSString *key in keys) {
        //添加分界线，换行
        [body appendFormat:@"%@\r\n",MPboundary];
        //添加字段名称，换2行
        [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n",key];
        //添加字段的值
        [body appendFormat:@"%@\r\n",[parameters objectForKey:key]];
    }
    ////添加分界线，换行
    [body appendFormat:@"%@\r\n",MPboundary];
    //声明pic字段，文件名为boris.png
    [body appendFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"%@\"\r\n",fileKey,fileName];
    //声明上传文件的格式
    [body appendFormat:@"Content-Type: image/jpge,image/gif, image/jpeg, image/pjpeg, image/pjpeg\r\n\r\n"];
    
    //声明结束符：--AaB03x--
    NSString *end=[[NSString alloc]initWithFormat:@"\r\n%@",endMPboundary];
    //声明myRequestData，用来放入http body
    NSMutableData *myRequestData = [NSMutableData data];
    
    //将body字符串转化为UTF8格式的二进制
    [myRequestData appendData:[body dataUsingEncoding:NSUTF8StringEncoding]];
    if(data){
        //将image的data加入
        [myRequestData appendData:data];
    }
    //加入结束符--AaB03x--
    [myRequestData appendData:[end dataUsingEncoding:NSUTF8StringEncoding]];
    
    //设置HTTPHeader中Content-Type的值
    NSString *content=[[NSString alloc]initWithFormat:@"multipart/form-data; boundary=%@",TWITTERFON_FORM_BOUNDARY];
    //设置HTTPHeader
    [self setValue:content forHTTPHeaderField:@"Content-Type"];
    //设置Content-Length
    [self setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[myRequestData length]] forHTTPHeaderField:@"Content-Length"];
    //设置http body
    [self setHTTPBody:myRequestData];
    //http method
    [self setHTTPMethod:@"POST"];
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration ephemeralSessionConfiguration] delegate:self delegateQueue:[NSOperationQueue currentQueue]];
    [[session uploadTaskWithRequest:self fromData:nil completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        [SVProgressHUD dismiss];
        [self dataAnalysis:data error:error success:success failure:failure];
    }] resume];
    self.progress = progress;
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task didSendBodyData:(int64_t)bytesSent totalBytesSent:(int64_t)totalBytesSent totalBytesExpectedToSend:(int64_t)totalBytesExpectedToSend{
    float progress = (float)totalBytesSent / totalBytesExpectedToSend;
    if (self.progress) {
        self.progress(progress * 100.0);
    }
}

@end
