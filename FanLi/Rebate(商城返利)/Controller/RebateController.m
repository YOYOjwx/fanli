//
//  RebateController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "RebateController.h"
#import "YUSegmented.h"
#import "MallCateModel.h"
#import "RebateCell.h"
#import "YUWebController.h"

@interface RebateController ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) YUSegmented *segmented;

@property (nonatomic, strong) NSMutableArray *menusAry;

@property (nonatomic, strong) UICollectionView *collectionView;

@property (nonatomic, strong) NSString *rebateDetailUrl;

@end

@implementation RebateController

- (NSMutableArray *)menusAry{
    if (!_menusAry) {
        _menusAry = [NSMutableArray array];
    }
    return _menusAry;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"返利商城";
    
    [self getMenusData];
    
    [self creatSubViews];
    
    UIBarButtonItem *helpItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"help"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(help)];
    self.navigationItem.rightBarButtonItem = helpItem;
}

- (void)help{
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:@"http://www.izhequ.com/h5/activity/2/06637d92-b396-4642-91e5-4138f5960a5d.html"];
    web.title_string = @"商城返利常见问题";
    web.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:web animated:YES];
}

- (void)creatSubViews{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 0;
    layout.minimumInteritemSpacing = 0;
    layout.sectionInset = UIEdgeInsetsMake(5, 0, 0, 0);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(kMainWidth, kMainHeight - 40 - 49 - 64);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
    self.collectionView.backgroundColor = [UIColor clearColor];
    self.collectionView.pagingEnabled = YES;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@35);
        make.leading.trailing.bottom.equalTo(@0);
    }];
    /** 注册cell */
    [self.collectionView registerClass:[RebateCell class] forCellWithReuseIdentifier:@"RebateCell"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.menusAry.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RebateCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RebateCell" forIndexPath:indexPath];
    MallCateModel *model = self.menusAry[indexPath.item];
    cell.cateId = model.seqId.stringValue;
    cell.click = ^(MallInfoModel *model) {
        [self pushThirdPartyRebateWith:model];
    };
    return cell;
}

- (void)pushThirdPartyRebateWith:(MallInfoModel *)model{
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:model.mallUrl];
    web.title_string = model.name;
    web.hidesBottomBarWhenPushed = YES;
    UIBarButtonItem *detailItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"商城细则"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(rebateDetail)];
    web.rightBarButtonItems = @[detailItem];
    self.rebateDetailUrl = model.detailUrl;
    [self.navigationController pushViewController:web animated:YES];
}

- (void)rebateDetail{
    YUWebController *rebateWeb = self.navigationController.childViewControllers.lastObject;
    YUWebController *web = [[YUWebController alloc] init];
    web.url = [NSURL URLWithString:self.rebateDetailUrl];
    web.title_string = @"商城细则";
    [rebateWeb.navigationController pushViewController:web animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    CGFloat x = scrollView.contentOffset.x;
    self.segmented.selectIndex = x / kMainWidth;
}

- (void)getMenusData{
    [[YURequest creatMarketRequest] POST:@"openApi/mall/getCateList" parameters:nil success:^(id JSON) {
        NSMutableArray *tmpAry = [NSMutableArray array];
        for (NSDictionary *dic in JSON) {
            MallCateModel *model = [[MallCateModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.menusAry addObject:model];
            [tmpAry addObject:model.cateName];
        }
        [self.collectionView reloadData];
        self.segmented = [[YUSegmented alloc] initWithArray:tmpAry column:3 click:^(NSInteger index) {
            [self.collectionView setContentOffset:CGPointMake(index * kMainWidth, 0) animated:YES];
        }];
        [self.view addSubview:self.segmented];
        [self.segmented mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(@0.5);
            make.leading.trailing.equalTo(@0);
            make.height.equalTo(@35);
        }];
    } failure:nil];
}


@end
