//
//  MallCateModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/31.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface MallCateModel : BaseModel

/** 商城编号 */
@property (nonatomic, strong) NSNumber *seqId;
/** 商城名字 */
@property (nonatomic, strong) NSString *cateName;
/** 排序编号 */
@property (nonatomic, strong) NSNumber *sortNo;

@end
