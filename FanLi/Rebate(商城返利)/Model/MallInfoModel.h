//
//  MallInfoModel.h
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

@interface MallInfoModel : BaseModel

/** 商城编号 */
@property (nonatomic, strong) NSNumber *seqId;
/** 商城名字 */
@property (nonatomic, strong) NSString *name;
/** 商城分类编号 */
@property (nonatomic, strong) NSNumber *cateId;
/** 排序编号 */
@property (nonatomic, assign) int sortNo;
/** 图片地址 */
@property (nonatomic, strong) NSString *pic;
/** 提醒信息 */
@property (nonatomic, strong) NSString *tips;
/** 详细介绍地址 */
@property (nonatomic, strong) NSString *detailUrl;
/** 商城入口信息 */
@property (nonatomic, strong) NSString *label;
/** 第三方返利Url */
@property (nonatomic, strong) NSString *mallUrl;

/** 根据seqId获取mallUrl */
- (void)getMallSuccuss:(void(^)(MallInfoModel *model))success;

@end
