//
//  MallInfoModel.m
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "MallInfoModel.h"

@implementation MallInfoModel

- (void)setSeqId:(NSNumber *)seqId{
    _seqId = seqId;
    [self getMallSuccuss:nil];
}

- (void)getMallSuccuss:(void(^)(MallInfoModel *model))success{
    [[YURequest creatMarketRequest] POST:@"openApi/mall/getMallUrl" parameters:@{@"mallId":_seqId} success:^(id JSON) {
        _mallUrl = [NSString stringWithFormat:@"%@",JSON];
        if (success) {
            success(self);
        }
    } failure:nil];
}



@end
