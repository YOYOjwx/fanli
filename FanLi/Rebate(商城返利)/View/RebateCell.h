//
//  RebateCell.h
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallInfoModel.h"

@interface RebateCell : UICollectionViewCell

@property (nonatomic, strong) NSString *cateId;

@property (nonatomic, copy) void (^click)(MallInfoModel *model);

@end
