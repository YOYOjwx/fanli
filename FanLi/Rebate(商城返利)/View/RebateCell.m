//
//  RebateCell.m
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "RebateCell.h"
#import "RebateInfoCell.h"

@interface RebateCell ()<UICollectionViewDelegate,UICollectionViewDataSource>

@property (nonatomic, strong) NSMutableArray *array;

@property (nonatomic, strong) UICollectionView *collectionView;

@end

@implementation RebateCell

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.minimumLineSpacing = 5;
        layout.minimumInteritemSpacing = 5;
        CGFloat width = (frame.size.width - 20) / 3;
        layout.itemSize = CGSizeMake(width, width);
        layout.sectionInset = UIEdgeInsetsMake(0, 5, 0, 5);
        self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        [self.contentView addSubview:self.collectionView];
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        self.collectionView.backgroundColor = [UIColor clearColor];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        /** 注册cell */
        [self.collectionView registerClass:[RebateInfoCell class] forCellWithReuseIdentifier:@"RebateInfoCell"];
    }
    return self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.array.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    RebateInfoCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RebateInfoCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    MallInfoModel *model = self.array[indexPath.item];
    if (model.mallUrl.length) {
        if (self.click) {
            self.click(model);
        }
    }else{
        [model getMallSuccuss:^(MallInfoModel *model) {
            if (self.click) {
                self.click(model);
            }
        }];
    }
}

- (void)setCateId:(NSString *)cateId{
    if ([cateId isEqualToString:_cateId]) {
        [self.collectionView reloadData];
    }else{
        [self.array removeAllObjects];
        [self getRebateListData:cateId];
    }
    _cateId = cateId;
}

- (void)getRebateListData:(NSString *)cateId{
    [[YURequest creatMarketRequest] POST:@"openApi/mall/getMallList" parameters:@{@"cateId":cateId} success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            MallInfoModel *model = [[MallInfoModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        [self.collectionView reloadData];
    } failure:nil];
}

@end
