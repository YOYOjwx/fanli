//
//  RebateInfoCell.h
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MallInfoModel.h"

@interface RebateInfoCell : UICollectionViewCell

@property (nonatomic, strong) MallInfoModel *model;

@end
