//
//  RebateInfoCell.m
//  FanLi
//
//  Created by 费猫 on 2017/6/1.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "RebateInfoCell.h"

@interface RebateInfoCell ()

@property (nonatomic, strong) UIImageView *icon_image;

@property (nonatomic, strong) UILabel *rebate_lb;

@end

@implementation RebateInfoCell

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFit;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
            make.leading.equalTo(@5);
            make.trailing.equalTo(@-5);
        }];
        self.rebate_lb = [UILabel new];
        [self.contentView addSubview:self.rebate_lb];
        self.rebate_lb.font = CurrencyFont(12);
        self.rebate_lb.textColor = UIColorFromHex(0x999999);
        [self.rebate_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.bottom.equalTo(@-8);
        }];
        self.contentView.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

- (void)setModel:(MallInfoModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"默认图片"]];
    _rebate_lb.text = model.label;
}

@end
