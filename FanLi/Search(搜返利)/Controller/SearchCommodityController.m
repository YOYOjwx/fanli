//
//  SearchCommodityController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "SearchCommodityController.h"
#import "UIImage+Common.h"
#import "YUWebController.h"
#import "SearchController.h"

@interface SearchCommodityController ()

@end

@implementation SearchCommodityController

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"搜返利";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    UIScrollView *scrollView = [UIScrollView new];
    [self.view addSubview:scrollView];
    [scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    UIView *contentView = [UIView new];
    [scrollView addSubview:contentView];
    [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.leading.trailing.equalTo(@0);
        make.width.equalTo(scrollView.mas_width);
    }];
    UIImageView *bg_image = [UIImageView new];
    [scrollView addSubview:bg_image];
    bg_image.image = [UIImage imageNamed:@"搜返利背景"];
    [bg_image mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.equalTo(@0);
        make.height.equalTo(bg_image.mas_width).multipliedBy(0.53333);
    }];
    UIView *search_view = [UIView new];
    [scrollView addSubview:search_view];
    search_view.layer.masksToBounds = YES;
    search_view.layer.cornerRadius = 4;
    search_view.backgroundColor = [UIColor whiteColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pushSearchVC)];
    [search_view addGestureRecognizer:tap];
    [search_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@10);
        make.trailing.equalTo(@-10);
        make.height.equalTo(@45);
        make.bottom.equalTo(bg_image).offset(-20);
    }];
    UILabel *text_bg_lb = [UILabel new];
    [search_view addSubview:text_bg_lb];
    text_bg_lb.textColor = [UIColor whiteColor];
    text_bg_lb.text = @"搜返利";
    text_bg_lb.backgroundColor = UIColorFromHex(0xFF5E56);
    text_bg_lb.font = CurrencyFont(13);
    text_bg_lb.textAlignment = NSTextAlignmentCenter;
    text_bg_lb.layer.masksToBounds = YES;
    text_bg_lb.layer.cornerRadius = 4;
    [text_bg_lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.bottom.equalTo(@-5);
        make.top.equalTo(@5);
        make.width.equalTo(@65);
    }];
    UISearchBar *search_bar = [UISearchBar new];
    [search_view addSubview:search_bar];
    search_bar.placeholder = @"复制商品标题，拿返利更方便";
    search_bar.userInteractionEnabled = NO;
    search_bar.backgroundImage = [UIImage imageWithColor:[UIColor whiteColor]];
    [search_bar mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(@10);
        make.top.bottom.equalTo(@0);
        make.trailing.equalTo(text_bg_lb.mas_leading).offset(-20);
    }];
    YUWebController *webVC = [[YUWebController alloc] init];
    webVC.url = [NSURL URLWithString:@"http://www.yoyo360.cn/doc/fanligo_qrcode.html"];
    [self addChildViewController:webVC];
    [scrollView addSubview:webVC.view];
    [webVC.view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(@0);
        make.top.equalTo(bg_image.mas_bottom);
    }];
    /** webView加载完成后执行的方法 */
    __weak __typeof(&*webVC)weakWebVc = webVC;
    webVC.webDidFinish = ^(WKWebView *webView) {
        [webView evaluateJavaScript:@"document.body.clientHeight" completionHandler:^(id _Nullable result, NSError * _Nullable error) {
            [weakWebVc.view mas_updateConstraints:^(MASConstraintMaker *make) {
                make.height.equalTo(result);
            }];
        }];
    };
    [contentView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(webVC.view.mas_bottom);
    }];
}

- (void)pushSearchVC{
    SearchController *sc = [[SearchController alloc] init];
    sc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sc animated:NO];
}

@end
