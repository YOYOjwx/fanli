//
//  SearchController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/31.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"

@interface SearchController : BaseViewController

/** 存储搜索关键词 */
@property (nonatomic, strong) NSString *search_key;

@end
