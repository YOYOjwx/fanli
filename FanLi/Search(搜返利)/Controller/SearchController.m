//
//  SearchController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/31.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "SearchController.h"
#import "UIImage+Common.h"
#import "SearchCell.h"
#import "TaobaoDetail.h"
#import "YURefreshHeader.h"
#import "LoginController.h"
#import "ExchangeSuccessController.h"

@interface SearchController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>

/** tableView */
@property (nonatomic, strong) UITableView *tableView;
/** 热门搜索view */
@property (nonatomic, strong) UIView *hot_search_view;
/** 搜索栏 */
@property (nonatomic, strong) UISearchBar *searchBar;
/** 设置筛选 */
@property (nonatomic, strong) UIView *set_screen_view;
/** 筛选View */
@property (nonatomic, strong) UIView *screen_view;
/** 存储商品的数组 */
@property (nonatomic, strong) NSMutableArray *array;
/** 是否为之搜索天猫 */
@property (nonatomic, assign) BOOL isMall;
/** 价格区间开始的tf */
@property (nonatomic, strong) UITextField *start_price_tf;
/** 价格区间结束的tf */
@property (nonatomic, strong) UITextField *end_price_tf;
/** 排序字段 */
@property (nonatomic, strong) NSString *order;
/** 购物车按钮 */
@property (nonatomic, strong) UIButton *shopping_cart_btn;

@end

@implementation SearchController

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (UIView *)screen_view{
    if (!_screen_view) {
        _screen_view = [[UIView alloc] initWithFrame:CGRectMake(0, 41, kMainWidth, self.view.frame.size.height - 41)];
        _screen_view.backgroundColor = RGBCOLOR(0, 0, 0, 0.4);
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapScreenView)];
        [_screen_view addGestureRecognizer:tap];
        _screen_view.hidden = YES;
        UIView *contentView = [UIView new];
        [_screen_view addSubview:contentView];
        contentView.backgroundColor = UIColorFromHex(0xF0F0F0);
        UITapGestureRecognizer *contentTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:nil];
        [contentView addGestureRecognizer:contentTap];
        UILabel *price_range = [UILabel new];
        [contentView addSubview:price_range];
        price_range.text = @"价格区间(元):";
        price_range.font = CurrencyFont(12);
        price_range.textColor = UIColorFromHex(0x666666);
        [price_range mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@15);
            make.top.equalTo(@20);
        }];
        UIView *start_price_view = [UIView new];
        [contentView addSubview:start_price_view];
        start_price_view.layer.masksToBounds = YES;
        start_price_view.layer.cornerRadius = 3;
        start_price_view.layer.borderColor = UIColorFromHex(0x333333).CGColor;
        start_price_view.layer.borderWidth = 0.5;
        [start_price_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(price_range.mas_trailing).offset(5);
            make.centerY.equalTo(price_range);
            make.width.equalTo(@75);
            make.height.equalTo(@30);
        }];
        self.start_price_tf = [UITextField new];
        [start_price_view addSubview:self.start_price_tf];
        self.start_price_tf.font = CurrencyFont(12);
        self.start_price_tf.keyboardType = UIKeyboardTypeNumberPad;
        [self.start_price_tf mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@5);
            make.trailing.equalTo(@-5);
            make.top.bottom.equalTo(@0);
        }];
        UILabel *fuhao = [UILabel new];
        [contentView addSubview:fuhao];
        fuhao.text = @"~";
        fuhao.textColor = UIColorFromHex(0x666666);
        fuhao.font = CurrencyFont(14);
        [fuhao mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(start_price_view);
            make.leading.equalTo(start_price_view.mas_trailing).offset(10);
        }];
        UIView *end_price_view = [UIView new];
        [contentView addSubview:end_price_view];
        end_price_view.layer.masksToBounds = YES;
        end_price_view.layer.cornerRadius = 3;
        end_price_view.layer.borderColor = UIColorFromHex(0x333333).CGColor;
        end_price_view.layer.borderWidth = 0.5;
        [end_price_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(start_price_view);
            make.leading.equalTo(fuhao.mas_trailing).offset(5);
            make.width.height.equalTo(start_price_view);
        }];
        self.end_price_tf = [UITextField new];
        [end_price_view addSubview:self.end_price_tf];
        self.end_price_tf.keyboardType = UIKeyboardTypeNumberPad;
        self.end_price_tf.font = CurrencyFont(12);
        [self.end_price_tf mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@5);
            make.trailing.equalTo(@-5);
            make.top.bottom.equalTo(@0);
        }];
        UILabel *service_lb = [UILabel new];
        [contentView addSubview:service_lb];
        service_lb.text = @"服务&折扣";
        service_lb.font = CurrencyFont(12);
        service_lb.textColor = UIColorFromHex(0x666666);
        [service_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@15);
            make.top.equalTo(price_range.mas_bottom).offset(30);
        }];
        NSArray *array = @[@"仅天猫店",@"从高到低",@"从低到高"];
        for (int i = 0; i < array.count; i++) {
            CGFloat width = (kMainWidth - 60) / 3;
            CGFloat x = 15 + i * (width + 15);
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(x, 89, width, 30);
            [contentView insertSubview:button atIndex:i];
            [button setTitle:array[i] forState:UIControlStateNormal];
            [button setTitleColor:UIColorFromHex(0x333333) forState:UIControlStateNormal];
            [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
            [button setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forState:UIControlStateNormal];
            [button setBackgroundImage:[UIImage imageWithColor:UIColorFromHex(0xFF5E56)] forState:UIControlStateSelected];
            button.titleLabel.font = CurrencyFont(12);
            button.layer.masksToBounds = YES;
            button.layer.cornerRadius = 3;
            button.layer.borderWidth = 0.5;
            button.tag = i;
            button.layer.borderColor = UIColorFromHex(0x999999).CGColor;
            [button addTarget:self action:@selector(detailedScreening:) forControlEvents:UIControlEventTouchUpInside];
        }
        UIButton *determine_btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [contentView addSubview:determine_btn];
        [determine_btn setTitle:@"确定" forState:UIControlStateNormal];
        [determine_btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        determine_btn.backgroundColor = UIColorFromHex(0xFF5E56);
        determine_btn.layer.masksToBounds = YES;
        determine_btn.layer.cornerRadius = 4;
        determine_btn.titleLabel.font = CurrencyFont(14);
        [determine_btn addTarget:self action:@selector(determine) forControlEvents:UIControlEventTouchUpInside];
        [determine_btn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@15);
            make.trailing.equalTo(@-15);
            make.top.equalTo(service_lb.mas_bottom).offset(60);
            make.height.equalTo(@35);
        }];
        [contentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.trailing.equalTo(@0);
            make.bottom.equalTo(determine_btn.mas_bottom).offset(20);
        }];
    }
    return _screen_view;
}

- (void)determine{
    self.screen_view.hidden = YES;
    [self.view endEditing:YES];
    [self.tableView.mj_header beginRefreshing];
}

- (void)detailedScreening:(UIButton *)btn{
    btn.selected = !btn.selected;
    NSInteger index = btn.tag;
    if (index == 1) {
        if (btn.selected) {
            UIButton *btn = [self.screen_view.subviews[0] subviews][2];
            btn.selected = NO;
            _order = @"price_desc";
        }else{
            _order = nil;
        }
    }else if (index == 2){
        if (btn.selected) {
            UIButton *btn = [self.screen_view.subviews[0] subviews][1];
            btn.selected = NO;
            _order = @"price_asc";
        }else{
            _order = nil;
        }
    }else{
        _isMall = btn.selected;
    }
}

- (void)tapScreenView{
    self.screen_view.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self creatSubViews];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    NSString *pasteStr = [UIPasteboard generalPasteboard].string;
    if (pasteStr.length) {
        self.searchBar.text = pasteStr;
        [self.tableView.mj_header beginRefreshing];
    }
}

- (void)creatSubViews{
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, kMainWidth - 80, 30)];
    self.searchBar.placeholder = @"复制商品标题，拿返利更方便";
    self.searchBar.delegate = self;
    self.searchBar.text = self.search_key;
    self.searchBar.backgroundImage = [UIImage imageWithColor:[UIColor whiteColor]];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.searchBar];
    UIBarButtonItem *cancel_item = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(back)];
    [cancel_item setTintColor:[UIColor grayColor]];
    [cancel_item setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(13)} forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = cancel_item;
    [self getHotSearchData];
    
    self.set_screen_view = [UIView new];
    [self.view addSubview:self.set_screen_view];
    [self.set_screen_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.equalTo(@0);
        make.top.equalTo(@0.5);
        make.height.equalTo(@40);
    }];
    NSArray *array = @[@"综合",@"返利",@"销量",@"筛选 ▾"];
    for (int i = 0; i < array.count; i++) {
        CGFloat width = kMainWidth / 4;
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(i * width, 0, width, 40);
        [button setTitle:array[i] forState:UIControlStateNormal];
        [button setTitleColor:UIColorFromHex(0x666666) forState:UIControlStateNormal];
        [button setTitleColor:UIColorFromHex(0xFF5E56) forState:UIControlStateSelected];
        [button addTarget:self action:@selector(screen:) forControlEvents:UIControlEventTouchUpInside];
        button.titleLabel.font = CurrencyFont(12);
        button.backgroundColor = [UIColor whiteColor];
        UIView *line = [UIView new];
        line.frame = CGRectMake(-0.5, 10, 0.5, 20);
        [button addSubview:line];
        line.backgroundColor = UIColorFromHex(0xF0F0F0);
        if (i == 0) {
            button.selected = YES;
        }
        [self.set_screen_view addSubview:button];
    }
    self.tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = NO;
    self.tableView.rowHeight = 120;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.trailing.bottom.equalTo(@0);
        make.top.equalTo(_set_screen_view.mas_bottom).offset(0.5);
    }];
    /** 注册cell */
    [self.tableView registerClass:[SearchCell class] forCellReuseIdentifier:@"SearchCell"];
    /** 下拉加载刷新 */
    WS(weakSelf);
    self.tableView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        [weakSelf.hot_search_view removeFromSuperview];
        [weakSelf.array removeAllObjects];
        [weakSelf getSearchData];
    }];
    
    if (self.search_key.length) {
        [self.tableView.mj_header beginRefreshing];
    }
    /** 创建购物车 */
    self.shopping_cart_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.view addSubview:self.shopping_cart_btn];
    [self.shopping_cart_btn setTitle:@" 购物车" forState:UIControlStateNormal];
    [self.shopping_cart_btn setImage:[UIImage imageNamed:@"购物车"] forState:UIControlStateNormal];
    self.shopping_cart_btn.titleLabel.font = CurrencyFont(14);
    self.shopping_cart_btn.backgroundColor = RGBCOLOR(0, 0, 0, 0.4);
    self.shopping_cart_btn.layer.masksToBounds = YES;
    self.shopping_cart_btn.layer.cornerRadius = 4;
    [self.shopping_cart_btn addTarget:self action:@selector(shoppingCart) forControlEvents:UIControlEventTouchUpInside];
    [self.shopping_cart_btn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.bottom.equalTo(@-10);
        make.width.equalTo(@80);
        make.height.equalTo(@35);
    }];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    SearchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    CateItemModel *model = self.array[indexPath.row];
    [TaobaoDetail showTaobaoDetail:self.navigationController itemID:model.tbItemId pid:model.taobaoPid success:^(AlibcTradeResult *result) {
        ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
        esc.navigation_title = @"支付成功";
        esc.success_title = @"购买商品支付成功";
        esc.btn_name = @"查看订单";
        esc.hidesBottomBarWhenPushed = YES;
        esc.pushController = @"TaobaoOrderController";
        [self.navigationController pushViewController:esc animated:YES];
    } failure:nil];
}

- (void)shoppingCart{
    [TaobaoDetail openShoppingCart:self success:^(AlibcTradeResult *result) {
        ExchangeSuccessController *esc = [[ExchangeSuccessController alloc] init];
        esc.navigation_title = @"支付成功";
        esc.success_title = @"购买商品支付成功";
        esc.btn_name = @"查看订单";
        esc.hidesBottomBarWhenPushed = YES;
        esc.pushController = @"TaobaoOrderController";
        [self.navigationController pushViewController:esc animated:YES];
    } failure:nil];
}


/** 获取热搜内容 */
- (void)getHotSearchData{
    [[YURequest creatBaseRequest] POST:@"hotWords/list.json" parameters:nil success:^(id JSON) {
        self.hot_search_view = [UIView new];
        [self.view addSubview:self.hot_search_view];
        self.hot_search_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [self.hot_search_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.bottom.equalTo(@0);
        }];
        UILabel *label = [UILabel new];
        [self.hot_search_view addSubview:label];
        label.text = @"热门搜索";
        label.font = CurrencyFont(14);
        label.textColor = UIColorFromHex(0x999999);
        [label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(@15);
            make.top.equalTo(@35);
        }];
        for (int i = 0; i < [JSON count]; i++) {
            CGFloat width = (kMainWidth - 50) / 3;
            CGFloat height = 35;
            CGFloat x = 15 + i % 3 * (width + 10);
            CGFloat y = 70 + i / 3 * (height + 10);
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [self.hot_search_view addSubview:button];
            button.frame = CGRectMake(x, y, width, height);
            [button setTitle:JSON[i][@"word"] forState:UIControlStateNormal];
            [button setTitleColor:UIColorFromHex(0xFF5E56)  forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
            button.titleLabel.font = CurrencyFont(14);
            button.layer.masksToBounds = YES;
            button.layer.cornerRadius = 4;
            [button addTarget:self action:@selector(hotSearch:) forControlEvents:UIControlEventTouchUpInside];
        }
        
    } failure:nil];
}

- (void)hotSearch:(UIButton *)btn{
    self.searchBar.text = btn.titleLabel.text;
    self.search_key = btn.titleLabel.text;
    [self.tableView.mj_header beginRefreshing];
    [self.hot_search_view removeFromSuperview];
}

/** 更换筛选类型 */
- (void)screen:(UIButton *)btn{
    for (UIButton *btn in self.set_screen_view.subviews) {
        if ([self.set_screen_view.subviews indexOfObject:btn] == 3) {
            break;
        }
        btn.selected = NO;
    }
    btn.selected = YES;
    NSString *title = btn.titleLabel.text;
    if ([title isEqualToString:@"综合"]) {
        self.order = nil;
        UIButton *btn = self.set_screen_view.subviews.lastObject;
        btn.selected = NO;
        [self.tableView.mj_header beginRefreshing];
    }else if ([title isEqualToString:@"返利"]){
        self.order = @"commissionRate_desc";
        [self.tableView.mj_header beginRefreshing];
    }else if ([title isEqualToString:@"销量"]){
        self.order = @"commissionNum_desc";
        [self.tableView.mj_header beginRefreshing];
    }else{
        [self.view addSubview:self.screen_view];
        self.screen_view.hidden = NO;
    }
}

- (void)back{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    _search_key = searchBar.text;
    [searchBar endEditing:YES];
    [self.tableView.mj_header beginRefreshing];
}

/** 搜索接口 */
- (void)getSearchData{
    NSInteger curPage = self.array.count / 20 + 1;
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:@{@"keywords":self.searchBar.text,@"curPage":@(curPage)}];
    if (self.start_price_tf.text.length) {
        [dic setValue:self.start_price_tf.text forKey:@"startPrice"];
    }
    if (self.end_price_tf.text.length) {
        [dic setValue:self.end_price_tf.text forKey:@"endPrice"];
    }
    if (_order.length) {
        [dic setValue:_order forKey:@"order"];
    }
    if (_isMall) {
        [dic setValue:@"true" forKey:@"isMall"];
    }
    [[YURequest creatMarketRequest] POST:@"item/search.json" parameters:dic success:^(id JSON) {
        for (NSDictionary *dic in JSON) {
            CateItemModel *model = [[CateItemModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        /** 判断是否需要上拉加载更多 */
        if ([JSON count] % 20 != 0 || [JSON count] == 0) {
            self.tableView.mj_footer = nil;
        }else{
            self.tableView.mj_footer = [MJRefreshAutoStateFooter footerWithRefreshingTarget:self refreshingAction:@selector(getSearchData)];
        }
        [self.tableView reloadData];
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    } failure:^(NSError *error, NSString *errorMsg, NSInteger code) {
        [self.tableView.mj_header endRefreshing];
        [self.tableView.mj_footer endRefreshing];
    }];
}

@end
