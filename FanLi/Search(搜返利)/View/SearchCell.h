//
//  SearchCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/31.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CateItemModel.h"

@interface SearchCell : UITableViewCell

@property (nonatomic, strong) CateItemModel *model;

@end
