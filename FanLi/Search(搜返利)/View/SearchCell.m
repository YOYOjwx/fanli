//
//  SearchCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/31.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "SearchCell.h"

@interface SearchCell ()

/** 商品图片 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 商品标题 */
@property (nonatomic, strong) UILabel *title_lb;
/** 天猫图标 */
@property (nonatomic, strong) UIImageView *mall_image;
/** 付款人数 */
@property (nonatomic, strong) UILabel *pay_count_lb;
/** 商家地址 */
@property (nonatomic, strong) UILabel *location_lb;
/** 商家别名 */
@property (nonatomic, strong) UILabel *seller_nick_lb;
/** 原来价钱 */
@property (nonatomic, strong) UILabel *origin_money_lb;
/** 购买加钱 */
@property (nonatomic, strong) UILabel *pay_money_lb;
/** 返利的金币 */
@property (nonatomic, strong) UILabel *coins_lb;

@end

@implementation SearchCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.layer.masksToBounds = YES;
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.equalTo(@10);
            make.bottom.equalTo(@-10);
            make.width.height.equalTo(@100);
        }];
        self.mall_image = [UIImageView new];
        [self.contentView addSubview:self.mall_image];
        self.mall_image.image = [UIImage imageNamed:@"天猫标志"];
        [self.mall_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(_icon_image);
            make.trailing.equalTo(@-10);
            make.width.height.equalTo(@20);
        }];
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.numberOfLines = 2;
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_icon_image.mas_trailing).offset(10);
            make.trailing.equalTo(_mall_image.mas_leading).offset(-10);
            make.top.equalTo(_icon_image);
        }];
        UIView *merchant_information_view = [UIView new];
        [self.contentView addSubview:merchant_information_view];
        self.pay_count_lb = [UILabel new];
        [merchant_information_view addSubview:self.pay_count_lb];
        self.pay_count_lb.font = CurrencyFont(12);
        self.pay_count_lb.textColor = UIColorFromHex(0x999999);
        [self.pay_count_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.equalTo(@0);
        }];
        self.location_lb = [UILabel new];
        [merchant_information_view addSubview:self.location_lb];
        self.location_lb.font = CurrencyFont(12);
        self.location_lb.textColor = UIColorFromHex(0x999999);
        [self.location_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_pay_count_lb.mas_trailing).offset(10);
            make.top.equalTo(@0);
        }];
        self.seller_nick_lb = [UILabel new];
        [merchant_information_view addSubview:self.seller_nick_lb];
        self.seller_nick_lb.font = CurrencyFont(12);
        self.seller_nick_lb.textColor = UIColorFromHex(0x999999);
        [self.seller_nick_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_location_lb.mas_trailing).offset(10);
            make.top.equalTo(@0);
        }];
        [merchant_information_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.equalTo(_title_lb);
            make.top.equalTo(_title_lb.mas_bottom).offset(10);
            make.height.equalTo(_location_lb);
        }];
        UIImageView *fan_image = [UIImageView new];
        [self.contentView addSubview:fan_image];
        fan_image.image = [UIImage imageNamed:@"返"];
        [fan_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.equalTo(_title_lb);
            make.bottom.equalTo(_icon_image);
            make.width.height.equalTo(@15);
        }];
        self.coins_lb = [UILabel new];
        [self.contentView addSubview:self.coins_lb];
        self.coins_lb.textColor = UIColorFromHex(0xFF5E56);
        self.coins_lb.font = CurrencyFont(12);
        [self.coins_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(fan_image);
            make.leading.equalTo(fan_image.mas_trailing).offset(3);
        }];
        self.pay_money_lb = [UILabel new];
        [self.contentView addSubview:self.pay_money_lb];
        self.pay_money_lb.font = [UIFont boldSystemFontOfSize:16];
        self.pay_money_lb.textColor = UIColorFromHex(0xFF5E56);
        [self.pay_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(fan_image.mas_top).offset(-3);
            make.leading.equalTo(_icon_image.mas_trailing).offset(20.5);
        }];
        UILabel *rmb_lb = [UILabel new];
        [self.contentView addSubview:rmb_lb];
        rmb_lb.font = CurrencyFont(10);
        rmb_lb.textColor = UIColorFromHex(0xFF5E56);
        rmb_lb.text = @"￥";
        [rmb_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.trailing.equalTo(_pay_money_lb.mas_leading);
            make.bottom.equalTo(_pay_money_lb).offset(-2.5);
        }];
        self.origin_money_lb = [UILabel new];
        [self.contentView addSubview:self.origin_money_lb];
        self.origin_money_lb.font = CurrencyFont(12);
        self.origin_money_lb.textColor = UIColorFromHex(0x999999);
        [self.origin_money_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.equalTo(_pay_money_lb);
            make.leading.equalTo(_pay_money_lb.mas_trailing).offset(8);
        }];
        UIView *detele_line_view = [UIView new];
        [self.origin_money_lb addSubview:detele_line_view];
        detele_line_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        detele_line_view.backgroundColor = self.origin_money_lb.textColor;
        [detele_line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
            make.height.equalTo(@0.5);
            make.width.equalTo(_origin_money_lb.mas_width).multipliedBy(1.15);
        }];
        UIView *line_view = [UIView new];
        [self.contentView addSubview:line_view];
        line_view.backgroundColor = UIColorFromHex(0xF0F0F0);
        [line_view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.trailing.bottom.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}

- (void)setModel:(CateItemModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.pics.firstObject] placeholderImage:[UIImage imageNamed:@"默认图片"]];
    _title_lb.text = model.title;
    if (model.itemType == 2) {
        _mall_image.hidden = NO;
    }else{
        _mall_image.hidden = YES;
    }
    _pay_count_lb.text = [NSString stringWithFormat:@"%d人付款",model.monthlySales];
    _location_lb.text = model.location;
    _seller_nick_lb.text = model.sellerNick;
    _origin_money_lb.text = model.price;
    _pay_money_lb.text = model.umpPrice;
    _coins_lb.text = [@(model.coins).stringValue stringByAppendingString:@"金币"];
}

@end
