//
//  StrollController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/12.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "StrollController.h"
#import "ListTableView.h"
#import "UIImage+Common.h"
#import "YURefreshHeader.h"
#import "CommodityCell.h"
#import "SearchController.h"

@interface StrollController ()<UICollectionViewDelegate,UICollectionViewDataSource,UISearchBarDelegate>

/** 商城菜单 */
@property (nonatomic, strong) ListTableView *marketList;
/** 搜索按钮 */
@property (nonatomic, strong) UISearchBar *search;
/** 商品列表 */
@property (nonatomic, strong) UICollectionView *collectionView;
/** 存放数据的字典 */
@property (nonatomic, strong) NSMutableDictionary *dic;
/** 当前点击那一个一级菜单 */
@property (nonatomic, strong) NSString *xid;

@end

@implementation StrollController

- (NSMutableDictionary *)dic{
    if (!_dic) {
        _dic = [NSMutableDictionary dictionary];
    }
    return _dic;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = @"趣逛逛";
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    WS(weakSelf);
    /** 创建搜索控件 */
    self.search = [UISearchBar new];
    [self.view addSubview:self.search];
    self.search.searchBarStyle = UISearchBarStyleMinimal;
    self.search.backgroundImage = [UIImage imageWithColor:[UIColor whiteColor]];
    self.search.placeholder = @"请输入淘宝商品标题或关键字";
    self.search.delegate = self;
    [self.search mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.top.trailing.equalTo(@0);
    }];
    /** 创建商城菜单 */
    self.marketList = [[ListTableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
    [self.view addSubview:self.marketList];
    [self.marketList mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(weakSelf.search.mas_bottom);
        make.leading.bottom.equalTo(@0);
        make.width.equalTo(@80);
    }];
    /** 点击菜单执行的block */
    self.marketList.click = ^(ListModel *model) {
        weakSelf.xid = model.cateId.stringValue;
        [weakSelf getData];
    };
    /** 创建商品列表 */
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = 5;
    layout.minimumInteritemSpacing = 5;
    layout.sectionInset = UIEdgeInsetsMake(5, 5, 5, 5);
    CGFloat width = (kMainWidth - 80 - 20) / 3;
    layout.itemSize = CGSizeMake(width, width * 1.41);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [self.view addSubview:self.collectionView];
    self.collectionView.backgroundColor = UIColorFromHex(0xF0F0F0);
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.showsVerticalScrollIndicator = NO;
    self.collectionView.mj_header = [YURefreshHeader headerWithRefreshingBlock:^{
        if (self.xid == nil) {
            [weakSelf.collectionView.mj_header endRefreshing];
            return;
        }
        [weakSelf.dic removeObjectForKey:self.xid];
        [weakSelf getData];
    }];
    [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.trailing.bottom.equalTo(@0);
        make.top.equalTo(weakSelf.marketList);
        make.leading.equalTo(weakSelf.marketList.mas_trailing);
    }];
    /** 注册cell */
    [self.collectionView registerClass:[CommodityCell class] forCellWithReuseIdentifier:@"commodity"];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [self.dic[self.xid] count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CommodityCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"commodity" forIndexPath:indexPath];
    cell.model = self.dic[self.xid][indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ListModel *model = self.dic[self.xid][indexPath.item];
    SearchController *sc = [[SearchController alloc] init];
    sc.search_key = model.name;
    sc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sc animated:NO];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    SearchController *sc = [[SearchController alloc] init];
    sc.search_key = searchBar.text;
    sc.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:sc animated:NO];
}


/** 获取数据 */
- (void)getData{
    if (self.dic[self.xid]) {
        [self.collectionView reloadData];
        [self.collectionView.mj_header endRefreshing];
    }else{
        [[YURequest creatMarketRequest] POST:@"openApi/category/secondMenus" parameters:@{@"xid":self.xid} success:^(id JSON) {
            NSMutableArray *array = [NSMutableArray array];
            for (NSDictionary *dic in JSON) {
                ListModel *model = [[ListModel alloc] init];
                [model setValuesForKeysWithDictionary:dic];
                [array addObject:model];
            }
            [self.dic setValue:array forKey:self.xid];
            [self.collectionView reloadData];
            [self.collectionView.mj_header endRefreshing];
        } failure:nil];
    }
}

@end
