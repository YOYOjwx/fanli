//
//  ListModel.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseModel.h"

typedef NS_ENUM (int, CateItemType) {
    /** 轮播 */
    CateItemTypeCycle= 1,
    /** 裤腰带 */
    CateItemTypeItems,
    /** 单品 */
    CateItemTypeWare,
    /** 专辑 */
    CateItemTypeBrand,
    /** 更多 */
    CateItemTypeMore
};

@interface ListModel : BaseModel

/** 菜单名字 */
@property (nonatomic, strong) NSString *name;
/** 菜单分类id */
@property (nonatomic, strong) NSNumber *cateId;

@property (nonatomic, strong) NSNumber *cateItemId;
/** 菜单图片地址 如果pic为空则显示name */
@property (nonatomic, strong) NSString *pic;
/** 分类类型 */
@property (nonatomic, assign) CateItemType xType;
/** logo图片 */
@property (nonatomic, strong) NSString *logoPic;
/** 页面标签 */
@property (nonatomic, strong) NSString *pageTitle;
/** 更多项下面包含的专辑数量 */
@property (nonatomic, assign) int containsNum;

@end
