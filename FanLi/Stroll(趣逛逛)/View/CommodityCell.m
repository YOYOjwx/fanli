//
//  CommodityCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "CommodityCell.h"

@interface CommodityCell ()

/** 图标 */
@property (nonatomic, strong) UIImageView *icon_image;
/** 标题 */
@property (nonatomic, strong) UILabel *title_lb;

@end

@implementation CommodityCell

- (instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        WS(weakSelf);
        self.contentView.backgroundColor = [UIColor whiteColor];
        /** 创建图标 */
        self.icon_image = [UIImageView new];
        [self.contentView addSubview:self.icon_image];
        self.icon_image.contentMode = UIViewContentModeScaleAspectFill;
        [self.icon_image mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(@0);
            make.centerY.equalTo(@0).multipliedBy(0.9);
            make.width.height.equalTo(weakSelf.contentView).multipliedBy(0.55);
        }];
        /** 创建标题 */
        UIView *bottom = [UIView new];
        [self.contentView addSubview:bottom];
        [bottom mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.leading.trailing.equalTo(@0);
            make.top.equalTo(weakSelf.icon_image.mas_bottom);
        }];
        self.title_lb = [UILabel new];
        [bottom addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(12);
        self.title_lb.textColor = UIColorFromHex(0x666666);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
    }
    return self;
}

- (void)setModel:(ListModel *)model{
    _model = model;
    [_icon_image sd_setImageWithURL:[NSURL URLWithString:model.pic] placeholderImage:[UIImage imageNamed:@"图片默认"]];
    _title_lb.text = model.name;
}

@end
