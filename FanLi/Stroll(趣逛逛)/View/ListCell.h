//
//  ListCell.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListModel.h"

@interface ListCell : UITableViewCell

@property (nonatomic, strong) ListModel *model;

@end
