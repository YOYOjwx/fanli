//
//  ListCell.m
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ListCell.h"

@interface ListCell ()

@property (nonatomic, strong) UILabel *title_lb;

@end

@implementation ListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = NO;
        self.title_lb = [UILabel new];
        [self.contentView addSubview:self.title_lb];
        self.title_lb.font = CurrencyFont(14);
        self.title_lb.textColor = UIColorFromHex(0x333333);
        [self.title_lb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.center.equalTo(@0);
        }];
        UIView *line = [UIView new];
        [self.contentView addSubview:line];
        line.backgroundColor = UIColorFromHex(0xF0F0F0);
        [line mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.leading.trailing.equalTo(@0);
            make.height.equalTo(@0.5);
        }];
    }
    return self;
}

- (void)setModel:(ListModel *)model{
    _model = model;
    self.title_lb.text = model.name;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if (selected) {
        self.backgroundColor = UIColorFromHex(0xF0F0F0);
        self.title_lb.textColor = UIColorFromHex(0xFF5E56);
    }else{
        self.backgroundColor = UIColorFromHex(0xFFFFFF);
        self.title_lb.textColor = UIColorFromHex(0x333333);
    }
}

@end
