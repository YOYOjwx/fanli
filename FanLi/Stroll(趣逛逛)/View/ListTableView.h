//
//  ListTableView.h
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListCell.h"

@interface ListTableView : UITableView

/** 获取网络数据 */
- (void)refresh;
/** 点击某一个cell执行的block */
@property (nonatomic, copy) void (^click)(ListModel *model);

@end
