//
//  ListTableView.m
//  FanLi
//
//  Created by 费猫 on 2017/5/22.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "ListTableView.h"

@interface ListTableView ()<UITableViewDelegate,UITableViewDataSource>
/** 选中的view */
@property (nonatomic, strong) UIView *selectView;
/** 存放网络数据的数组 */
@property (nonatomic, strong) NSMutableArray *array;
/** 当前点击的第几个cell */
@property (nonatomic, assign) NSInteger index;

@end

@implementation ListTableView

- (NSMutableArray *)array{
    if (!_array) {
        _array = [NSMutableArray array];
    }
    return _array;
}

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.separatorStyle = NO;
        self.showsVerticalScrollIndicator = NO;
        self.backgroundColor = UIColorFromHex(0xF0F0F0);
        /** 注册cell */
        [self registerClass:[ListCell class] forCellReuseIdentifier:@"listCell"];
        /** 创建选择View */
        self.selectView = [UIView new];
        [self addSubview:self.selectView];
        self.selectView.backgroundColor = UIColorFromHex(0xFF5E56);
        [self.selectView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.leading.top.equalTo(@0);
            make.width.equalTo(@4);
            make.height.equalTo(@45);
        }];
        /** 从网络获取数据 */
        [self refresh];
    }
    return self;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"listCell" forIndexPath:indexPath];
    cell.model = self.array[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.index == indexPath.row) return;
    ListCell *cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.index inSection:0]];
    [cell setSelected:NO animated:YES];
    self.index = indexPath.row;
    [self.selectView mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(indexPath.row * 44);
    }];
    [UIView animateWithDuration:0.3 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0 options:UIViewAnimationOptionTransitionNone animations:^{
        [self layoutIfNeeded];
    } completion:nil];
    if (self.click) {
        self.click(self.array[indexPath.row]);
    }
}

/** 从网络获取数据 */
- (void)refresh{
    [[YURequest creatMarketRequest] POST:@"openApi/category/menus" parameters:nil success:^(id JSON) {
        [self.array removeAllObjects];
        for (NSDictionary *dic in JSON) {
            ListModel *model = [[ListModel alloc] init];
            [model setValuesForKeysWithDictionary:dic];
            [self.array addObject:model];
        }
        [self reloadData];
        if (self.click) {
            self.click(self.array[0]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            ListCell *cell = [self cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.index inSection:0]];
            [cell setSelected:YES animated:NO];
        });
    } failure:nil];
}

@end
