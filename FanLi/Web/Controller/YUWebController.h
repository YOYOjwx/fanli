//
//  YUWebController.h
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "BaseViewController.h"
#import <WebKit/WebKit.h>

@interface YUWebController : BaseViewController

/** 右侧barButton数组 */
@property(nonatomic, strong) NSArray<UIBarButtonItem *> *rightBarButtonItems;
/** URL */
@property (nonatomic, strong) NSURL *url;
/** title */
@property (nonatomic, strong) NSString *title_string;
/** 是否隐藏进度条 */
@property (nonatomic, assign) BOOL hideProgress;
/** 页面加载完成的Block */
@property (nonatomic, copy) void (^webDidFinish)(WKWebView *webView);
/** 点击页面上的控件执行的Block */
@property (nonatomic, copy) void (^decidePolicy)(WKNavigationAction *navigationAction);

@end
