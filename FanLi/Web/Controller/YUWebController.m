//
//  YUWebController.m
//  FanLi
//
//  Created by 费猫 on 2017/5/17.
//  Copyright © 2017年 费猫. All rights reserved.
//

#import "YUWebController.h"

@interface YUWebController ()<WKUIDelegate,WKNavigationDelegate>

/** webView */
@property (nonatomic, strong) WKWebView *web_view;
/** 进度条 */
@property (nonatomic, strong) UIView *progress_view;
/** 左侧返回item */
@property (nonatomic, strong) UIBarButtonItem *backItem;
/** 关闭item */
@property (nonatomic, strong) UIBarButtonItem *closeItem;

@end

@implementation YUWebController

- (void)dealloc{
    /** 移除观察者 */
    [_web_view removeObserver:self forKeyPath:@"estimatedProgress"];
    [_web_view removeObserver:self forKeyPath:@"title"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    /** 设置标题 */
    self.navigationItem.title = self.title_string;
    /** 设置barbuttonitem */
    self.navigationItem.rightBarButtonItems = self.rightBarButtonItems;
    /** 创建子视图 */
    [self creatSubViews];
}

/** 创建子视图 */
- (void)creatSubViews{
    /** 创建webView */
    self.web_view = [WKWebView new];
    [self.view addSubview:self.web_view];
    [self.web_view addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    [self.web_view addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
    self.web_view.UIDelegate = self;
    self.web_view.navigationDelegate = self;
    [self.web_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.trailing.bottom.equalTo(@0);
    }];
    [self.web_view loadRequest:[NSURLRequest requestWithURL:self.url]];
    /** 创建进度条 */
    self.progress_view = [UIView new];
    [self.view addSubview:self.progress_view];
    self.progress_view.backgroundColor = UIColorFromHex(0xFF4C4C);
    [self.progress_view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.leading.equalTo(@0);
        make.height.equalTo(@2);
    }];
    /** 创建左侧返回itme */
    self.backItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"返回"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStyleDone target:self action:@selector(canGoBack)];
    /** 创建关闭按钮 */
    self.closeItem = [[UIBarButtonItem alloc] initWithTitle:@"关闭" style:UIBarButtonItemStylePlain target:self action:@selector(closeWeb)];
    self.closeItem.tintColor = [UIColor blackColor];
    [self.closeItem setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(15)} forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItems = @[self.backItem];
    /** 是否隐藏进度条 */
    self.progress_view.hidden = self.hideProgress;
}

/** 返回方法 */
- (void)canGoBack{
    if (self.web_view.canGoBack) {
        [self.web_view goBack];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/** 关闭方法 */
- (void)closeWeb{
    [self.navigationController popViewControllerAnimated:YES];
}

/** 观察者之行的方法 */
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    WKWebView *web_view = (WKWebView *)object;
    /** 获取进度条 */
    if ([keyPath isEqualToString:@"estimatedProgress"]) {
        [self.progress_view mas_updateConstraints:^(MASConstraintMaker *make) {
            make.width.equalTo(@(web_view.estimatedProgress * kMainWidth));
        }];
        if (self.web_view.estimatedProgress >= 1.0f) {
            [UIView animateWithDuration:0.5f animations:^{
                self.progress_view.alpha = 0.0f;
            }];
        }else{
            self.progress_view.alpha = 1.0f;
        }
    }
    if ([keyPath isEqualToString:@"title"]) {
        NSString *title = change[@"new"];
        self.navigationItem.title = title;
        if ([web_view canGoBack]) {
            self.navigationItem.leftBarButtonItems = @[self.backItem,self.closeItem];
        }else{
            self.navigationItem.leftBarButtonItems = @[self.backItem];
        }
    }
}

/** 页面加载完成走的方法 */
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    if (self.webDidFinish) {
        self.webDidFinish(webView);
    }
}

/** wkView不能link跳转app解决方法 */
-(void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler{
    //如果link跳转
//    if (navigationAction.navigationType == WKNavigationTypeLinkActivated) {
//        [[UIApplication sharedApplication] openURL:navigationAction.request.URL];
//    }
    if ([navigationAction.request.URL.absoluteString isEqualToString:@"fanli://orderTask"]) {
        [self pushOrderTaskController];
    }
    if (self.decidePolicy) {
        self.decidePolicy(navigationAction);
    }
    decisionHandler(WKNavigationActionPolicyAllow);
}

/** 跳转任务详情控制器 */
- (void)pushOrderTaskController{
    YUWebController *orderVC = [[YUWebController alloc] init];
    orderVC.url = [NSURL URLWithString:OrderDetailURL];
    orderVC.title_string = @"下单任务";
    UIBarButtonItem *rule_item = [[UIBarButtonItem alloc] initWithTitle:@"规则" style:UIBarButtonItemStyleDone target:self action:@selector(pushRuleWebView)];
    rule_item.tintColor = [UIColor blackColor];
    [rule_item setTitleTextAttributes:@{NSFontAttributeName:CurrencyFont(12)} forState:UIControlStateNormal];
    orderVC.rightBarButtonItems = @[rule_item];
    [self.navigationController pushViewController:orderVC animated:YES];
}

- (void)pushRuleWebView{
    YUWebController *orderVC = self.navigationController.childViewControllers.lastObject;
    YUWebController *ruleVC = [[YUWebController alloc] init];
    ruleVC.title_string = @"下单任务细则";
    ruleVC.url = [NSURL URLWithString:OrderTaskRuleURL];
    [orderVC.navigationController pushViewController:ruleVC animated:YES];
}

/** wkwebview不能弹窗，这里获取弹窗信息，用系统的控件进行弹窗 */
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"提示" message:message?:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:([UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }])];
    [alertController addAction:([UIAlertAction actionWithTitle:@"确认" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * _Nullable))completionHandler{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:prompt message:@"" preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = defaultText;
    }];
    [alertController addAction:([UIAlertAction actionWithTitle:@"完成" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(alertController.textFields[0].text?:@"");
    }])];
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
